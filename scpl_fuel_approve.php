<?php
	require('connect.php'); 
 
	$dbid = $conn->real_escape_string($_POST['dbid']);
    // $tripno = $conn->real_escape_string($_POST['tripno']);
	
	$tripno = $conn->real_escape_string(explode("_",$_POST['tripno'])[0]);
	$tripno_new = $conn->real_escape_string(explode("_",$_POST['tripno'])[1]);
	
    $today = date('Y-m-d');
    $timestamp = date('Y-m-d H:i:s');

try {
	$conn->query("START TRANSACTION"); 

		    $sql = "select * from diesel_api.cons_fuel where id='$dbid'";
			if ($conn->query($sql) === FALSE) {
				throw new Exception(mysqli_error($conn)." Code 001");             
         	}
			$row = $conn->query($sql)->fetch_assoc();
			$qty = $row['qty'];
			$rate = $row['rate'];
			$amount = $row['amount'];
			$pump_code = $row['pump'];
			$truckno = $row['tno'];
 		    $unq_id = $row['tno']."_".date('dmYHis');

 		    $sql = "select * from dairy.diesel_pump_own where code='$pump_code'";
			if ($conn->query($sql) === FALSE) {
				throw new Exception(mysqli_error($conn)." Code 002");             
         	}
			$row = $conn->query($sql)->fetch_assoc();
			$pump_company = $row['comp'];
			$narration = $pump_company."-".$pump_code;

				$sql = "INSERT INTO dairy.diesel (trip_id,trip_no, narration, unq_id, tno, amount, date, branch, rate, qty, branch_user,timestamp) 
				VALUES ('$tripno', '$tripno_new','$narration', '$unq_id', '$truckno', '$amount', '$today', '$branch_name', '$rate', '$qty',
				'$branch_emp','$timestamp')";
				
				if ($conn->query($sql) === FALSE) {
					throw new Exception(mysqli_error($conn)." Code 017");             
				}
				$L1 = $conn->insert_id;

				$sql = "INSERT INTO dairy.diesel_entry (narration, unq_id, tno, diesel, date, branch, card_pump, card, dsl_company, 
				veh_no,timestamp) VALUES ('$narration', '$unq_id', '$truckno', '$amount', '$today', '$branch_name', 'PUMP', '$pump_code',
				'$pump_company', '$pump_code','$timestamp')";
				
				if ($conn->query($sql) === FALSE) {
					throw new Exception(mysqli_error($conn)." Code 018");             
				}
				$L2 = $conn->insert_id;

				if($L1!=$L2){
					throw new Exception("Unable to add Diesel and Diesel Entry - ID Mismatched");             
				}



				$sql = "select count(*) as rowcount, COALESCE(SUM(diesel_qty),0) as diesel_qty, COALESCE(SUM(diesel),0) as diesel from dairy.trip where id='$tripno' and (status='0' or active_trip='1')";	 
				if ($conn->query($sql) === FALSE) {
						throw new Exception(mysqli_error($conn)." Code 027");             
				}
				$trp = $conn->query($sql)->fetch_assoc();

				$getamount = $amount;

				$trip_qty = sprintf("%.2f", $trp['diesel_qty'] + $qty);
				$trip_amount =  sprintf("%.2f", $trp['diesel'] + $getamount);

				if($trp['rowcount'] > 0){

					$sql = "update dairy.trip set diesel='$trip_amount', diesel_qty='$trip_qty' where id='$tripno'"; 
					if ($conn->query($sql) === FALSE) {
						throw new Exception(mysqli_error($conn)." Code 028");             
	         		}

         		} else{
					throw new Exception("Running trip does not exist");             
         		}

				$sql = "select sum(qty) as qty, sum(amount) as amount from dairy.diesel where trip_id='$tripno'";	
				if ($conn->query($sql) === FALSE) {
					throw new Exception(mysqli_error($conn)." Code 029");             
				}
				$res = $conn->query($sql)->fetch_assoc();

				if($res['qty']!=$trip_qty or $res['amount']!=$trip_amount){
					throw new Exception("Mismatched Diesel Qty or Amount in Trip $tripno");             
				}
 
				  $sql = "select name from rrpl_database.emp_attendance where code = '$branch_emp'";
		          if($conn->query($sql) === FALSE) {
								throw new Exception(mysqli_error($conn)." Code 030");             
		          } 
		          $res = $conn->query($sql);
		          $data = $res->fetch_assoc();
		          $empname = $data['name'];

				$sql = "update diesel_api.cons_fuel set tripid='$tripno', approv='1', approv_user='$empname', approv_time=now() where id='$dbid'"; 
					if ($conn->query($sql) === FALSE) {
						throw new Exception(mysqli_error($conn)." Code 032");             
				}

	$conn->query("COMMIT");
	echo "
	<script>
	Swal.fire({
	position: 'top-end',
	icon: 'success',
	title: 'Updated Successfully',
	showConfirmButton: false,
	timer: 1000
	})
	</script>";

} catch(Exception $e) { 

	$conn->query("ROLLBACK"); 
	$content = $e->getMessage();
	$content = preg_replace("/[^0-9a-zA-Z_\.\- ]/", "", $content);  
	echo "
	<script>
	Swal.fire({
	icon: 'error',
	title: 'Error !!!',
	text: '$content'
	})
	</script>";		
} 