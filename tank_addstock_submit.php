<?php
   	
	require('connect.php'); 
 
    $dbid = $conn->real_escape_string($_POST['dbid']);
    $qty = $conn->real_escape_string($_POST['qty']);
    $otp = $conn->real_escape_string($_POST['otp']);
 
 try {
	$conn->query("START TRANSACTION"); 

  		  if($qty>100){
  					throw new Exception("Quantity cannot be greater than 100");             
  		  } 
    
			  $sql = "select * from dairy.diesel_pump_stock where id='$dbid'";
		  	if($conn->query($sql) === FALSE) {
					throw new Exception(mysqli_error($conn)." Code 0012");             
		  	} 
		  	$row = $conn->query($sql)->fetch_assoc();

      if($row['errorcode']!=$otp){
          throw new Exception("Invalid OTP number !");             
      }

      if($row['status']!='0'){
          throw new Exception("Stock may be closed already !");             
      }

      $updatedBalance = $row['balance'] + $qty;

			$sql = "update dairy.diesel_pump_stock set balance='$updatedBalance', oldbal='0', errorcode='1' where id='$row[id]' and status='0'";
			if ($conn->query($sql) === FALSE) {
					throw new Exception(mysqli_error($conn)." Code 001");             
			}
 
			$conn->query("COMMIT");
			echo "
			<script>
			Swal.fire({
			position: 'top-end',
			icon: 'success',
			title: 'Updated Successfully',
			showConfirmButton: false,
			timer: 1000
			})
			</script>";

} catch(Exception $e) { 

			$conn->query("ROLLBACK"); 
			$content = $e->getMessage();
			$content = preg_replace("/[^0-9a-zA-Z_\.\- ]/", "", $content);  
			echo "
			<script>
			Swal.fire({
			icon: 'error',
			title: 'Error !!!',
			text: '$content'
			})
			</script>";		
} 