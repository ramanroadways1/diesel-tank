<?php
   	
	require('connect.php'); 
 
    $dbid = $conn->real_escape_string($_POST['dbid']);
    $qty = $conn->real_escape_string($_POST['qty']);
    $tank = $conn->real_escape_string($_POST['tank']);
 
 try {
	$conn->query("START TRANSACTION"); 

		  if($qty>100){
					throw new Exception("Quantity cannot be greater than 100");             
		  } 

 		  $otp = rand(111111,999999);
          $sql = "select name from rrpl_database.emp_attendance where code = '$branch_emp'";
          if($conn->query($sql) === FALSE) {
					throw new Exception(mysqli_error($conn)." Code 0012");             
          } 
          $res = $conn->query($sql);
          $data = $res->fetch_assoc();
          $empname = $data['name'];

          $sql = "select * from diesel_api.dsl_cpanel where company='SMSAUTH'";
          if($conn->query($sql) === FALSE) {
					throw new Exception(mysqli_error($conn)." Code 0013");             
          }
          $resk = $conn->query($sql);
          $data = $resk->fetch_assoc();
          $smsvalue = $data['value'];
          $sms = $data['title'];
          $sms = json_decode($sms, true);
          $sms = $sms['data'];

// $message="".." of ".strtolower($branch_name)." adding manual balance of $qty litre to stock $tank, OTP For Approval: ".$otp."RamanRoadways"; 
          
$empname1 = strtolower($empname);		  
$branch1 = strtolower($branch_name);		  

$message = "$empname1 of $branch1 adding manual balance of $qty litre to stock $tank.\nOTP is: $otp\nRamanRoadways";	  

          $messagebackup = htmlentities($message); 
          // $message = urlencode($message);
          $senderId=$sms['senderid'];
          $route=$sms['route'];
          $country=$sms['country'];
          $authKey=$sms['authkey'];
          if($smsvalue=='1'){
            $mobileNumber=$sms['mobileno'];
          } else {
            $mobileNumber="9549199160";
          }

                $postData = array(
                    'apikey' => $authKey,
                    'numbers' => $mobileNumber,
                    'message' => $message,
                    'sender' => $senderId,
                    'messagetype' => 'TXT'
                  );
                  $url=$sms['url'];
                  $ch = curl_init();
                  curl_setopt_array($ch, array(
                    CURLOPT_URL => $url,
                    CURLOPT_RETURNTRANSFER => true,
                    CURLOPT_POST => true,
                    CURLOPT_POSTFIELDS => $postData
                  ));

                  curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
                  curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);

                  $output = curl_exec($ch);
                  
                if(curl_errno($ch))
                {
                  echo 'error:' . curl_error($ch);
                } else {

                    $sql = "insert into dairy.diesel_pump_log (type, content) values ('message','$messagebackup')";
                    if($conn->query($sql) === FALSE) {
                        echo mysqli_error($conn);         
                    }

                }

                curl_close($ch);


			$sql = "select * from dairy.diesel_pump_stock where masterid='$dbid' order by id desc";
		  	if($conn->query($sql) === FALSE) {
					throw new Exception(mysqli_error($conn)." Code 0012");             
		  	} 
		  	$row = $conn->query($sql)->fetch_assoc();

			$sql = "update dairy.diesel_pump_stock set errorcode='$otp', oldbal='$qty' where id='$row[id]' and status='0'";
			if ($conn->query($sql) === FALSE) {
					throw new Exception(mysqli_error($conn)." Code 001");             
			}
 
			$conn->query("COMMIT");
			echo "
			<script>
			Swal.fire({
			position: 'top-end',
			icon: 'success',
			title: 'Sended Successfully',
			showConfirmButton: false,
			timer: 1000
			})
			</script>";

} catch(Exception $e) { 

			$conn->query("ROLLBACK"); 
			$content = $e->getMessage();
			$content = preg_replace("/[^0-9a-zA-Z_\.\- ]/", "", $content);  
			echo "
			<script>
			Swal.fire({
			icon: 'error',
			title: 'Error !!!',
			text: '$content'
			})
			</script>";		
} 