<?php
 
require('connect.php');
 
    $connection = new PDO('mysql:host='.$host.';dbname='.$db_name.';', $username, $password );
    $statement = $connection->prepare("SELECT masterid, purchasedate, pumpcode, sum(purchaseqty) as purchaseqty, sum(balance) as balance, adi, o.name as pumpname FROM dairy.diesel_pump_stock s
    left join dairy.diesel_pump_branch o on o.code = s.pumpcode
    where status='0' and o.admin='$branch_name' and o.billtype='2' GROUP by masterid");
    $statement->execute();
    $result = $statement->fetchAll();
    $count = $statement->rowCount();
    $data = array();

foreach($result as $row)
{
 
    $sub_array = array(); 
    $bar = sprintf("%.2f",$row["balance"]/$row["purchaseqty"]*100);

    $sub_array[] = "".$row["pumpname"]."";//date('d/m/Y', strtotime($row['purchasedate'])); 
    $sub_array[] = "<div class='progress' style='width: 65%; float: left;'>
<div class='progress-bar bg-info' role='progressbar' style='width: $bar%;' aria-valuenow='$bar' aria-valuemin='0' aria-valuemax='100'></div> 
</div> <span style='float: right'> $bar% </span>"; 
    $sub_array[] = "<span style='float:right;'>".$row["purchaseqty"]."</span>"; 

        $dis = '';
    if($row["balance"]>500){
        $dis = 'disabled';
    }

    $sub_array[] = "<span style='float:right;'>".$row["balance"]." &nbsp; <button style='padding:2px 5px;' class='btn btn-sm btn-danger' onclick='AddQty(\"$row[masterid]\")' $dis> <i class='fa fa-plus'></i> </button> </span>"; 

    $data[] = $sub_array;
} 

    $results = array(
      "sEcho" => 1,
    "iTotalRecords" => $count,
    "iTotalDisplayRecords" => $count,
    "aaData"=>$data);

echo json_encode($results); 
exit
?>
 