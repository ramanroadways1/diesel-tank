<?php 
include_once 'header.php';
?>  

<script type="text/javascript" src="https://cdn.datatables.net/1.10.19/js/jquery.dataTables.min.js"></script>
<script type="text/javascript" src="https://cdn.datatables.net/buttons/1.5.6/js/dataTables.buttons.min.js"></script>
<script type="text/javascript" src="https://cdn.datatables.net/buttons/1.5.6/js/buttons.flash.min.js"></script>
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/jszip/3.1.3/jszip.min.js"></script>
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.53/vfs_fonts.js"></script> 
<script type="text/javascript" src="https://cdn.datatables.net/buttons/1.5.6/js/buttons.html5.min.js"></script>
<script type="text/javascript" src="https://cdn.datatables.net/buttons/1.5.6/js/buttons.print.min.js"></script>
<script type="text/javascript" src="https://cdn.datatables.net/searchbuilder/1.0.0/js/dataTables.searchBuilder.min.js"></script>
<link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/searchbuilder/1.0.0/css/searchBuilder.dataTables.min.css">

<style type="text/css">
   #user_data_paginate{background-color:#fff}a.dt-button,button.dt-button,div.dt-button{padding:.2em 1em}div.dt-button-collection{max-height:300px;overflow-y:scroll}.dataTables_scroll{margin-bottom:20px}.table{margin:0!important}.applyBtn{border-radius:0!important}table.table-bordered.dataTable td{padding:10px 5px 10px 10px}.dt-buttons{float:right!important}.user_data_filter{float:right}.dt-button{padding:5px 20px;text-transform:uppercase;font-size:12px;text-align:center;cursor:pointer;outline:0;color:#fff;background-color:#37474f;border:none;border-radius:2px;box-shadow:0 4px #999}.dt-button:hover{background-color:#3e8e41}.dt-button:active{background-color:#3e8e41;box-shadow:0 5px #666;transform:translateY(4px)}#user_data_wrapper{width:100%!important}.dt-buttons{margin-bottom:20px}#appenddiv,#appenddiv2{display:block;position:relative}.ui-autocomplete{position:absolute}.table-hover tbody tr:hover td,.table-hover tbody tr:hover th{background-color: #FFF5D7}.table td{vertical-align:middle!important;font-size:11px!important;color:#000;font-family:Verdana,Geneva,sans-serif;padding-top:4px;padding-right:4px;padding-bottom:4px;padding-left:10px}.table-bordered td{border:3px solid #e3e6f0}#user_data_info,#user_data_length{float:left}#user_data_filter,#user_data_paginate{float:right}.paginate_button{color:#000;float:left;padding:6px 12px;text-decoration:none;border:1px solid #ccc;cursor:pointer}.ellipsis{display:none}[type=search]{margin-right:10px;width:250px}.ui-autocomplete{z-index:2150000000!important}.container input{position:absolute;opacity:0;cursor:pointer;height:0;width:0}.checkmark{border-radius:2px;position:absolute;top:0;height:20px;width:20px;background-color:#fff;border:1px solid #000}.container:hover input~.checkmark{background-color:#fff}.container input:checked~.checkmark{background-color:#fff}.container input:disabled~.checkmark{background-color:#eaecf4}.checkmark:after{content:"";position:absolute;display:none}.container input:checked~.checkmark:after{display:block}.container .checkmark:after{left:6px;top:-1px;width:8px;height:16px;border:solid #000;border-width:0 3px 3px 0;-webkit-transform:rotate(45deg);-ms-transform:rotate(45deg);transform:rotate(45deg)}button:disabled,button[disabled]{border:1px solid #333!important;color:#333!important;cursor:no-drop}.table .thead-light th{text-align:center;font-size:11px;color:#444}.component{display:none}table{width:100%!important}table.table-bordered.dataTable td{white-space:nowrap;overflow:hidden;text-overflow:ellipsis}.table .thead-light th{text-transform:uppercase!important}label{text-transform:uppercase}#appenddivbill,#appenddivbillparty,#appenddivcons,#appenddivdo,#appenddivfrom,#appenddivinv,#appenddivitem,#appenddivlr,#appenddivship,#appenddivtno,#appenddivto{display:block;position:relative}.ui-autocomplete{position:absolute}.card label{color:#444}.card label{color:#444} .content{padding-bottom: 0px !important;}
   .main-panel>.content{
    padding: 0 20px 20px;
   }
   table.table-bordered.dataTable td{
    padding: 5px 5px 5px 10px;
   }
   #user_data2_info,#user_data2_length{float:left}#user_data2_filter,#user_data2_paginate{float:right}
   .user_data2_filter{float:right}
   #user_data2_wrapper{width:100%!important}
   #user_data2_paginate{background-color:#fff}

   #user_data3_info,#user_data3_length{float:left}#user_data3_filter,#user_data3_paginate{float:right}
   .user_data3_filter{float:right;}
   #user_data3_wrapper{width:100%!important}
   #user_data3_paginate{background-color:#fff}

   table.table-bordered.dataTable td{
    text-align: center;
   }

.noselect {
    cursor: default;
    -webkit-touch-callout: none;
    -webkit-user-select: none;
    -khtml-user-select: none;
    -moz-user-select: none;
    -ms-user-select: none;
    user-select: none;
}
</style>

<div id="response"></div>

<div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="memberModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content"> 
            <div class="dash" id="modaldetail"> 
            </div> 
        </div>
    </div>
</div>

<div id="content-wrapper" class="d-flex flex-column"> 
<div id="content">

<div id="updatereq_status"></div> 
 <div class="container-fluid"> 
<div class="row"> 

<div class="col-md-12" >
<div class="card shadow mb-4" style="border-radius: 0px !important;"> 
<div class="card-header">
 

  
 
<form method="post" action="" id="UploadBill" autocomplete="off" enctype='multipart/form-data'> 
<div class="col-md-10 offset-md-1" >
<div class="" style="padding-top: 5px; padding-bottom: 10px;">
   <h6> PURCHASE  INVOICE (CONSUMER PUMP) </h6> 
</div>
<div class="card-body "  style="background-color: #fff; border: 1px solid #ccc;">

  <div class="row">
	<div class="col-md-3">
      <label style="text-transform: uppercase;"> BRANCH </label>
      <select id="" class="form-control" name="stockadmin" onChange="updt(this);" required="required">
        <option value=""> -- select -- </option>
        <?php 

        $sql = "SELECT * FROM rrpl_database.user where role='2'";
        $res = $conn->query($sql);
        while($row = $res->fetch_assoc()){
          echo "<option value='".$row['username']."'>".$row['username']."</option>";
        }

        ?> 
      </select>
    </div>

   
    <div class="col-md-3">
      <label>INVOICE DATE</label>
      <input type="date" name="idate" min="<?= date('Y-m-d',strtotime("-7 days")); ?>" max="<?= date('Y-m-d',strtotime("+1 days")); ?>" class="form-control" required="required">
    </div>
    <div class="col-md-2">
      <label>PURCHASE QTY (Litre)</label>
      <input type="text" onkeyup="getrate();" id="qty" name="pqty" class="form-control" required="required" oninput="this.value=this.value.replace(/[^0-9.]/,'')">
    </div>
    <div class="col-md-2">
      <label>PURCHASE AMOUNT</label>
      <input type="number" name="pamt" id="pamt" class="form-control" required="required" onkeyup="getrate(); toWords();" oninput="this.value=this.value.replace(/[^0-9]/,'')" maxlength="9">
    </div> 

    <input type="hidden" name="prate" id="rate" oninput="this.value=this.value.replace(/[^0-9]/,'')" readonly="">
      
        <div class="col-md-2">
      <label style="text-transform: uppercase;"> Fuel Type </label>
      <select id="" class="form-control" name="ftype" required="required">
        <option value=""> -- select -- </option>
        <option value="0"> HSD </option>
        <option value="1"> ADI </option>
      </select>
    </div>
 
 
    <div class="col-md-12"> &nbsp; </div>
    <div class="col-md-4">
      <label>AMOUNT (IN WORDS)</label>
      <input type="text" name="" readonly="" id="output" class="form-control" style="background: #fff;">
    </div> 
 
    <div class="col-md-3">
      <label> INVOICE</label>
      <input type="file" name="file[]" accept="image/x-png,image/gif,image/jpeg,application/pdf" class="form-control"  required="" style="overflow: hidden;">
    </div> 

<!--     <div class="col-md-3">
      <label>LOADING INVOICE TEMP (<sup> o </sup>C)</label>
      <input type="text"  id="load" name="loadtemp" class="form-control" oninput="this.value=this.value.replace(/[^0-9.]/,'')">
    </div>  -->



    <div class="col-md-3">
      <label>SUPPLIER</label>
      <input type="text" id="" style="text-transform: uppercase;" name="supp" class="form-control" required="required" oninput="this.value=this.value.replace(/[^a-z A-Z 0-9]\-/,'')">
    </div>


   <div class="col-md-2">
      <label>PASSWORD</label>
      <input type="password" id="pin" name="pin" class="form-control" required="required" oninput="this.value=this.value.replace(/[^0-9]/,'')"  maxlength="4">
    </div>

    <div class="col-md-12" > 
      <button type="submit" style="float: right !important;" class='btn btn-success'> <i class='fa fa-upload '></i> <b> UPLOAD </b> </button>    
    </div>
  </div>
  
</div>
</form> 

<br> 

</div> 
</div>
</div> 
<div id="content-wrapper" class="d-flex flex-column">
   <div id="content">
      <div id="updatereq_status"></div>
      <div class="">
         <div class="row">
            <div class="col-md-12" >
               <div class="card shadow mb-4" style="border-radius: 0px !important;">
                  <div class="card-header">
                     <!-- <div class="col-md-12" style="padding-top: 5px; padding-bottom: 10px;">
                        <h6>REJECTED POD - UPLOADS APPROVAL </h6> 
                        </div> -->
                 
                     <div class="card-body col-md-10 offset-md-1" style="padding-top: 0px; margin-bottom: 15px;">
                        <!-- style="  -->
                        <table id="user_data" class="table table-bordered table-hover" style="background: #fff;">
                           <thead class="thead-light">
                            <tr>
                                 <th colspan="8" style="background: #fff; font-size: 13px; letter-spacing: 0.5px;">   INVOICE - PENDING  APPROVAL </th>
                              </tr>
                              <tr>
                                 <th>  BRANCH</th>
                                 <th>  DATE </th>
                                 <th>  QTY  </th>
                                 <th>  AMOUNT </th>
                                 <th>  FUEL </th>
                                 <th>  SUPPLIER </th>
                                 <th>  INVOICE</th>
                              </tr>
                           </thead>
                        </table>
                     </div> 
                     <br> 
                  </div>
               </div>
            </div>
         </div>
      </div>
   </div>
</div>
</div>
</div> 
</div>
</div>
 <script type="text/javascript">
                function convert_number(number) {
            if ((number < 0) || (number > 999999999)) {
                return "Number is out of range";
            }
            var Gn = Math.floor(number / 10000000); /* Crore */
            number -= Gn * 10000000;
            var kn = Math.floor(number / 100000); /* lakhs */
            number -= kn * 100000;
            var Hn = Math.floor(number / 1000); /* thousand */
            number -= Hn * 1000;
            var Dn = Math.floor(number / 100); /* Tens (deca) */
            number = number % 100; /* Ones */
            var tn = Math.floor(number / 10);
            var one = Math.floor(number % 10);
            var res = "";

            if (Gn > 0) {
                res += (convert_number(Gn) + " Crore");
            }
            if (kn > 0) {
                res += (((res == "") ? "" : " ") +
                    convert_number(kn) + " Lakhs");
            }
            if (Hn > 0) {
                res += (((res == "") ? "" : " ") +
                    convert_number(Hn) + " Thousand");
            }

            if (Dn) {
                res += (((res == "") ? "" : " ") +
                    convert_number(Dn) + " Hundred");
            }


            var ones = Array("", "One", "Two", "Three", "Four", "Five", "Six", "Seven", "Eight", "Nine", "Ten", "Eleven", "Twelve", "Thirteen", "Fourteen", "Fifteen", "Sixteen", "Seventeen", "Eightteen", "Nineteen");
            var tens = Array("", "", "Twenty", "Thirty", "Fourty", "Fifty", "Sixty", "Seventy", "Eigthy", "Ninety");

            if (tn > 0 || one > 0) {
                if (!(res == "")) {
                    res += " and ";
                }
                if (tn < 2) {
                    res += ones[tn * 10 + one];
                } else {

                    res += tens[tn];
                    if (one > 0) {
                        res += ("-" + ones[one]);
                    }
                }
            }

            if (res == "") {
                res = "zero";
            }
            return res;
        }

        function toWords() { 
            // document.getElementById("output").innerHTML = convert_number(document.getElementById("pamt").value);
            document.getElementById("output").value = convert_number(document.getElementById("pamt").value); 
        }
    </script>
<script> 

           function getrate() 
           { 
            amount = $("#pamt").val();
            qty = $("#qty").val();

            rate = parseFloat(Number(amount/qty)).toFixed(2);
            
            $("#rate").val(rate);
 
          }


$("form#UploadBill").submit(function(e) {
    e.preventDefault();    
    var formData = new FormData(this);
                $('#loadicon').show(); 

    $.ajax({
        url: 'stock_bill_save.php',
        type: 'POST',
        data: formData,
        success: function (data) {
            $('#response').html(data);  
            document.getElementById("UploadBill").reset();
            $('#user_data').DataTable().ajax.reload(null,false); 
                            $('#loadicon').hide(); 

        },
        cache: false,
        contentType: false,
        processData: false
    });
});

jQuery( document ).ready(function() {

  var table = jQuery("#user_data").dataTable({
  "lengthMenu": [ [10, 100, 500, -1], [10, 100, 500, "All"] ], 
  "bProcessing": true,
  "searchDelay": 1000,
  "scrollY": 100,
  "scrollX": true,
  "sAjaxSource": "stock_bill_data.php",
  "bPaginate": false,
  "sPaginationType":"full_numbers",
  "iDisplayLength": 10,
  "ordering": false,
  "buttons": [],
  "language": {
          "loadingRecords": "&nbsp;",
          "processing": "<center> <font color=brown> Please wait while Loading </font> <img src=https://www.mypsdtohtml.com/_ui/images/loading.gif height=20> </center>"
      },
  dom: "<\'toolbar\'>lBfrtip", 
  "columnDefs":[
  {

  "targets": [ 1 ],
  "visible": false 
  },
  ], 
  "initComplete": function( settings, json ) {
  }
  });   
  });

  $(document).ready(function() { 
  var table = $("#user_data").DataTable(); 
  $("#table-filter").on("change", function(){
  table.search(this.value).draw();   
  });
  } );   

</script>
<?php 
  include_once 'footer.php';
?>  
 