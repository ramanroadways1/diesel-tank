<?php
 
 require('connect.php');
 
    $pumpcode = $conn->real_escape_string($_POST['pumpcode']);
    $shortage = $conn->real_escape_string($_POST['shortage']);
    $otp = $conn->real_escape_string($_POST['otp']);
    $getotp = sha1($otp);

    try {
    $conn->query("START TRANSACTION"); 

          $sql = "select otp, masterid, purchasedate, sum(expshort) as tempshort, sum(purchaseqty) as purchaseqty from dairy.diesel_pump_stock WHERE status='0' and pumpcode='$_POST[pumpcode]' group by masterid order by id asc LIMIT 1";
          if($conn->query($sql) === FALSE) {
                throw new Exception(mysqli_error($conn)." Code 002");             
          }          
          $resulto = $conn->query($sql);
          $rowo = $resulto->fetch_assoc(); 
          $purchaseqty = $rowo['purchaseqty'];
          $purchasedate = $rowo['purchasedate'];
          $MasterID = $rowo['masterid'];
          $TempShortage = $rowo['tempshort'];
          $otp = $rowo['otp'];
 
          $sql = "SELECT COALESCE(sum(qty),0) as qty FROM dairy.diesel where stockid in (select purchaseid from dairy.diesel_pump_stock where masterid='$MasterID')";
          if($conn->query($sql) === FALSE) {
                throw new Exception(mysqli_error($conn)." Code 003");             
          } 
          $resk = $conn->query($sql);
          $rowk = $resk->fetch_assoc();
          $ownstock = $rowk['qty'];

          $sql = "SELECT COALESCE(sum(qty),0) as qty FROM rrpl_database.diesel_fm where stockid in (select purchaseid from dairy.diesel_pump_stock where masterid='$MasterID')";
          if($conn->query($sql) === FALSE) {
                throw new Exception(mysqli_error($conn)." Code 004");             
          }          
          $resk = $conn->query($sql);
          $rowk = $resk->fetch_assoc();
          $mktstock = $rowk['qty'];

          $sql = "SELECT COALESCE(sum(qty),0) as qty FROM diesel_api.cons_dsl where stockid in (select purchaseid from dairy.diesel_pump_stock where masterid='$MasterID')";
          if($conn->query($sql) === FALSE) {
                throw new Exception(mysqli_error($conn)." Code 005");             
          }
          $resk = $conn->query($sql);
          $rowk = $resk->fetch_assoc();
          $othstock = $rowk['qty'];

          $sql = "SELECT COALESCE(sum(qty),0) as qty FROM diesel_api.stock_transfer where stockid in (select purchaseid from dairy.diesel_pump_stock where masterid='$MasterID')";
          if($conn->query($sql) === FALSE) {
                throw new Exception(mysqli_error($conn)." Code 051");             
          }
          $resk = $conn->query($sql);
          $rowk = $resk->fetch_assoc();
          $trasferqty = $rowk['qty'];

          $totalconsum = $ownstock + $mktstock + $othstock + $trasferqty;
          $ActualShortage = $purchaseqty - $totalconsum - $TempShortage;
 

          $sql = "select * from dairy.diesel_pump_stock WHERE masterid='$MasterID' order by id desc LIMIT 1";
          if($conn->query($sql) === FALSE) {
                throw new Exception(mysqli_error($conn)." Code 006");             
          }          
          $resulto = $conn->query($sql);
          $rowo = $resulto->fetch_assoc(); 
          $purchaserate = $rowo['purchaserate'];
          $purchaseid = $rowo['purchaseid'];

          $sqli = "SELECT COALESCE(sum(qty),0) as qty FROM diesel_api.cons_dsl where stockid in (select purchaseid from dairy.diesel_pump_stock where masterid='$MasterID')";
          $resi = $conn->query($sqli);
          $rowi = $resi->fetch_assoc(); 
          $scplqty = $rowi['qty']; 
          @$scplper = round((($scplqty/$totalconsum)*(100)), 2);
          $scplshortage =  round((($scplper*$shortage)/(100)), 2);
          $today = date('Y-m-d');
          $scplshortageamt = round(($scplshortage * $purchaserate), 2);


          if($getotp!=$otp){
            throw new Exception("Incorrect OTP !"); 
          }

          if(round($shortage,2)!=round($ActualShortage,2)){
            throw new Exception("Shortage Changed $shortage to $ActualShortage - Please Check Again !"); 
          }

                  $sql = "update dairy.diesel_pump_stock set excess='$shortage' WHERE purchaseid='$purchaseid'";
                  if ($conn->query($sql) === FALSE) {
                     throw new Exception(mysqli_error($conn)." Code 007");             
                  } 
                
                  $sql = "update dairy.diesel_pump_stock set oldbal=balance, balance='0', status='1', otp=NULL, closeuser='$branch_emp' WHERE masterid='$MasterID'";
                  if ($conn->query($sql) === FALSE) {
                      throw new Exception(mysqli_error($conn)." Code 008");             
                  }

                  if($scplqty>0){
                        $sql = "insert into diesel_api.cons_dsl (date, company, tno, pump, qty, rate, amount, stockid, done, done_time, done_branch, stamp, user) value ('$today', 'SCPL', 'SHORTAGE', '$pumpcode', '$scplshortage', '$purchaserate', '$scplshortageamt', '$purchaseid', '1', now(), '$branch_name', now(), 'SCPL.ADMIN')";  
                        if ($conn->query($sql) === FALSE) {
                        throw new Exception(mysqli_error($conn)." Code 009");             
                        } 
                  }
                  

////////////// start - Regularization
        $sql = "SELECT title FROM diesel_api.dsl_cpanel where company='DIESELPLUS_LOGIN'";
        if ($conn->query($sql) === FALSE) {
                throw new Exception(mysqli_error($conn)." Code 010");             
        }
        $resx = $conn->query($sql);
        $rowx = $resx->fetch_assoc();
        define("dplus_login",$rowx["title"]);

        $sql = "SELECT title FROM diesel_api.dsl_cpanel where company='DIESELPLUS_TOKEN'";
        if ($conn->query($sql) === FALSE) {
                throw new Exception(mysqli_error($conn)." Code 011");             
        }
        $resx = $conn->query($sql);
        $rowx = $resx->fetch_assoc();
        define("dplus_token",$rowx["title"]);

        $sql = "SELECT * FROM dairy.diesel_pump_branch where code='$pumpcode'";
        if ($conn->query($sql) === FALSE) {
                throw new Exception(mysqli_error($conn)." Code 091");             
        }
        $res = $conn->query($sql);
        $row = $res->fetch_assoc();
        $tanker_locationid = $row['locationid'];
        $tanker_tankid = $row['tankid'];
        $tanker_billtype = $row['billtype'];
 
        date_default_timezone_set('Asia/Kolkata'); 
        $getDD = strtotime(date('Y-m-d H:i:s'));
        $inv1 = date("Y-m-d", strtotime('+5 minutes', $getDD));
        $inv2 = date("H:i:s", strtotime('+5 minutes', $getDD));
        $invDate =  $inv1."T".$inv2."Z";
  
        $curl = curl_init(); 
        curl_setopt_array($curl, array(
          CURLOPT_URL => 'https://api.dieselplus.net/api/v1/raman-roadways/tank_inventories.json',
          CURLOPT_RETURNTRANSFER => true,
          CURLOPT_ENCODING => '',
          CURLOPT_MAXREDIRS => 10,
          CURLOPT_TIMEOUT => 30,
          CURLOPT_FOLLOWLOCATION => true,
          CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
          CURLOPT_CUSTOMREQUEST => 'POST',
          CURLOPT_POSTFIELDS =>'{
          "tank_inventory": {
            "location_id": "'.$tanker_locationid.'",
            "tank_id": "'.$tanker_tankid.'",
            "date": "'.$invDate.'",
            "quantity": 0,
            "inventory_reason": "on behalf of rrpl online"
          }
        }',
          CURLOPT_HTTPHEADER => array(
            "X-Login-Name: ".dplus_login,
            "X-Auth-Token: ".dplus_token,
            "Content-Type: application/json"
          ),
        ));

        $response = curl_exec($curl); 
        curl_close($curl); 

        $output = json_decode($response , true); 
        $output = $output['tank_inventory'];

        if($tanker_billtype=='2'){
 
            if($output['bio_percentage']!='0.0'){
                    throw new Exception("Error in Regularization dieselplus API");             
            }

            $sql = "INSERT INTO diesel_api.dplus_inventories (id, number, location_id, tank_id, date, fluid_id, quantity, bio_percentage, cost_price, cost_amount, inventory_reason, updated_by, updated_at, organization_id, timestamp) VALUES ('".$output['id']."', '".$output['number']."', '".$output['location_id']."', '".$output['tank_id']."', '".$output['date']."', '".$output['fluid_id']."', '".$output['quantity']."', '".$output['bio_percentage']."', '".$output['cost_price']."', '".$output['cost_amount']."', '".$output['inventory_reason']."', '".$output['updated_by']."', '".$output['updated_at']."', '".$output['organization_id']."', now())";
            if ($conn->query($sql) === FALSE) {
                    throw new Exception(mysqli_error($conn)." Code 095");             
            }

            $sql = "update dairy.diesel_pump_stock set dplus_inventory='".$output['id']."' WHERE masterid='$MasterID'";
              if ($conn->query($sql) === FALSE) {
                    throw new Exception(mysqli_error($conn)." Code 097");             
            }

        }
 ////////////// end - Regularization
 
 
      $conn->query("COMMIT");
      echo "
      <script>
      Swal.fire({
      position: 'top-end',
      icon: 'success',
      title: 'Stock Closed Successfully',
      showConfirmButton: false,
      timer: 1000
      })
      </script>";
    }
    catch(Exception $e) {
        $conn->query("ROLLBACK"); 
        $content = $e->getMessage();
        $content = preg_replace("/[^0-9a-zA-Z_\.\- ]/", "", $content);  
        echo "
        <script>
        Swal.fire({
        icon: 'error',
        title: 'Error !!!',
        text: '$content'
        })
        </script>"; 
    }
 
    
?>