<?php
    require('connect.php'); 
    
    $id = $conn->real_escape_string($_REQUEST['id']);
    $sql = "SELECT d.*, b.name as pumpname, emp.name as empname from dairy.diesel d
left join dairy.diesel_entry e on e.id=d.id and e.unq_id = d.unq_id
left join dairy.diesel_pump_branch b on b.code = e.card 
left join rrpl_database.emp_attendance emp on emp.code = d.branch_user
where d.id='$id'";
    if ($conn->query($sql) === FALSE) {
        echo mysqli_error($conn);
        exit();                  
    }
    $row = $conn->query($sql)->fetch_assoc();
?>
<style type="text/css"> 
   .modal-backdrop
   {
   opacity:0.9 !important;
   background: #e9ecef;
   }

	#appenddiv, #appenddiv2 {
		display: block; 
		position:relative
	} 
	.ui-autocomplete {
		position: absolute;
	}
</style>

<script type="text/javascript">
  $(function() {
  $("#newveh").autocomplete({
  source: 'tank_vehicle_auto.php',
  appendTo: '#appenddiv',
  select: function (event, ui) { 
         $('#newveh').val(ui.item.value);   
         return false;
  },
  change: function (event, ui) {
  if(!ui.item){
	  $(event.target).val("");
	  Swal.fire({
	  icon: 'error',
	  title: 'Error !!!',
	  text: 'Vehicle does not exists !'
	  })
	  $("#newveh").val("");
	  $("#newveh").focus();
  }
  }, 
  focus: function (event, ui){
  return false;
  }
  });
  });
</script>
<form method="post" action="" id="AddTrip" role="form" autocomplete="off">
   <input type="hidden" value="<?php echo $id; ?>" name="dbid" oninput="this.value=this.value.replace(/[^a-z A-Z 0-9]/,'')">
   <div class="modal-body">
      <p style="color: #444;"> ADD DIESEL TO TRIP <button type="button" class="close" data-dismiss="modal"> &times; </button> 
      <p style="border-bottom: 1px solid #ccc;"></p>
      </p>
      <div class="row">
         <div class="form-group col-md-4">
         	<label>Date</label>
            <input type="text" oninput="this.value=this.value.replace(/[^a-z A-Z 0-9]/,'')" class="form-control" id="" name="" readonly="" value="<?php echo date('d/m/Y H:i:s', strtotime($row['timestamp'])) ?>">
         </div>
         <div class="form-group col-md-4">
         	<label>pUMP</label>
            <input type="text" oninput="this.value=this.value.replace(/[^a-z A-Z 0-9]/,'')" class="form-control" id="" name="" readonly="" value="<?php echo $row["pumpname"] ?>">
         </div>
         <div class="form-group col-md-4">
         	<label>Vehicle</label>
            <input type="text" oninput="this.value=this.value.replace(/[^a-z A-Z 0-9]/,'')" class="form-control" id="" name="" readonly="" value="<?php echo $row["tno"] ?>">
         </div>
         <div class="form-group col-md-4">
         	<label>User</label>
            <input type="text" oninput="this.value=this.value.replace(/[^a-z A-Z 0-9]/,'')" class="form-control" id="" name="" readonly="" value="<?php echo $row["empname"] ?>">
         </div>
         <div class="form-group col-md-4">
         	<label>Quantity</label>
            <input type="text" oninput="this.value=this.value.replace(/[^a-z A-Z 0-9]/,'')" class="form-control" id="" name="" readonly="" value="<?php echo $row["qty"] ?>">
         </div>
        
        <?php
            
              $trucktype = 'RRPL Vehicle';

              //start - stock balance availability
              $sql = "select * from dairy.diesel_pump_branch where trim(name)='".trim($row['pumpname'])."'";
              if ($conn->query($sql) === FALSE) {
                  echo mysqli_error($conn);
                  exit();                  
              }
              $res = $conn->query($sql)->fetch_assoc();
              $pump_code = $res['code'];

              $sql = "SELECT COALESCE(SUM(balance),0) as balance FROM dairy.diesel_pump_stock WHERE pumpcode='$pump_code' and status='0'";
              if ($conn->query($sql) === FALSE) {
                    echo mysqli_error($conn);
                    exit();                  
              }
              $res = $conn->query($sql)->fetch_assoc();
              $pump_balance = $res['balance'];


              $sql = "select * from dairy.trip where tno='".preg_replace("/[^0-9a-zA-Z]/", "", $row["tno"])."' and (status='0' or active_trip='1')";
              if ($conn->query($sql) === FALSE) {
                    echo mysqli_error($conn);
                    exit();                  
              }
              $trip_result = $conn->query($sql);

              $sql = "select * from diesel_api.cons_dsl where tno='".preg_replace("/[^0-9a-zA-Z]/", "", $row["tno"])."' and done='0'";
              if ($conn->query($sql) === FALSE) {
                    echo mysqli_error($conn);
                    exit();                  
              }
              $scpl_result = $conn->query($sql);

        ?>
        <input type="hidden" value="<?php echo $trucktype; ?>" name="" oninput="this.value=this.value.replace(/[^a-z A-Z 0-9]/,'')">



    
        
        <?php
            if($trucktype=='RRPL Vehicle'){
        ?>
        <div class="form-group col-md-4">
            <label>Trip </label>
            <select class="form-control" required="" name="tripno">
              <option value=""> -- select -- </option>
              <?php
                while ($data = $trip_result->fetch_assoc()) {
                  echo "<option value='$data[id]_$data[trip_no]'> $data[from_station] to $data[to_station] ($data[id]) </option>";
                }
              ?>
            </select>
         </div>
         <?php } ?>

   

      </div>
   </div>
   <div class="modal-footer" style="">
  <button type="button" id="hidemodal" class="btn btn-danger" data-dismiss="modal">CLOSE</button>
            <?php
            echo '<input type="submit" id="formbtn" class="btn btn-success" name="submit" value="SAVE" />';
            ?>
     
   </div>
</form>