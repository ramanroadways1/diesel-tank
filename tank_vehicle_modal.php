<?php
   require('connect.php'); 
    $id = $conn->real_escape_string($_REQUEST['id']);
    $sql = "select * from diesel_api.dplus_supplies where dbid='$id'";
    $row = $conn->query($sql)->fetch_assoc();
?>
<style type="text/css"> 
   .modal-backdrop
   {
   opacity:0.9 !important;
   background: #e9ecef;
   }

	#appenddiv, #appenddiv2 {
		display: block; 
		position:relative
	} 
	.ui-autocomplete {
		position: absolute;
	}
</style>

<script type="text/javascript">
  $(function() {
  $("#newveh").autocomplete({
  source: 'tank_vehicle_auto.php',
  appendTo: '#appenddiv',
  select: function (event, ui) { 
         $('#newveh').val(ui.item.value);   
         return false;
  },
  change: function (event, ui) {
  if(!ui.item){
	  $(event.target).val("");
	  Swal.fire({
	  icon: 'error',
	  title: 'Error !!!',
	  text: 'Vehicle does not exists !'
	  })
	  $("#newveh").val("");
	  $("#newveh").focus();
  }
  }, 
  focus: function (event, ui){
  return false;
  }
  });
  });
</script>
<form method="post" action="" id="updateTruck" role="form" autocomplete="off">
   <input type="hidden" value="<?php echo $id; ?>" name="dbid" oninput="this.value=this.value.replace(/[^a-z A-Z 0-9]/,'')">
   <div class="modal-body">
      <p style="color: #444;"> UPDATE VEHICLE <button type="button" class="close" data-dismiss="modal"> &times; </button> 
      <p style="border-bottom: 1px solid #ccc;"></p>
      </p>
      <div class="row">
         <div class="form-group col-md-4">
         	<label>Date</label>
            <input type="text" oninput="this.value=this.value.replace(/[^a-z A-Z 0-9]/,'')" class="form-control" id="" name="" readonly="" value="<?php echo date('d/m/Y H:i:s', strtotime($row['datetime'])) ?>">
         </div>
         <div class="form-group col-md-4">
         	<label>Tanker</label>
            <input type="text" oninput="this.value=this.value.replace(/[^a-z A-Z 0-9]/,'')" class="form-control" id="" name="" readonly="" value="<?php echo $row["tank_name"] ?>">
         </div>
         <div class="form-group col-md-4">
         	<label>Vehicle</label>
            <input type="text" oninput="this.value=this.value.replace(/[^a-z A-Z 0-9]/,'')" class="form-control" id="" name="" readonly="" value="<?php echo $row["vehicle_name"] ?>">
         </div>
         <div class="form-group col-md-4">
         	<label>Operator</label>
            <input type="text" oninput="this.value=this.value.replace(/[^a-z A-Z 0-9]/,'')" class="form-control" id="" name="" readonly="" value="<?php echo $row["personnel_name"] ?>">
         </div>
         <div class="form-group col-md-4">
         	<label>Quantity</label>
            <input type="text" oninput="this.value=this.value.replace(/[^a-z A-Z 0-9]/,'')" class="form-control" id="" name="" readonly="" value="<?php echo $row["quantity"] ?>">
         </div>
         <div class="form-group col-md-4">
         	<label>New Vehicle</label>
            <input type="text" oninput="this.value=this.value.replace(/[^a-z A-Z 0-9]/,'')"  class="form-control" id="newveh" name="truckno" required="">
			      <div id="appenddiv"></div>
         </div>
      </div>
   </div>
   <div class="modal-footer">
      <button type="button" id="hidemodal" class="btn btn-danger" data-dismiss="modal">CLOSE</button>
   	  <?php if(substr($row['vehicle_name'], -4)=="0000"){
   	  	echo '<input type="submit" id="" class="btn btn-success" name="submit" value="SAVE" />';
   	  } ?>
   
   </div>
</form>