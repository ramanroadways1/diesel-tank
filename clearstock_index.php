<?php 
include_once 'header.php';
?>  

<script type="text/javascript" src="https://cdn.datatables.net/1.10.19/js/jquery.dataTables.min.js"></script>
<script type="text/javascript" src="https://cdn.datatables.net/buttons/1.5.6/js/dataTables.buttons.min.js"></script>
<script type="text/javascript" src="https://cdn.datatables.net/buttons/1.5.6/js/buttons.flash.min.js"></script>
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/jszip/3.1.3/jszip.min.js"></script>
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.53/vfs_fonts.js"></script> 
<script type="text/javascript" src="https://cdn.datatables.net/buttons/1.5.6/js/buttons.html5.min.js"></script>
<script type="text/javascript" src="https://cdn.datatables.net/buttons/1.5.6/js/buttons.print.min.js"></script>
<script type="text/javascript" src="https://cdn.datatables.net/searchbuilder/1.0.0/js/dataTables.searchBuilder.min.js"></script>
<link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/searchbuilder/1.0.0/css/searchBuilder.dataTables.min.css">

<style type="text/css">
   #user_data_paginate{background-color:#fff}a.dt-button,button.dt-button,div.dt-button{padding:.2em 1em}div.dt-button-collection{max-height:300px;overflow-y:scroll}.dataTables_scroll{margin-bottom:20px}.table{margin:0!important}.applyBtn{border-radius:0!important}table.table-bordered.dataTable td{padding:10px 5px 10px 10px}.dt-buttons{float:right!important}.user_data_filter{float:right}.dt-button{padding:5px 20px;text-transform:uppercase;font-size:12px;text-align:center;cursor:pointer;outline:0;color:#fff;background-color:#37474f;border:none;border-radius:2px;box-shadow:0 4px #999}.dt-button:hover{background-color:#3e8e41}.dt-button:active{background-color:#3e8e41;box-shadow:0 5px #666;transform:translateY(4px)}#user_data_wrapper{width:100%!important}.dt-buttons{margin-bottom:20px}#appenddiv,#appenddiv2{display:block;position:relative}.ui-autocomplete{position:absolute}.table-hover tbody tr:hover td,.table-hover tbody tr:hover th{background-color: #FFF5D7}.table td{vertical-align:middle!important;font-size:11px!important;color:#000;font-family:Verdana,Geneva,sans-serif;padding-top:4px;padding-right:4px;padding-bottom:4px;padding-left:10px}.table-bordered td{border:3px solid #e3e6f0}#user_data_info,#user_data_length{float:left}#user_data_filter,#user_data_paginate{float:right}.paginate_button{color:#000;float:left;padding:6px 12px;text-decoration:none;border:1px solid #ccc;cursor:pointer}.ellipsis{display:none}[type=search]{margin-right:10px;width:250px}.ui-autocomplete{z-index:2150000000!important}.container input{position:absolute;opacity:0;cursor:pointer;height:0;width:0}.checkmark{border-radius:2px;position:absolute;top:0;height:20px;width:20px;background-color:#fff;border:1px solid #000}.container:hover input~.checkmark{background-color:#fff}.container input:checked~.checkmark{background-color:#fff}.container input:disabled~.checkmark{background-color:#eaecf4}.checkmark:after{content:"";position:absolute;display:none}.container input:checked~.checkmark:after{display:block}.container .checkmark:after{left:6px;top:-1px;width:8px;height:16px;border:solid #000;border-width:0 3px 3px 0;-webkit-transform:rotate(45deg);-ms-transform:rotate(45deg);transform:rotate(45deg)}button:disabled,button[disabled]{border:1px solid #333!important;color:#333!important;cursor:no-drop}.table .thead-light th{text-align:center;font-size:11px;color:#444}.component{display:none}table{width:100%!important}table.table-bordered.dataTable td{white-space:nowrap;overflow:hidden;text-overflow:ellipsis}.table .thead-light th{text-transform:uppercase!important}label{text-transform:uppercase}#appenddivbill,#appenddivbillparty,#appenddivcons,#appenddivdo,#appenddivfrom,#appenddivinv,#appenddivitem,#appenddivlr,#appenddivship,#appenddivtno,#appenddivto{display:block;position:relative}.ui-autocomplete{position:absolute}.card label{color:#444}.card label{color:#444} .content{padding-bottom: 0px !important;}
   .main-panel>.content{
    padding: 0 20px 20px;
   }
   table.table-bordered.dataTable td{
    padding: 5px 5px 5px 10px;
   }
   #user_data2_info,#user_data2_length{float:left}#user_data2_filter,#user_data2_paginate{float:right}
   .user_data2_filter{float:right}
   #user_data2_wrapper{width:100%!important}
   #user_data2_paginate{background-color:#fff}

   #user_data3_info,#user_data3_length{float:left}#user_data3_filter,#user_data3_paginate{float:right}
   .user_data3_filter{float:right;}
   #user_data3_wrapper{width:100%!important}
   #user_data3_paginate{background-color:#fff}

   table.table-bordered.dataTable td{
    text-align: center;
   }

.noselect {
    cursor: default;
    -webkit-touch-callout: none;
    -webkit-user-select: none;
    -khtml-user-select: none;
    -moz-user-select: none;
    -ms-user-select: none;
    user-select: none;
}
</style>

<script type="text/javascript">
  $(function() {
    $("#hsd1").autocomplete({
      source: 'clearstock_getpump.php',
      appendTo: '#appenddiv',
      select: function (event, ui) { 
         $('#hsd1').val(ui.item.value);   
         $('#hsd2').val(ui.item.balance);
         $('#hsd4').val(ui.item.pumpcode);      
         return false;
      },
      change: function (event, ui) {
        if(!ui.item){
          $(event.target).val("");
          Swal.fire({
            icon: 'error',
            title: 'Error !!!',
            text: 'Pump does not exists !'
          })
          $("#hsd2").val("");
          $("#hsd4").val("");
          $("#hsd1").val("");
          $("#hsd1").focus();
        }
      }, 
      focus: function (event, ui){
        return false;
      }
    });
  });
</script>


<div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="memberModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content"> 
            <div class="dash" id="modaldetail"> 
            </div> 
        </div>
    </div>
</div>

<div id="content-wrapper" class="d-flex flex-column"> 
<div id="content">

<div id="updatereq_status"></div> 
 <div class="container-fluid"> 
<div class="row"> 

<div class="col-md-12" >
<div class="card shadow mb-4" style="border-radius: 0px !important;"> 
<div class="card-header">
 

  
 
<form method="post" action="" id="UploadBill" autocomplete="off" enctype='multipart/form-data'> 
<div class="col-md-10 offset-md-1" >
<div class="" style="padding-top: 5px; padding-bottom: 10px;">
   <h6> CLOSE PENDING STOCK  </h6> 
</div>
<div class="card-body "  style="background-color: #fff; border: 1px solid #ccc;">
 
  <div class="row" >

	<div class="col-md-3">
      <label> PUMP</label>
      <input type="text" id="hsd1" name="hsdpump" class="form-control" oninput="this.value=this.value.replace(/[^a-z A-Z 0-9]/,'')" required="">
      <div id="appenddiv"></div>
    </div>

	<div class="col-md-2">
      <label>PUMP Code</label>
      <input type="text" name="pumpcode"  id="hsd4" class="form-control" oninput="this.value=this.value.replace(/[^0-9.]/,'')" readonly="">
    </div> 
    <div class="col-md-2">
      <label>PUMP Balance</label>
      <input type="text" id="hsd2" name="hsdbal" class="form-control" oninput="this.value=this.value.replace(/[^0-9.]/,'')" readonly="">
    </div>
    
    <div class="col-md-2">
     	<label> </label>
  		<button class="btn btn-warning" type="submit" style="margin-top: 20px;">  <i class='fa fa-random'></i> SEND OTP </button>
    </div>

   </div>



</div>
</form> 

<br> 

</div> 
</div>
</div> 
 
</div>
</div> 
</div>
</div>
<div id="response"></div>

<script> 

	$("form#UploadBill").submit(function(e) {
	    e.preventDefault();    
	    var formData = new FormData(this);
		$('#loadicon').show(); 
	    $.ajax({
	        url: 'clearstock_next.php',
	        type: 'POST',
	        data: formData,
	        success: function (data) {
	            $('#response').html(data);  
				$('#loadicon').hide(); 
				$("#UploadBill")[0].reset(); 
	        },
	        cache: false,
	        contentType: false,
	        processData: false
	    });
	});


</script>

<?php 
  include_once 'footer.php';
?>  
 