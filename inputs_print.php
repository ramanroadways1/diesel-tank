<?php 
include_once 'header.php';

$p = $_REQUEST['p'];
?>  

<script type="text/javascript" src="https://cdn.datatables.net/1.10.19/js/jquery.dataTables.min.js"></script>
<script type="text/javascript" src="https://cdn.datatables.net/buttons/1.5.6/js/dataTables.buttons.min.js"></script>
<script type="text/javascript" src="https://cdn.datatables.net/buttons/1.5.6/js/buttons.flash.min.js"></script>
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/jszip/3.1.3/jszip.min.js"></script>
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.53/vfs_fonts.js"></script> 
<script type="text/javascript" src="https://cdn.datatables.net/buttons/1.5.6/js/buttons.html5.min.js"></script>
<script type="text/javascript" src="https://cdn.datatables.net/buttons/1.5.6/js/buttons.print.min.js"></script>
<script type="text/javascript" src="https://cdn.datatables.net/searchbuilder/1.0.0/js/dataTables.searchBuilder.min.js"></script>
<link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/searchbuilder/1.0.0/css/searchBuilder.dataTables.min.css">

<style type="text/css">
   #user_data_paginate{background-color:#fff}a.dt-button,button.dt-button,div.dt-button{padding:.2em 1em}div.dt-button-collection{max-height:300px;overflow-y:scroll}.dataTables_scroll{margin-bottom:20px}.table{margin:0!important}.applyBtn{border-radius:0!important}table.table-bordered.dataTable td{padding:10px 5px 10px 10px}.dt-buttons{float:right!important}.user_data_filter{float:right}.dt-button{padding:5px 20px;text-transform:uppercase;font-size:12px;text-align:center;cursor:pointer;outline:0;color:#fff;background-color:#37474f;border:none;border-radius:2px;box-shadow:0 4px #999}.dt-button:hover{background-color:#3e8e41}.dt-button:active{background-color:#3e8e41;box-shadow:0 5px #666;transform:translateY(4px)}#user_data_wrapper{width:100%!important}.dt-buttons{margin-bottom:20px}#appenddiv,#appenddiv2{display:block;position:relative}.ui-autocomplete{position:absolute}.table-hover tbody tr:hover td,.table-hover tbody tr:hover th{background-color: #FFF5D7}.table td{vertical-align:middle!important;font-size:11px!important;color:#000;font-family:Verdana,Geneva,sans-serif;padding-top:4px;padding-right:4px;padding-bottom:4px;padding-left:10px}.table-bordered td{border:3px solid #e3e6f0}#user_data_info,#user_data_length{float:left}#user_data_filter,#user_data_paginate{float:right}.paginate_button{color:#000;float:left;padding:6px 12px;text-decoration:none;border:1px solid #ccc;cursor:pointer}.ellipsis{display:none}[type=search]{margin-right:10px;width:250px}.ui-autocomplete{z-index:2150000000!important}.container input{position:absolute;opacity:0;cursor:pointer;height:0;width:0}.checkmark{border-radius:2px;position:absolute;top:0;height:20px;width:20px;background-color:#fff;border:1px solid #000}.container:hover input~.checkmark{background-color:#fff}.container input:checked~.checkmark{background-color:#fff}.container input:disabled~.checkmark{background-color:#eaecf4}.checkmark:after{content:"";position:absolute;display:none}.container input:checked~.checkmark:after{display:block}.container .checkmark:after{left:6px;top:-1px;width:8px;height:16px;border:solid #000;border-width:0 3px 3px 0;-webkit-transform:rotate(45deg);-ms-transform:rotate(45deg);transform:rotate(45deg)}button:disabled,button[disabled]{border:1px solid #333!important;color:#333!important;cursor:no-drop}.table .thead-light th{text-align:center;font-size:11px;color:#444}.component{display:none}table{width:100%!important}table.table-bordered.dataTable td{white-space:nowrap;overflow:hidden;text-overflow:ellipsis}.table .thead-light th{text-transform:uppercase!important}label{text-transform:uppercase}#appenddivbill,#appenddivbillparty,#appenddivcons,#appenddivdo,#appenddivfrom,#appenddivinv,#appenddivitem,#appenddivlr,#appenddivship,#appenddivtno,#appenddivto{display:block;position:relative}.ui-autocomplete{position:absolute}.card label{color:#444}.card label{color:#444} .content{padding-bottom: 0px !important;}
   .main-panel>.content{
    padding: 0 20px 20px;
   }
   table.table-bordered.dataTable td{
    /*padding: 5px 5px 5px 10px;*/
   }
   #user_data2_info,#user_data2_length{float:left}#user_data2_filter,#user_data2_paginate{float:right}
   .user_data2_filter{float:right}
   #user_data2_wrapper{width:100%!important}
   #user_data2_paginate{background-color:#fff}

   #user_data3_info,#user_data3_length{float:left}#user_data3_filter,#user_data3_paginate{float:right}
   .user_data3_filter{float:right;}
   #user_data3_wrapper{width:100%!important}
   #user_data3_paginate{background-color:#fff}

   table.table-bordered.dataTable td{
    text-align: center;
   }

.noselect {
    cursor: default;
    -webkit-touch-callout: none;
    -webkit-user-select: none;
    -khtml-user-select: none;
    -moz-user-select: none;
    -ms-user-select: none;
    user-select: none;
}
td:last-child{
    text-align: center;
}
</style>
<div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="memberModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content"> 
            <div class="dash"> 
            </div> 
        </div>
    </div>
</div>

<div id="content-wrapper" class="d-flex flex-column"> 
<div id="content">
<div class="mb-4"></div>

<div id="updatereq_status"></div> 
 <div class="container-fluid"> 
<div class="row"> 

<div class="col-md-8 offset-md-2 " >
<div class="card shadow mb-4"> 
<div class="card-header py-3" style="display: none;">
<h6 class="m-0 font-weight-bold text-dark" style="font-family: Verdana, Geneva, sans-serif; font-weight: normal; text-transform: uppercase;"> <span style="color: maroon;">NEGATIVE</span> BALANCE CARDS </h6>
</div>
<div class="card-body">


	<table id="user_data" class="table table-bordered table-hover" style="cursor: pointer;">
			<thead class="thead-light">
				<tr>
        <!-- <th style="font-size: 11px; color:#444; text-align: center;"> DATE </th> -->
        <th style="font-size: 11px; color:#444; text-align: center;"> DATE </th>
        <th style="font-size: 11px; color:#444; text-align: center;"> DATE </th>
        <th style="font-size: 11px; color:#444; text-align: center;"> BRANCH </th>
        <th style="font-size: 11px; color:#444; text-align: center;"> PUMP </th>
        <th style="font-size: 11px; color:#444; text-align: center;"> COMPANY </th>
        <th style="font-size: 11px; color:#444; text-align: center;"> TRUCKNO </th> 
        <th style="font-size: 11px; color:#444; text-align: center;"> QTY </th> 
        <th style="font-size: 11px; color:#444; text-align: center;"> RATE </th> 
        <th style="font-size: 11px; color:#444; text-align: center;"> AMOUNT </th> 
				</tr>
			</thead>
			<tfoot class="thead-light">
				<tr>
        <!-- <th style="font-size: 11px; color:#444; text-align: center;"> DATE </th> -->
        <th style="font-size: 11px; color:#444; text-align: center;">  </th>
        <th style="font-size: 11px; color:#444; text-align: center;">  </th>
        <th style="font-size: 11px; color:#444; text-align: center;">  </th>
        <th style="font-size: 11px; color:#444; text-align: center;">  </th>
        <th style="font-size: 11px; color:#444; text-align: center;">  </th>
        <th style="font-size: 11px; color:#444; text-align: center;">  </th> 
        <th style="font-size: 11px; color:#444; text-align: center;">  </th> 
        <th style="font-size: 11px; color:#444; text-align: center;">  </th> 
        <th style="font-size: 11px; color:#444; text-align: center;">  </th> 
				</tr>
			</tfoot>
    
</div>
</div>
</div> 
</div>
</div>
</div>
</div>


<div id="dataModal" class="modal fade">  
      <div class="modal-dialog  modal-lg">  
           <div class="modal-content" id="employee_detail">   
           </div>  
      </div>  
 </div>   
 
<script type="text/javascript">

	$(document).ready(function() {
    var table = $('#user_data').DataTable({
        "columnDefs": [
            { "visible": false, "targets": 1 }
        ],
		"sAjaxSource": "inputs_print_fetch.php?p=<?php echo $p; ?>",
		  "bPaginate": false,
		 "bProcessing": true,
"dom": 'Bfrtip',
    "ordering": false,
    "buttons": [
        'copy', 'csv', 'excel', 'print'
    ],
        "order": [[ 1, 'asc' ]],
        "displayLength": 25,
         "footerCallback": function ( row, data, start, end, display ) {
            var api = this.api(), data;
 
            // Remove the formatting to get integer data for summation
            var intVal = function ( i ) {
                return typeof i === 'string' ?
                    i.replace(/[\$,]/g, '')*1 :
                    typeof i === 'number' ?
                        i : 0;
            };
 
            // Total over all pages
            total = api
                .column( 6 )
                .data()
                .reduce( function (a, b) {
                    return intVal(a) + intVal(b);
                }, 0 );
 
            // Total over this page
            pageTotal = api
                .column( 6, { page: 'current'} )
                .data()
                .reduce( function (a, b) {
                    return intVal(a) + intVal(b);
                }, 0 );
 
            // Update footer
            $( api.column( 6 ).footer() ).html(
                ''+pageTotal.toFixed(2) +''
                // '$'+pageTotal +' ( $'+ total +' total)'
            );


		// Total over all pages
            total = api
                .column( 8 )
                .data()
                .reduce( function (a, b) {
                    return intVal(a) + intVal(b);
                }, 0 );
 
            // Total over this page
            pageTotal = api
                .column( 8, { page: 'current'} )
                .data()
                .reduce( function (a, b) {
                    return intVal(a) + intVal(b);
                }, 0 );
 
            // Update footer
            $( api.column( 8 ).footer() ).html(
                ''+pageTotal.toFixed(2) +''
                // '$'+pageTotal +' ( $'+ total +' total)'
            );
        },
        "drawCallback": function ( settings ) {
            var api = this.api();
            var rows = api.rows( {page:'current'} ).nodes();
            var last=null;
            var subTotal = new Array();
            var groupID = -1;
            var aData = new Array();
            var index = 0;
            
            api.column(1, {page:'current'} ).data().each( function ( group, i ) {
            	
              // console.log(group+">>>"+i);
            
              var vals = api.row(api.row($(rows).eq(i)).index()).data();
              var salary = vals[6] ? parseFloat(vals[6]) : 0;
              var salarynew = vals[8] ? parseFloat(vals[8]) : 0;
               
              if (typeof aData[group] == 'undefined') {
                 aData[group] = new Array();
                 aData[group].rows = [];
                 aData[group].salary = [];
		 aData[group].salarynew = [];
              }
          
           		aData[group].rows.push(i); 
			aData[group].salary.push(salary); 
			aData[group].salarynew.push(salarynew); 
                
            } );
    

            var idx= 0;

      
          	for(var office in aData){
       	
	           idx =  Math.max.apply(Math,aData[office].rows);
      
                   var sum = 0; 
                   $.each(aData[office].salary,function(k,v){
                        sum = sum + v;
                   });

                   var sumnew = 0; 
                   $.each(aData[office].salarynew,function(k,v){
                        sumnew = sumnew + v;
                   });

  									//console.log(aData[office].salary);
                   $(rows).eq( idx ).after(
                        '<tr style="background-color: #FAFAFC;" class="group"><td colspan="4"> </td><td style=""> '+office+' </td>'+
                        '<td>'+sum.toFixed(2)+'</td> <td> </td><td>'+sumnew.toFixed(2)+'</td></tr>'
                    );
                    
            };

        }
    } );

} ); 
</script>  
<?php 
include_once 'footer.php';
?> 