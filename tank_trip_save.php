<?php
   	
	require('connect.php'); 
 
    $dbid = $conn->real_escape_string($_POST['dbid']);
	$tripno = $conn->real_escape_string(explode("_",$_POST['tripno'])[0]);
	$tripno_new = $conn->real_escape_string(explode("_",$_POST['tripno'])[1]);
	
	$timestamp = date("Y-m-d H:i:s");
	
  
try {
	$conn->query("START TRANSACTION"); 
  
			$sql = "update dairy.diesel set trip_id='$tripno',trip_no='$tripno_new' where id='$dbid'";
			if ($conn->query($sql) === FALSE) {
					throw new Exception(mysqli_error($conn)." Code 001");             
			}

			$sql = "select * from dairy.diesel where id='$dbid'";
			if ($conn->query($sql) === FALSE) {
					throw new Exception(mysqli_error($conn)." Code 002");             
			}
			$dsl = $conn->query($sql)->fetch_assoc();
			$qty = $dsl['qty'];
			$amount = $dsl['amount'];

///////////////start - Trip Quantity Update

				$sql = "select count(id) as rowcount,trip_no, COALESCE(SUM(diesel_qty),0) as diesel_qty, COALESCE(SUM(diesel),0) as diesel from dairy.trip where id='$tripno' and (status='0' or active_trip='1')";	 
				if ($conn->query($sql) === FALSE) {
						throw new Exception(mysqli_error($conn)." Code 003");             
				}
				$trp = $conn->query($sql)->fetch_assoc();
  
				$trip_qty = sprintf("%.2f", $trp['diesel_qty'] + $qty);
				$trip_amount =  sprintf("%.2f", $trp['diesel'] + $amount);
				$trip_no_new = $trp['trip_no'];
				
				if($trp['rowcount'] > 0){

					$sql = "update dairy.trip set diesel='$trip_amount', diesel_qty='$trip_qty' where id='$tripno'"; 
					if ($conn->query($sql) === FALSE) {
						throw new Exception(mysqli_error($conn)." Code 004");             
	         		}

         		} else{
					throw new Exception("Running trip does not exist");             
         		}

				$sql = "select sum(qty) as qty, sum(amount) as amount from dairy.diesel where trip_id='$tripno'";	
				if ($conn->query($sql) === FALSE) {
					throw new Exception(mysqli_error($conn)." Code 005");             
				}
				$res = $conn->query($sql)->fetch_assoc();

				if($res['qty']!=$trip_qty or $res['amount']!=$trip_amount){
					throw new Exception("Mismatched Diesel Qty or Amount in Trip $tripno");             
				}


///////////////end - Trip Quantity Update

	$conn->query("COMMIT");
	echo "
	<script>
	Swal.fire({
	position: 'top-end',
	icon: 'success',
	title: 'Updated Successfully',
	showConfirmButton: false,
	timer: 1000
	})
	</script>";

} catch(Exception $e) { 

	$conn->query("ROLLBACK"); 
	$content = $e->getMessage();
	$content = preg_replace("/[^0-9a-zA-Z_\.\- ]/", "", $content);  
	echo "
	<script>
	Swal.fire({
	icon: 'error',
	title: 'Error !!!',
	text: '$content'
	})
	</script>";		
} 