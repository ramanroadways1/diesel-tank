<?php
 require('connect.php');

    $tanker = $conn->real_escape_string($_POST['tanker']);
    $fuel = $conn->real_escape_string($_POST['fuel']);
    $total = $conn->real_escape_string($_POST['total']);
    $hsdpump = $conn->real_escape_string($_POST['hsdpump']);
    $hsdbal = $conn->real_escape_string($_POST['hsdbal']);
    $hsdqty = $conn->real_escape_string($_POST['hsdqty']);
    $adipump = $conn->real_escape_string($_POST['adipump']);
    $adibal = $conn->real_escape_string($_POST['adibal']);
    $adiqty = $conn->real_escape_string($_POST['adiqty']);
    $hsdcode = $conn->real_escape_string($_POST['hsdcode']);
    $adicode = $conn->real_escape_string($_POST['adicode']);
    $dd = $conn->real_escape_string($_POST['dd']);
    $hh = $conn->real_escape_string($_POST['hh']);
    $mm = $conn->real_escape_string($_POST['mm']);
 
        $getDD = strtotime("$dd $hh:$mm:05");
        $inv1 = date("Y-m-d", strtotime('-20 minutes', $getDD));
        $inv2 = date("H:i:s", strtotime('-20 minutes', $getDD));
        $ent1 = date("Y-m-d", strtotime('-15 minutes', $getDD));
        $ent2 = date("H:i:s", strtotime('-15 minutes', $getDD));

        $invDate =  $inv1."T".$inv2."Z";
        $entDate =  $ent1."T".$ent2."Z";

try {
    $conn->query("START TRANSACTION"); 
        
        // if(true){
        //     throw new Exception("$dd $hh $mm");    
        // }

        $multiTank = False;

        $sql = "select * from dairy.diesel_pump_stock where status='0' and pumpcode in (SELECT code FROM dairy.diesel_pump_branch where admin='$branch_name' and billtype='2')";
        if($conn->query($sql) === FALSE) {
                    throw new Exception(mysqli_error($conn)." Code 001");             
        }
        if($conn->query($sql)->num_rows>0){
                    throw new Exception("Please clear all pending stocks !");             
        }

///////////////start - get balance from main stocks
       $sql = "select COALESCE(SUM(balance),0) as balance from dairy.diesel_pump_stock where pumpcode='$hsdcode' and status='0' and adi='0' group by pumpcode";
      if ($conn->query($sql) === FALSE) {
            throw new Exception(mysqli_error($conn)." Code 002");             
      }
      $res = $conn->query($sql)->fetch_assoc();
      $hsdbal = $res['balance'];

      if(($fuel=='HSD' or $fuel=='MIX') and $hsdqty>$hsdbal){
            throw new Exception("HSD qty is greater than Pump HSD Balance");             
      }

      $sql = "select COALESCE(SUM(balance),0) as balance from dairy.diesel_pump_stock where pumpcode='$adicode' and status='0' and adi='1' group by pumpcode";
      if ($conn->query($sql) === FALSE) {
            throw new Exception(mysqli_error($conn)." Code 003");             
      }
      $res = $conn->query($sql)->fetch_assoc();
      $adibal = $res['balance'];
      
      if(($fuel=='ADI' or $fuel=='MIX') and $adiqty>$adibal){
            throw new Exception("ADI qty is greater than Pump ADI Balance");             
      }
///////////////end - get balance from main stocks

                $sql = "select id+1 as id from dairy.diesel_pump_stock ORDER by id desc limit 1";
                if($conn->query($sql) === FALSE) {
                        throw new Exception(mysqli_error($conn)." Code 004");             
                }
                $res = $conn->query($sql)->fetch_assoc();
                $SetMaster = $res['id'];

/////////////// Fuel Type HSD *********************************************
      if($fuel=='HSD' or $fuel=='MIX'){
///////////////start - fuel type single/multiple entry
            $qty = $hsdqty;
            $pump_code = $hsdcode;
            $split_entry = "0";

            $sql = "SELECT id,purchaseid,masterid,totalqty,rate,balance FROM dairy.diesel_pump_stock WHERE pumpcode='$pump_code' and balance>0 and status='0' and adi='0'";
            if ($conn->query($sql) === FALSE) {
                throw new Exception(mysqli_error($conn)." Code 005");             
            } 
            $stockcount = $conn->query($sql)->num_rows;

            if($stockcount==1)
            {
                $res = $conn->query($sql)->fetch_assoc();
                
                if($qty>$res['balance'])
                {
                    throw new Exception("Out of Stock - quantity is greater than stock balance");             
                }
                
                $UpdateStockBalance = sprintf("%.2f",$res['balance']-$qty);
                $StockId = $res['id'];
                $PurchaseId = $res['purchaseid'];
                $StockQty = $res['totalqty'];
                $StockMasterId = $res['masterid'];
                
                $rate = $res['rate'];
                $amount = round($qty*$rate);
                $HSDrate = $rate;
            }
            else if($stockcount==2)
            {
                
                $sql = "SELECT id,purchaseid,masterid,totalqty,rate,balance FROM dairy.diesel_pump_stock WHERE pumpcode='$pump_code' AND balance>0 and status='0' and adi='0' ORDER BY id ASC LIMIT 1";
                if ($conn->query($sql) === FALSE) {
                    throw new Exception(mysqli_error($conn)." Code 006");             
                } 
                $rowFirstPump = $conn->query($sql)->fetch_assoc();

                $sql = "SELECT id,purchaseid,masterid,totalqty,rate,balance FROM dairy.diesel_pump_stock WHERE pumpcode='$pump_code' AND balance>0 and status='0' and adi='0' ORDER BY id DESC LIMIT 1";
                if ($conn->query($sql) === FALSE) {
                    throw new Exception(mysqli_error($conn)." Code 007");             
                } 
                $rowSecondPump = $conn->query($sql)->fetch_assoc();
                 
                if(($rowFirstPump['balance']+$rowSecondPump['balance'])<$qty)
                {
                    throw new Exception("Out of Stock - quantity is greater than stock balance");             
                }
                
                if($rowFirstPump['balance']>=$qty)
                {
                    $rate = $rowFirstPump['rate'];
                    $amount = round($qty*$rate);
                    $UpdateStockBalance = sprintf("%.2f",$rowFirstPump['balance']-$qty);
                    $StockId = $rowFirstPump['id'];
                    $PurchaseId = $rowFirstPump['purchaseid'];
                    $StockQty = $rowFirstPump['totalqty'];
                    $StockMasterId = $rowFirstPump['masterid'];
                    $HSDrate = $rate;
                }
                else
                {
                    $split_entry="1";
                    
                    $rate = $rowFirstPump['rate'];
                    $rate2 = $rowSecondPump['rate'];
                    $qty1 = $rowFirstPump['balance'];
                    $qty2 = sprintf("%.2f",$qty-$qty1);
                    $amount = round($rate * $qty1);
                    $amount2 = round($rate2 * $qty2);
                    $StockId = $rowFirstPump['id'];
                    $StockId2 = $rowSecondPump['id'];
                    $UpdateStockBalance = sprintf("%.2f", 0);
                    $UpdateStockBalance2 = sprintf("%.2f",$rowSecondPump['balance']-$qty2);
                    $PurchaseId = $rowFirstPump['purchaseid'];
                    $StockQty = $rowFirstPump['totalqty'];
                    $PurchaseId2 = $rowSecondPump['purchaseid'];
                    $StockQty2 = $rowSecondPump['totalqty'];
                    $StockMasterId = $rowFirstPump['masterid'];
                    $StockMasterId2 = $rowSecondPump['masterid'];
                    $HSDrate = sprintf("%.2f",($rate+$rate2)/2.0);

                }
            }
            else
            {
                throw new Exception("There should be atleast 1 stock and atmost 2 stocks !");             
            }

///////////////end - fuel type single/multiple entry

///////////////start - insert records 
 
            $pid = md5(time()).'HSD';

            if($split_entry=='0'){

                $sql = "insert into dairy.diesel_pump_stock (openuser, purchasedate, purchaseid, purchaseamt, purchaserate, pumpcode, rate, purchaseqty, totalqty, balance, invoice, stamp, vendor, branch, adi, status, parentstock, masterid) values ('$branch_emp' ,now(), '$pid', '$amount', '$rate', '$tanker', '$rate', '$qty', '$qty', '$qty', NULL, now(), '$pump_code', NULL, '0', '0', '$StockId', '$SetMaster')";
                if($conn->query($sql) === FALSE) {
                    throw new Exception(mysqli_error($conn)." Code 008");             
                }

                $sql = "insert into diesel_api.stock_transfer (`refid`, `creditor`, `debitor`, `branch`, `creditor_comp`, `debitor_comp`, `qty`, `rate`, `amount`, `dated`, `stockid`, `timestamp`) values ('$pid','$tanker', '$pump_code', '$branch_name', 'RELIANCE', 'RELIANCE', '$qty', '$rate', '$amount', now(), '$PurchaseId', now())";
                if($conn->query($sql) === FALSE) {
                    throw new Exception(mysqli_error($conn)." Code 009");             
                }

                $sql = "Update dairy.diesel_pump_stock set balance='$UpdateStockBalance' where purchaseid='$PurchaseId'";
                if($conn->query($sql) === FALSE) {
                    throw new Exception(mysqli_error($conn)." Code 010");             
                }

            } else {
                
                $sql = "insert into dairy.diesel_pump_stock (openuser, purchasedate, purchaseid, purchaseamt, purchaserate, pumpcode, rate, purchaseqty, totalqty, balance, invoice, stamp, vendor, branch, adi, status, parentstock, masterid) values ('$branch_emp' ,now(), '$pid', '$amount', '$rate', '$tanker', '$rate', '$qty1', '$qty1', '$qty1', NULL, now(), '$pump_code', NULL, '0', '0', '$StockId', '$SetMaster')";
                if($conn->query($sql) === FALSE) {
                    throw new Exception(mysqli_error($conn)." Code 044");             
                }

                $sql = "insert into diesel_api.stock_transfer (`refid`, `creditor`, `debitor`, `branch`, `creditor_comp`, `debitor_comp`, `qty`, `rate`, `amount`, `dated`, `stockid`, `timestamp`) values ('$pid', '$tanker', '$pump_code', '$branch_name', 'RELIANCE', 'RELIANCE', '$qty1', '$rate', '$amount', now(), '$PurchaseId', now())";
                if($conn->query($sql) === FALSE) {
                    throw new Exception(mysqli_error($conn)." Code 045");             
                }

                $sql = "Update dairy.diesel_pump_stock set balance='$UpdateStockBalance' where purchaseid='$PurchaseId'";
                if($conn->query($sql) === FALSE) {
                    throw new Exception(mysqli_error($conn)." Code 046");             
                }

                $pid = $pid."002";

                $sql = "insert into dairy.diesel_pump_stock (openuser, purchasedate, purchaseid, purchaseamt, purchaserate, pumpcode, rate, purchaseqty, totalqty, balance, invoice, stamp, vendor, branch, adi, status, parentstock, masterid) values ('$branch_emp' ,now(), '$pid', '$amount2', '$rate2', '$tanker', '$rate2', '$qty2', '$qty2', '$qty2', NULL, now(), '$pump_code', NULL, '0', '0', '$StockId2', '$SetMaster')";
                if($conn->query($sql) === FALSE) {
                    throw new Exception(mysqli_error($conn)." Code 011");             
                }

                $sql = "insert into diesel_api.stock_transfer (`refid`, `creditor`, `debitor`, `branch`, `creditor_comp`, `debitor_comp`, `qty`, `rate`, `amount`, `dated`, `stockid`, `timestamp`) values ('$pid', '$tanker', '$pump_code', '$branch_name', 'RELIANCE', 'RELIANCE', '$qty2', '$rate2', '$amount2', now(), '$PurchaseId2', now())";
                if($conn->query($sql) === FALSE) {
                    throw new Exception(mysqli_error($conn)." Code 012");             
                }

                $sql = "Update dairy.diesel_pump_stock set status='1', closeuser='$branch_emp' where purchaseid='$PurchaseId'";
                if($conn->query($sql) === FALSE) {
                    throw new Exception(mysqli_error($conn)." Code 013");             
                }

                $sql = "Update dairy.diesel_pump_stock set balance='$UpdateStockBalance2' where purchaseid='$PurchaseId2'";
                if($conn->query($sql) === FALSE) {
                    throw new Exception(mysqli_error($conn)." Code 014");             
                }
            } 
///////////////end - insert records 
      }

/////////////// Fuel Type ADI *********************************************
      if($fuel=='ADI' or $fuel=='MIX'){
///////////////start - fuel type single/multiple entry
            $qty = $adiqty;
            $pump_code = $adicode;
            $split_entry = "0";

            $sql = "SELECT id,purchaseid,masterid,totalqty,rate,balance FROM dairy.diesel_pump_stock WHERE pumpcode='$pump_code' and balance>0 and status='0' and adi='1'";
            if ($conn->query($sql) === FALSE) {
                throw new Exception(mysqli_error($conn)." Code 015");             
            } 
            $stockcount = $conn->query($sql)->num_rows;

            if($stockcount==1)
            {
                $res = $conn->query($sql)->fetch_assoc();
                
                if($qty>$res['balance'])
                {
                    throw new Exception("Out of Stock - quantity is greater than stock balance");             
                }
                
                $UpdateStockBalance = sprintf("%.2f",$res['balance']-$qty);
                $StockId = $res['id'];
                $PurchaseId = $res['purchaseid'];
                $StockQty = $res['totalqty'];
                $StockMasterId = $res['masterid'];
                
                $rate = $res['rate'];
                $amount = round($qty*$rate);
                $ADIrate = $rate;
            }
            else if($stockcount==2)
            {
                
                $sql = "SELECT id,purchaseid,masterid,totalqty,rate,balance FROM dairy.diesel_pump_stock WHERE pumpcode='$pump_code' AND balance>0 and status='0' and adi='1' ORDER BY id ASC LIMIT 1";
                if ($conn->query($sql) === FALSE) {
                    throw new Exception(mysqli_error($conn)." Code 016");             
                } 
                $rowFirstPump = $conn->query($sql)->fetch_assoc();

                $sql = "SELECT id,purchaseid,masterid,totalqty,rate,balance FROM dairy.diesel_pump_stock WHERE pumpcode='$pump_code' AND balance>0 and status='0' and adi='1' ORDER BY id DESC LIMIT 1";
                if ($conn->query($sql) === FALSE) {
                    throw new Exception(mysqli_error($conn)." Code 017");             
                } 
                $rowSecondPump = $conn->query($sql)->fetch_assoc();
                 
                if(($rowFirstPump['balance']+$rowSecondPump['balance'])<$qty)
                {
                    throw new Exception("Out of Stock - quantity is greater than stock balance");             
                }
                
                if($rowFirstPump['balance']>=$qty)
                {
                    $rate = $rowFirstPump['rate'];
                    $amount = round($qty*$rate);
                    $UpdateStockBalance = sprintf("%.2f",$rowFirstPump['balance']-$qty);
                    $StockId = $rowFirstPump['id'];
                    $PurchaseId = $rowFirstPump['purchaseid'];
                    $StockQty = $rowFirstPump['totalqty'];
                    $StockMasterId = $rowFirstPump['masterid'];
                    $ADIrate = $rate;
                }
                else
                {
                    $split_entry="1";
                    
                    $rate = $rowFirstPump['rate'];
                    $rate2 = $rowSecondPump['rate'];
                    $qty1 = $rowFirstPump['balance'];
                    $qty2 = sprintf("%.2f",$qty-$qty1);
                    $amount = round($rate * $qty1);
                    $amount2 = round($rate2 * $qty2);
                    $StockId = $rowFirstPump['id'];
                    $StockId2 = $rowSecondPump['id'];
                    $UpdateStockBalance = sprintf("%.2f", 0);
                    $UpdateStockBalance2 = sprintf("%.2f",$rowSecondPump['balance']-$qty2);
                    $PurchaseId = $rowFirstPump['purchaseid'];
                    $StockQty = $rowFirstPump['totalqty'];
                    $PurchaseId2 = $rowSecondPump['purchaseid'];
                    $StockQty2 = $rowSecondPump['totalqty'];
                    $StockMasterId = $rowFirstPump['masterid'];
                    $StockMasterId2 = $rowSecondPump['masterid'];
                    $ADIrate = sprintf("%.2f",($rate+$rate2)/2.0);

                }
            }
            else
            {
                throw new Exception("There should be atleast 1 stock and atmost 2 stocks !");             
            }

///////////////end - fuel type single/multiple entry

///////////////start - insert records 

                $pid = md5(time()).'ADI';

            if($split_entry=='0'){

                $sql = "insert into dairy.diesel_pump_stock (openuser, purchasedate, purchaseid, purchaseamt, purchaserate, pumpcode, rate, purchaseqty, totalqty, balance, invoice, stamp, vendor, branch, adi, status, parentstock, masterid) values ('$branch_emp' ,now(), '$pid', '$amount', '$rate', '$tanker', '$rate', '$qty', '$qty', '$qty', NULL, now(), '$pump_code', NULL, '1', '0', '$StockId', '$SetMaster')";
                if($conn->query($sql) === FALSE) {
                    throw new Exception(mysqli_error($conn)." Code 018");             
                }

                $sql = "insert into diesel_api.stock_transfer (`refid`, `creditor`, `debitor`, `branch`, `creditor_comp`, `debitor_comp`, `qty`, `rate`, `amount`, `dated`, `stockid`, `timestamp`) values ('$pid', '$tanker', '$pump_code', '$branch_name', 'RELIANCE', 'RELIANCE', '$qty', '$rate', '$amount', now(), '$PurchaseId', now())";
                if($conn->query($sql) === FALSE) {
                    throw new Exception(mysqli_error($conn)." Code 019");             
                }

                $sql = "Update dairy.diesel_pump_stock set balance='$UpdateStockBalance' where purchaseid='$PurchaseId'";
                if($conn->query($sql) === FALSE) {
                    throw new Exception(mysqli_error($conn)." Code 020");             
                }

            } else {
                
                $sql = "insert into dairy.diesel_pump_stock (openuser, purchasedate, purchaseid, purchaseamt, purchaserate, pumpcode, rate, purchaseqty, totalqty, balance, invoice, stamp, vendor, branch, adi, status, parentstock, masterid) values ('$branch_emp' ,now(), '$pid', '$amount', '$rate', '$tanker', '$rate', '$qty1', '$qty1', '$qty1', NULL, now(), '$pump_code', NULL, '1', '0', '$StockId', '$SetMaster')";
                if($conn->query($sql) === FALSE) {
                    throw new Exception(mysqli_error($conn)." Code 041");             
                }

                $sql = "insert into diesel_api.stock_transfer (`refid`, `creditor`, `debitor`, `branch`, `creditor_comp`, `debitor_comp`, `qty`, `rate`, `amount`, `dated`, `stockid`, `timestamp`) values ('$pid', '$tanker', '$pump_code', '$branch_name', 'RELIANCE', 'RELIANCE', '$qty1', '$rate', '$amount', now(), '$PurchaseId', now())";
                if($conn->query($sql) === FALSE) {
                    throw new Exception(mysqli_error($conn)." Code 042");             
                }

                $sql = "Update dairy.diesel_pump_stock set balance='$UpdateStockBalance' where purchaseid='$PurchaseId'";
                if($conn->query($sql) === FALSE) {
                    throw new Exception(mysqli_error($conn)." Code 043");             
                }
 
                $pid = $pid."002";

                $sql = "insert into dairy.diesel_pump_stock (openuser, purchasedate, purchaseid, purchaseamt, purchaserate, pumpcode, rate, purchaseqty, totalqty, balance, invoice, stamp, vendor, branch, adi, status, parentstock, masterid) values ('$branch_emp' ,now(), '$pid', '$amount2', '$rate2', '$tanker', '$rate2', '$qty2', '$qty2', '$qty2', NULL, now(), '$pump_code', NULL, '1', '0', '$StockId2', '$SetMaster')";
                if($conn->query($sql) === FALSE) {
                    throw new Exception(mysqli_error($conn)." Code 021");             
                }

                $sql = "insert into diesel_api.stock_transfer (`refid`, `creditor`, `debitor`, `branch`, `creditor_comp`, `debitor_comp`, `qty`, `rate`, `amount`, `dated`, `stockid`, `timestamp`) values ('$pid', '$tanker', '$pump_code', '$branch_name', 'RELIANCE', 'RELIANCE', '$qty2', '$rate2', '$amount2', now(), '$PurchaseId2', now())";
                if($conn->query($sql) === FALSE) {
                    throw new Exception(mysqli_error($conn)." Code 022");             
                }

                $sql = "Update dairy.diesel_pump_stock set status='1', closeuser='$branch_emp' where purchaseid='$PurchaseId'";
                if($conn->query($sql) === FALSE) {
                    throw new Exception(mysqli_error($conn)." Code 023");             
                }

                $sql = "Update dairy.diesel_pump_stock set balance='$UpdateStockBalance2' where purchaseid='$PurchaseId2'";
                if($conn->query($sql) === FALSE) {
                    throw new Exception(mysqli_error($conn)." Code 024");             
                }
            } 
///////////////end - insert records 
      }

 
        $sql = "SELECT title FROM diesel_api.dsl_cpanel where company='DIESELPLUS_LOGIN'";
        if ($conn->query($sql) === FALSE) {
                throw new Exception(mysqli_error($conn)." Code 010");             
        }
        $resx = $conn->query($sql);
        $rowx = $resx->fetch_assoc();
        define("dplus_login",$rowx["title"]);

        $sql = "SELECT title FROM diesel_api.dsl_cpanel where company='DIESELPLUS_TOKEN'";
        if ($conn->query($sql) === FALSE) {
                throw new Exception(mysqli_error($conn)." Code 011");             
        }
        $resx = $conn->query($sql);
        $rowx = $resx->fetch_assoc();
        define("dplus_token",$rowx["title"]);

        $sql = "SELECT * FROM dairy.diesel_pump_branch where code='$tanker'";
        if ($conn->query($sql) === FALSE) {
                throw new Exception(mysqli_error($conn)." Code 091");             
        }
        $res = $conn->query($sql);
        $row = $res->fetch_assoc();
        $tanker_locationid = $row['locationid'];
        $tanker_tankid = $row['tankid'];
        $tanker_billtype = $row['billtype'];

        if($fuel=='HSD'){
            $TankerRate = $HSDrate;
            $TankerPump = $hsdpump;
        } else if($fuel=='ADI'){
            $TankerRate = $ADIrate;
            $TankerPump = $adipump;
        } else if($fuel=='MIX'){
            $TankerRate = sprintf("%.2f",($HSDrate+$ADIrate)/2.0);
            $TankerPump = $hsdpump." / ".$adipump;
        }
  
 ////////////// start - New Inputs
        $curl = curl_init();
        curl_setopt_array($curl, array(
          CURLOPT_URL => 'https://api.dieselplus.net/api/v1/raman-roadways/tank_entries.json',
          CURLOPT_RETURNTRANSFER => true,
          CURLOPT_ENCODING => '',
          CURLOPT_MAXREDIRS => 10,
          CURLOPT_TIMEOUT => 30,
          CURLOPT_FOLLOWLOCATION => true,
          CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
          CURLOPT_CUSTOMREQUEST => 'POST',
          CURLOPT_POSTFIELDS =>'{
          "tank_entry": {
            "location_id": "'.$tanker_locationid.'",
            "tank_id": "'.$tanker_tankid.'",
            "date": "'.$entDate.'",
            "quantity": '.$total.',
            "cost_price": '.$TankerRate.',
            "vendor_name": "'.$TankerPump.'"
          }
        }',
          CURLOPT_HTTPHEADER => array(
            "X-Login-Name: ".dplus_login,
            "X-Auth-Token: ".dplus_token,
            "Content-Type: application/json"
          ),
        ));

        $response = curl_exec($curl);
        curl_close($curl);

        $output = json_decode($response , true); 
        $output = $output['tank_entry'];

        if($output['bio_percentage']!='0.0'){
                throw new Exception("Error in Input dieselplus API");             
        }

        $sql = "INSERT INTO diesel_api.dplus_entries (`id`, `location_id`, `tank_id`, `date`, `fluid_id`, `quantity`, `bio_percentage`, `cost_price`, `cost_amount`, `vendor_id`, `vendor_vatin`, `vendor_name`, `vendor_document_number`, `shipping_document`, `updated_by`, `updated_at`, `organization_id`, `timestamp`) VALUES ('".$output['id']."', '".$output['location_id']."', '".$output['tank_id']."', '".$output['date']."', '".$output['fluid_id']."', '".$output['quantity']."', '".$output['bio_percentage']."', '".$output['cost_price']."', '".$output['cost_amount']."', '".$output['vendor_id']."', '".$output['vendor_vatin']."', '".$output['vendor_name']."', '".$output['vendor_document_number']."', '".$output['shipping_document']."', '".$output['updated_by']."', '".$output['updated_at']."', '".$output['organization_id']."', now())";
        if ($conn->query($sql) === FALSE) {
                throw new Exception(mysqli_error($conn)." Code 097");             
        }

        $sql = "update dairy.diesel_pump_stock set dplus_entry='".$output['id']."' WHERE masterid='$SetMaster'";
        if ($conn->query($sql) === FALSE) {
                throw new Exception(mysqli_error($conn)." Code 098");             
        }
 ////////////// end - New Inputs

 
    $conn->query("COMMIT");
    echo "
    <script>
    Swal.fire({
    position: 'top-end',
    icon: 'success',
    title: 'Updated Successfully',
    showConfirmButton: false,
    timer: 1000
    })
    </script>";

} catch(Exception $e) { 

    $conn->query("ROLLBACK"); 
    $content = $e->getMessage();
    $content = preg_replace("/[^0-9a-zA-Z_\.\- ]/", "", $content);  
    echo "
    <script>
    Swal.fire({
    icon: 'error',
    title: 'Error !!!',
    text: '$content'
    })
    </script>";     
} 

?>