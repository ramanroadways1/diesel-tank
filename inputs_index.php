<?php 
include_once 'header.php';
?>  


<script type="text/javascript" src="https://cdn.datatables.net/1.10.19/js/jquery.dataTables.min.js"></script>
<script type="text/javascript" src="https://cdn.datatables.net/buttons/1.5.6/js/dataTables.buttons.min.js"></script>
<script type="text/javascript" src="https://cdn.datatables.net/buttons/1.5.6/js/buttons.flash.min.js"></script>
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/jszip/3.1.3/jszip.min.js"></script>
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.53/vfs_fonts.js"></script> 
<script type="text/javascript" src="https://cdn.datatables.net/buttons/1.5.6/js/buttons.html5.min.js"></script>
<script type="text/javascript" src="https://cdn.datatables.net/buttons/1.5.6/js/buttons.print.min.js"></script>
<script type="text/javascript" src="https://cdn.datatables.net/searchbuilder/1.0.0/js/dataTables.searchBuilder.min.js"></script>
<link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/searchbuilder/1.0.0/css/searchBuilder.dataTables.min.css">
<script src="https://cdn.datatables.net/rowgroup/1.0.2/js/dataTables.rowGroup.min.js"></script>

<style> 
 #user_data_paginate{background-color:#fff}a.dt-button,button.dt-button,div.dt-button{padding:.2em 1em}div.dt-button-collection{max-height:300px;overflow-y:scroll}.dataTables_scroll{margin-bottom:20px}.table{margin:0!important}.applyBtn{border-radius:0!important}table.table-bordered.dataTable td{padding:10px 5px 10px 10px}.dt-buttons{float:right!important}.user_data_filter{float:right}.dt-button{padding:5px 20px;text-transform:uppercase;font-size:12px;text-align:center;cursor:pointer;outline:0;color:#fff;background-color:#37474f;border:none;border-radius:2px;box-shadow:0 4px #999}.dt-button:hover{background-color:#3e8e41}.dt-button:active{background-color:#3e8e41;box-shadow:0 5px #666;transform:translateY(4px)}#user_data_wrapper{width:100%!important}.dt-buttons{margin-bottom:20px}#appenddiv,#appenddiv2{display:block;position:relative}.ui-autocomplete{position:absolute}.table-hover tbody tr:hover td,.table-hover tbody tr:hover th{background-color: #FFF5D7}.table td{vertical-align:middle!important;font-size:11px!important;color:#000;font-family:Verdana,Geneva,sans-serif;padding-top:4px;padding-right:4px;padding-bottom:4px;padding-left:10px}.table-bordered td{border:3px solid #e3e6f0}#user_data_info,#user_data_length{float:left}#user_data_filter,#user_data_paginate{float:right}.paginate_button{color:#000;float:left;padding:6px 12px;text-decoration:none;border:1px solid #ccc;cursor:pointer}.ellipsis{display:none}[type=search]{margin-right:10px;width:250px}.ui-autocomplete{z-index:2150000000!important}.container input{position:absolute;opacity:0;cursor:pointer;height:0;width:0}.checkmark{border-radius:2px;position:absolute;top:0;height:20px;width:20px;background-color:#fff;border:1px solid #000}.container:hover input~.checkmark{background-color:#fff}.container input:checked~.checkmark{background-color:#fff}.container input:disabled~.checkmark{background-color:#eaecf4}.checkmark:after{content:"";position:absolute;display:none}.container input:checked~.checkmark:after{display:block}.container .checkmark:after{left:6px;top:-1px;width:8px;height:16px;border:solid #000;border-width:0 3px 3px 0;-webkit-transform:rotate(45deg);-ms-transform:rotate(45deg);transform:rotate(45deg)}button:disabled,button[disabled]{border:1px solid #333!important;color:#333!important;cursor:no-drop}.table .thead-light th{text-align:center;font-size:11px;color:#444}.component{display:none}table{width:100%!important}table.table-bordered.dataTable td{white-space:nowrap;overflow:hidden;text-overflow:ellipsis}.table .thead-light th{text-transform:uppercase!important}label{text-transform:uppercase}#appenddivbill,#appenddivbillparty,#appenddivcons,#appenddivdo,#appenddivfrom,#appenddivinv,#appenddivitem,#appenddivlr,#appenddivship,#appenddivtno,#appenddivto{display:block;position:relative}.ui-autocomplete{position:absolute}.card label{color:#444}.card label{color:#444} .content{padding-bottom: 0px !important;}
</style>

  <style type="text/css">
    table.dataTable tr.group td{background-color:#fcfcfc} 
    table.dataTable tr.group:hover td{background-color:#e0e0e0; cursor: pointer;} 
       table.table-bordered.dataTable td{
    padding: 5px 5px 5px 10px;
   }
   
  </style>

<div id="content-wrapper" class="d-flex flex-column"> 
<div id="content">
<div id="updatereq_status"></div> 
<div class="container-fluid"> 
<div class="row"> 

<?php

    $sql = "SELECT * FROM dairy.diesel_pump_branch where admin='$branch_name'";
    if($conn->query($sql) === FALSE) {
      echo mysqli_error($conn);         
    }
    $res = $conn->query($sql);
    $Sno = 1;
    while($row = $res->fetch_assoc()){
    $Sno += 1;
?> 
  <div class="col-md-6 " >
      <div class="card shadow mb-4"> 
        <div class="card-header py-3" style="display: none;">
        <h6 class="m-0 font-weight-bold text-dark" style="font-family: Verdana, Geneva, sans-serif; font-weight: normal; text-transform: uppercase;"> <span style="color: maroon;">NEGATIVE</span> BALANCE CARDS </h6>
        </div>
        <div class="card-body ">
        <div class="" style="">
        <table id="user_data100<?php echo $Sno; ?>" class="table table-bordered table-hover">
        <thead class="thead-light">
        <tr>
          <th style="font-size: 10px; color:#444; text-align: center;"> </th>
          <th style="font-size: 10px; color:#444; text-align: center;"> </th>
          <th style="font-size: 10px; color:#444; text-align: center;"> Date</th>
          <th style="font-size: 10px; color:#444; text-align: center;"> Purchase <br> Qty</th> 
          <th style="font-size: 10px; color:#444; text-align: center;"> Total <br> Consume</th> 
          <th style="font-size: 10px; color:#444; text-align: center;"> Stock <br> Balance</th> 
          <th style="font-size: 10px; color:#444; text-align: center;"> Temp. <br> Shortage</th> 
          <th style="font-size: 10px; color:#444; text-align: center;"> Actual <br> Shortage</th> 
        </tr> 
        </thead> 
        </table>
        </div> 
        </div>
      </div>
  </div> 
 
  <script type="text/javascript"> 
 
       $( document ).ready(function() {
      var collapsedGroups = {};
      $('#loadicon').show(); 
      var table = $('#user_data100<?php echo $Sno; ?>').DataTable({ 
      "scrollY": 350,
      "scrollX": true,
      "lengthMenu": [ [10, 500, 1000, -1], [10, 500, 1000, "All"] ], 
      "bProcessing": true,
      "sAjaxSource": "inputs_fetch.php?p=<?php echo $row['code'] ?>",
      "bPaginate": false,
      "sPaginationType":"full_numbers",
      "iDisplayLength": 10,
      "order": [[ 1, "desc" ]], 
      "dom": '<"toolbar100<?php echo $Sno ?>">Bfrtip',
      "ordering": false,
      "buttons": [
      // 'copy', 'csv', 'excel', 'print'
      ],
      "searching": false,
      "columnDefs": [
            {
                "targets": [ 0 ],
                "visible": false
            }
        ],
      "aaSorting": [],
      rowGroup: {
        // Uses the 'row group' plugin
        dataSrc: 0,
        startRender: function (rows, group) {
          var collapsed = !!collapsedGroups[group];

          rows.nodes().each(function (r) {
              r.style.display = collapsed ? '' : 'none';
          });
          var toggleClass = collapsed ? 'fa-plus-square' : 'fa-minus-square';
 
                     var dts = rows.data().pluck(2)[0];
                     var pqty = rows.data().pluck(3).reduce( function (a, b) {return a + b*1;}, 0);
                     var tqty = rows.data().pluck(4).reduce( function (a, b) {return a + b*1;}, 0);
                     var bqty = rows.data().pluck(5).reduce( function (a, b) {return a + b*1;}, 0);
                     var tshort = rows.data().pluck(6).reduce( function (a, b) {return a + b.replace(/[^\d-.]/g, '')*1;}, 0) || 0;
                     var ashort = rows.data().pluck(7).reduce( function (a, b) {return a + b.replace(/[^\d-.]/g, '')*1;}, 0) || 0;
           return $('<tr/>')
              .append('<td colspan="">' + '<center><span class="fa fa-fw ' + toggleClass + ' toggler"/></center></td>')
              .append('<td colspan=""> '+ dts +' </td>')
              .append('<td colspan=""> ' + pqty.toFixed(2) + '</td>')
              .append('<td colspan=""> ' + tqty.toFixed(2) + '</td>')
              .append('<td colspan=""> ' + bqty.toFixed(2) + '</td>')
              .append('<td colspan=""> ' + tshort.toFixed(2) + '</td>')
              .append('<td colspan=""> ' + ashort + '</td>')
              .attr('data-name', group)
              .toggleClass('collapsed', collapsed);
          }
      },
     
      "initComplete": function( settings, json ) {
      $('#loadicon').hide();
      }
      });  
 
      $('#user_data100<?php echo $Sno; ?> tbody').on('click', 'tr.group-start', function () {
        var name = $(this).data('name');
        collapsedGroups[name] = !collapsedGroups[name];
        table.draw();
      }); 

      $("div.toolbar100<?php echo $Sno ?>").html('<h4 style=" color: #5a5a5a; float: right; margin-top:2px; margin-bottom: 5px;"> <?php echo $row['name'] ?> </h4> ');
      }); 
  </script>

<?php } ?>

</div>
</div>
</div>
</div>
<?php 
include_once 'footer.php';
?> 