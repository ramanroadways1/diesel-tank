<?php
    require('connect.php'); 
    
    $id = $conn->real_escape_string($_REQUEST['id']);
    
    $sql = "SELECT r.*, d.name as name FROM diesel_api.cons_fuel r 
        left join (select * from dairy.diesel_pump_own group by code) d on d.code = r.pump 
        where r.id='$id'";

    if ($conn->query($sql) === FALSE) {
        echo mysqli_error($conn);
        exit();                  
    }
    $row = $conn->query($sql)->fetch_assoc();
?>
<style type="text/css"> 
   .modal-backdrop
   {
   opacity:0.9 !important;
   background: #e9ecef;
   }

	#appenddiv, #appenddiv2 {
		display: block; 
		position:relative
	} 
	.ui-autocomplete {
		position: absolute;
	}
</style>

<script type="text/javascript">
  $(function() {
  $("#newveh").autocomplete({
  source: 'tank_vehicle_auto.php',
  appendTo: '#appenddiv',
  select: function (event, ui) { 
         $('#newveh').val(ui.item.value);   
         return false;
  },
  change: function (event, ui) {
  if(!ui.item){
	  $(event.target).val("");
	  Swal.fire({
	  icon: 'error',
	  title: 'Error !!!',
	  text: 'Vehicle does not exists !'
	  })
	  $("#newveh").val("");
	  $("#newveh").focus();
  }
  }, 
  focus: function (event, ui){
  return false;
  }
  });
  });
</script>
  <div class="modal-body">
      <p style="color: #444;"> Approve Diesel to Trip <button type="button" class="close" data-dismiss="modal"> &times; </button> 
      <p style="border-bottom: 1px solid #ccc;"></p>
      </p>

<form method="post" action="" id="RejectBill" autocomplete="off">
<input type="hidden" value="<?php echo $id; ?>" name="dbid" oninput="this.value=this.value.replace(/[^a-z A-Z 0-9]/,'')">
<div class="row">
<div class=" col-md-10" style="max-height: 200px; max-width: 80%; margin-bottom: 10px; overflow: scroll;">
  <a target="_blank" href="http://diesel.rrpl.online/uploads/<?php echo $row['bill']; ?>"><img src="http://diesel.rrpl.online/uploads/<?php echo $row['bill']; ?>"></a>
</div>
<div class="col-md-2" style="padding-top: 50px; padding-left: 35px;">
  <button class="btn btn-sm btn-danger" type="submit"> <i class="fa fa-times"></i> <br> Reject <br> Bill </button>
</div>
</div>
</form>

      <form method="post" action="" id="ApproveTrip" autocomplete="off">
   <input type="hidden" value="<?php echo $id; ?>" name="dbid" oninput="this.value=this.value.replace(/[^a-z A-Z 0-9]/,'')">
 

      <div class="row">


         <div class="form-group col-md-3">
         	<label>Done Time</label>
            <input type="text" oninput="this.value=this.value.replace(/[^a-z A-Z 0-9]/,'')" class="form-control" id="" name="" readonly="" value="<?php echo date('d/m/Y H:i:s', strtotime($row['done_time'])) ?>">
         </div>
         
          <div class="form-group col-md-3">
          <label>Done User</label>
            <input type="text" oninput="this.value=this.value.replace(/[^a-z A-Z 0-9]/,'')" class="form-control" id="" name="" readonly="" value="<?php echo $row["done_user"] ?>">
         </div>
<div class="form-group col-md-3">
          <label>PUMP</label>
            <input type="text" oninput="this.value=this.value.replace(/[^a-z A-Z 0-9]/,'')" class="form-control" id="" name="" readonly="" value="<?php echo $row["name"] ?>">
         </div>
         <div class="form-group col-md-3">
         	<label>Vehicle</label>
            <input type="text" oninput="this.value=this.value.replace(/[^a-z A-Z 0-9]/,'')" class="form-control" id="" name="" readonly="" value="<?php echo $row["tno"] ?>">
         </div>
        
         <div class="form-group col-md-3">
         	<label>Quantity</label>
            <input type="text" oninput="this.value=this.value.replace(/[^a-z A-Z 0-9]/,'')" class="form-control" id="" name="" readonly="" value="<?php echo $row["qty"] ?>">
         </div>


         <div class="form-group col-md-3">
          <label>Rate</label>
            <input type="text" oninput="this.value=this.value.replace(/[^a-z A-Z 0-9]/,'')" class="form-control" id="" name="" readonly="" value="<?php echo $row["rate"] ?>">
         </div>


         <div class="form-group col-md-3">
          <label>Amount</label>
            <input type="text" oninput="this.value=this.value.replace(/[^a-z A-Z 0-9]/,'')" class="form-control" id="" name="" readonly="" value="<?php echo $row["amount"] ?>">
         </div>
        
        <div class="form-group col-md-3">
            <label>Trip </label>
            <select class="form-control" required="required" name="tripno">
              <option value=""> -- select -- </option>
              <?php

                $sql = "select * from dairy.trip where tno='".preg_replace("/[^0-9a-zA-Z]/", "", $row["tno"])."' and (status='0' or active_trip='1')";
                if ($conn->query($sql) === FALSE) {
                    echo mysqli_error($conn);
                    exit();                  
                }
                $trip_result = $conn->query($sql);

                while ($data = $trip_result->fetch_assoc()) {
                  echo "<option value='$data[id]_$data[trip_no]'> $data[from_station] to $data[to_station] ($data[id]) </option>";
                }
              ?>
            </select>
         </div>

      </div>
      <div class="modal-footer" style="">
      <button type="button" id="hidemodal" class="btn btn-warning" data-dismiss="modal">CLOSE</button>
        <?php
        echo '<input type="submit" id="formbtn" class="btn btn-success" name="submit" value="SAVE" />';
        ?>

      </div>
   </div>
   </form>

