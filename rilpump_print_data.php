<?php

  require('connect.php'); 
  
  $p = $_REQUEST['p'];

  $connection = new PDO('mysql:host='.$DATABASE_HOST.';dbname='.$DATABASE_rrpl.';', $DATABASE_USER, $DATABASE_PASS );
 
   $statement = $connection->prepare("
    ( 
    SELECT 'OWN' as type, diesel_entry.branch as branch, diesel_entry.timestamp as stamp, diesel_entry.date as date, diesel_entry.card as card, own_truck.comp as comp, diesel_entry.tno, diesel.qty, diesel.rate, diesel.amount from dairy.diesel 
    LEFT JOIN dairy.diesel_entry on diesel_entry.id=diesel.id
    LEFT join dairy.own_truck on own_truck.tno=diesel.tno
    where stockid='$p' 
    ) 
    UNION
    (
    SELECT 'MARKET' as type, diesel_fm.branch as branch, diesel_fm.timestamp as stamp, diesel_fm.pay_date as date, diesel_fm.dcard as card, diesel_fm.com as comp, diesel_fm.tno, diesel_fm.qty, diesel_fm.rate, diesel_fm.disamt from rrpl_database.diesel_fm 
    where stockid='$p'
    )

    order by date");
 
  $statement->execute();
  $result = $statement->fetchAll();
  $count = $statement->rowCount();
  $data = array();

foreach($result as $row)
{ 
  $sub_array = array(); 
 
 if($row["comp"]=="RAMAN_ROADWAYS"){
  $comp = "RR";
 } else {
  $comp = $row["comp"];
 }

  $sub_array[] = date('d-m-Y', strtotime($row["date"]));
  $sub_array[] = date('d-m-Y', strtotime($row["date"]));
  $sub_array[] = $row["branch"];  
  $sub_array[] = "'".$row["card"];  
  $sub_array[] = $comp;
  $sub_array[] = $row["tno"];
  $sub_array[] = $row["qty"];
  $sub_array[] = $row["rate"];
  $sub_array[] = $row["amount"];
  $sub_array[] = $row["type"];

  $data[] = $sub_array;
} 

$results = array(
  "sEcho" => 1,
    "iTotalRecords" => $count,
    "iTotalDisplayRecords" => $count,
    "aaData"=>$data);

echo json_encode($results); 
exit
?>