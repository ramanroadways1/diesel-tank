<?php
 
require('connect.php');
 
    $connection = new PDO('mysql:host='.$host.';dbname='.$db_name.';', $username, $password );
    $statement = $connection->prepare("select d.*, g.full_name, g.name, if(admin='0','http://diesel.rrpl.online/uploads','uploads') as adminpath from diesel_api.dplus_vehicles d  LEFT join diesel_api.dplus_vehicle_groups g on g.id = d.vehicle_group_id order by code desc");
    $statement->execute();
    $result = $statement->fetchAll();
    $count = $statement->rowCount();
    $data = array();

foreach($result as $row)
{
    $sub_array = array(); 
    $sub_array[] = $row["code"]; 
    $sub_array[] = $row["name"]; 
    $sub_array[] = $row["license"]; 
    $sub_array[] = $row["terminal"]; 
    $sub_array[] = $row["personnel"]; 

if($row["personnel"]!=''){
    $sub_array[] = "<a target='_blank' href='$row[adminpath]/$row[truckphoto]'>TRUCK</a> | <a href='$row[adminpath]/$row[tagphoto]' target='_blank'>TAG</a>"; 
} else {
    $sub_array[] = 'NA'; 
}

    if($row["is_active"]=='1'){
    $sub_array[] = '<font color="green"> ACTIVE </font>'; 
    $stat='';
    } else{
    $sub_array[] = '<font color="red"> DELETED </font>'; 
    $stat='disabled';
    }

    $sub_array[] = '<button onclick="TagUpdate('.$row["id"].')" class="btn btn-sm btn-danger" style="padding:5px;" '.$stat.' disabled="disabled">  <i style="font-size:15px;" class="fa fa-ban"></i> </button>'; 


    $data[] = $sub_array;
} 

    $results = array(
      "sEcho" => 1,
    "iTotalRecords" => $count,
    "iTotalDisplayRecords" => $count,
    "aaData"=>$data);

echo json_encode($results); 
exit
?>
 