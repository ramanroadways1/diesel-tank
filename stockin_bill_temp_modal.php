<?php
   require('connect.php'); 
    $id = $conn->real_escape_string($_REQUEST['id']);
    $sql = "select * from dairy.diesel_pump_stock where id='$id'";
    $row = $conn->query($sql)->fetch_assoc();
?>
<style type="text/css"> 
   .modal-backdrop
   {
   opacity:0.9 !important;
   background: #e9ecef;
   }

	#appenddiv, #appenddiv2 {
		display: block; 
		position:relative
	} 
	.ui-autocomplete {
		position: absolute;
	}
</style>
 
<form method="post" action="" id="TempSave" role="form" autocomplete="off" enctype='multipart/form-data'>
   <input type="hidden" value="<?php echo $id; ?>" name="dbid" oninput="this.value=this.value.replace(/[^a-z A-Z 0-9]/,'')">
   <div class="modal-body">
      <p style="color: #444;"> <?php echo $row["branch"] ?> STOCK-IN  <button type="button" class="close" data-dismiss="modal"> &times; </button> 
      <p style="border-bottom: 1px solid #ccc;"></p>
      </p>
      <div class="row">
         <div class="form-group col-md-3">
         	<label>Date</label>
            <input type="text" oninput="this.value=this.value.replace(/[^a-z A-Z 0-9]/,'')" class="form-control" id="" name="" readonly="" value="<?php echo date('d/m/Y', strtotime($row['purchasedate'])) ?>" style=" color: #000;">
         </div>
         <div class="form-group col-md-3">
         	<label>Qty (ltr)</label>
            <input type="text" oninput="this.value=this.value.replace(/[^a-z A-Z 0-9]/,'')" class="form-control" id="qty" name="" readonly="" value="<?php echo $row["purchaseqty"] ?>" style=" color: #000;">
         </div> 
         <div class="form-group col-md-3">
         	<?php

                $sql = "select * from dairy.diesel_pump_branch where code='$row[pumpcode]'";
                $res = $conn->query($sql);
                $data = $res->fetch_assoc();


              ?>
         	<label>Pump</label>
            <input type="text" oninput="this.value=this.value.replace(/[^a-z A-Z 0-9]/,'')" class="form-control" id="" name="" readonly="" value="<?php echo $data["name"] ?>" style=" color: #000;">
         </div>
 

	<div class="col-md-3">
      <label>  TEMP. Shortage </label>
      <input type="text" id="" name="temp" class="form-control" required="required" oninput="this.value=this.value.replace(/[^0-9.]/,'')">
    </div> 


      </div>
   </div>
   <div class="modal-footer">
      <button type="button" id="hidemodal" class="btn btn-danger" data-dismiss="modal">CLOSE</button>
   	  <?php 
   	  	echo '<input type="submit" id="uploadbtn" class="btn btn-success" name="submit" value="SAVE" />';
      ?>
   
   </div>
</form>
 