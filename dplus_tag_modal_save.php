<?php
	require('connect.php');

    $id = $conn->real_escape_string($_REQUEST['id']);
    $getotp = $conn->real_escape_string($_REQUEST['getotp']);
    $otp = $conn->real_escape_string($_REQUEST['otp']);

	try {
	$conn->query("START TRANSACTION"); 

	$sql = "SELECT title FROM diesel_api.dsl_cpanel where company='DIESELPLUS_LOGIN'";
	if ($conn->query($sql) === FALSE) {
	  throw new Exception(mysqli_error($conn)); 
	}
	$resx = $conn->query($sql);
	$rowx = $resx->fetch_assoc();
	define("dplus_login",$rowx["title"]);

	$sql = "SELECT title FROM diesel_api.dsl_cpanel where company='DIESELPLUS_TOKEN'";
	if ($conn->query($sql) === FALSE) {
	  throw new Exception(mysqli_error($conn)); 
	}
	$resx = $conn->query($sql);
	$rowx = $resx->fetch_assoc();
	define("dplus_token",$rowx["title"]);

	if($getotp!=$otp){
	  throw new Exception("Incorrect OTP"); 
	}

	$sql = "select v.*, g.full_name from diesel_api.dplus_vehicles v
	left join diesel_api.dplus_vehicle_groups g on g.id=v.vehicle_group_id
	 where v.id='$id'";
	if ($conn->query($sql) === FALSE) {
	  throw new Exception(mysqli_error($conn)); 
	}
	$row = $conn->query($sql)->fetch_assoc();

	    $endDate = date('Y-m-d');
	    $startDate = $row['activation_start_date'];

	    if(strtotime($endDate)==strtotime($startDate)){
	      $endDate = date('Y-m-d', strtotime("+1 day"));
	    }

		  $curl = curl_init(); 
		  curl_setopt_array($curl, array(
		    CURLOPT_URL => "https://api.dieselplus.net/api/v1/raman-roadways/vehicles/".$id.".json",
		    CURLOPT_RETURNTRANSFER => true,
		    CURLOPT_ENCODING => "",
		    CURLOPT_MAXREDIRS => 10,
		    CURLOPT_TIMEOUT => 30,
		    CURLOPT_FOLLOWLOCATION => true,
		    CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
		    CURLOPT_CUSTOMREQUEST => "PUT",
		    CURLOPT_POSTFIELDS =>'{
		    "vehicle": {
		      "registration_country_id": "IN",
		      "license": "'.$row['license'].'",
		      "control_unit": "none",
		      "is_consumption_calculation": false,
		      "is_allowed_internal_facilities_supplies": false,
		      "is_allowed_all_fluids": false,
		      "is_active": false,
		      "activation_start_date": "'.$startDate.'T00:00:00Z",
		      "activation_end_date": "'.$endDate.'T00:00:00Z"
		    }
		  }',
		    CURLOPT_HTTPHEADER => array(
		      "X-Login-Name: ".dplus_login,
		      "X-Auth-Token: ".dplus_token,
		      "Content-Type: application/json"
		    ),
		  ));

		  $response = curl_exec($curl); 
		  curl_close($curl);

			$output = json_decode($response , true); 

			if($output['vehicle']['id']!=$id){
      				throw new Exception("API error - $response"); 
			}

			$sql = "UPDATE diesel_api.dplus_vehicles set is_active='0' where id='$id'";
    		if ($conn->query($sql) === FALSE) {
      			throw new Exception(mysqli_error($conn)); 
    		}

			$conn->query("COMMIT");
			echo "
			<script>
			Swal.fire({
			position: 'top-end',
			icon: 'success',
			title: 'Updated Successfully',
			showConfirmButton: false,
			timer: 1000
			})
			</script>";

 	} catch(Exception $e) { 

			$conn->query("ROLLBACK"); 
			$content = $e->getMessage();
			$content = preg_replace("/[^0-9a-zA-Z_ ]/", "", $content);  
			echo "
			<script>
			Swal.fire({
			icon: 'error',
			title: 'Error !!!',
			text: '$content'
			})
			</script>";		
	} 
?>