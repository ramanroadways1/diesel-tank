<?php  require('header.php'); ?> 

<style type="text/css">

/*DEMO ONLY*/
.service-categories .card{
  transition: all 0.2s;
}
.service-categories .card-title{
  padding-top: 0.5em;
}
.service-categories a:hover{
  text-decoration: none;
}
.service-card{
  background: #fff;
  border: 0;
}
.service-card:hover{
  background: #fffde7;
}

.card{
	margin-top: 10px;
}

</style>
<!-- <a href="stock_bill_index.php"> Upload Bill </a>
<br>


<a href="stockin_bill_index.php"> Stock-In </a>
<br>


<a href="addstock_index.php"> Add Stock </a>
<br>

<a href="clearstock_index.php"> Close Stock </a>
<br>

<a href="transferstock_index.php"> Transfer Stock </a>
<br> -->
<!-- pump in usage for diesel entry - enable disable  -->

<section class="service-categories text-xs-center">
<div class="container">
  <div class="row">
    
      <div class="col-md-3">
      <a href="tank_index.php">
        <div class="card service-card card-inverse">
          <div class="card-block">
            <center><img src="assets/gas.png" width="70px" style="padding-top: 20px;"></center>
            <h4 style=" text-align: center; margin-top: 2px;" class="card-title">Tank Supplies </h4>
          </div>
        </div>
      </a>
    </div>  

     <div class="col-md-3">
      <a href="inputs_index.php">
        <div class="card service-card card-inverse">
          <div class="card-block">
            <center><img src="assets/Gas-Station-PNG-Photo.png" width="135px" style="padding-top: 20px;"></center>
            <h4 style=" text-align: center; margin-top: 2px;" class="card-title">Stock Inputs </h4>
          </div>
        </div>
      </a>
    </div>  

    <div class="col-md-3">
      <a href="stock_bill_index.php">
        <div class="card service-card card-inverse">
          <div class="card-block">
            <center><img src="assets/receipt-min.png" width="70px" style="padding-top: 20px;"></center>
            <h4 style=" text-align: center; margin-top: 2px;" class="card-title">Upload Invoice</h4>
          </div>
        </div>
      </a>
    </div>   
        
    
    <div class="col-md-3">
      <a href="stockin_bill_index.php">
        <div class="card service-card card-inverse">
          <div class="card-block">
            <center><img src="assets/kyc-min.png" width="70px" style="padding-top: 20px;"></center>
            <h4 style=" text-align: center; margin-top: 2px;" class="card-title">Check-in Invoice </h4>
          </div>
        </div>
      </a>
    </div>  

    
    <div class="col-md-3">
      <a href="addstock_index.php">
        <div class="card service-card card-inverse">
          <div class="card-block">
            <center><img src="assets/tank-truck-min.png" width="70px" style="padding-top: 20px;"></center>
            <h4 style=" text-align: center; margin-top: 2px;" class="card-title">Open Stock</h4>
          </div>
        </div>
      </a>
    </div>  

    
    <div class="col-md-3">
      <a href="clearstock_index.php">
        <div class="card service-card card-inverse">
          <div class="card-block">
            <center><img src="assets/oil-tanker-min.png" width="70px" style="padding-top: 20px;"></center>
            <h4 style=" text-align: center; margin-top: 2px;" class="card-title">Close Stock</h4>
          </div>
        </div>
      </a>
    </div>  

    
    <div class="col-md-3">
      <a href="transferstock_index.php">
        <div class="card service-card card-inverse">
          <div class="card-block">
            <center><img src="assets/tank-min.png" width="70px" style="padding-top: 20px;"></center>
            <h4 style=" text-align: center; margin-top: 2px;" class="card-title">Transfer Stock</h4>
          </div>
        </div>
      </a>
    </div>  

    <div class="col-md-3">
      <a href="dplus_tag_index.php">
        <div class="card service-card card-inverse">
          <div class="card-block">
            <center><img src="assets/3231350.png" width="70px" style="padding-top: 20px;"></center>
            <h4 style=" text-align: center; margin-top: 2px;" class="card-title">Vehicle Tag</h4>
          </div>
        </div>
      </a>
    </div>  

    
  </div><!--End Row-->
  
</div>
</section>
<?php include('footer.php'); ?>