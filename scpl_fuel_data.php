<?php

      require('connect.php');

      $DATABASE = $DATABASE_rrpl; 

      $connection = new PDO('mysql:host='.$DATABASE_HOST.';dbname='.$DATABASE.';', $DATABASE_USER, $DATABASE_PASS );
      $statement = $connection->prepare("SELECT r.*, d.name as name FROM diesel_api.cons_fuel r 
        left join (select * from dairy.diesel_pump_own group by code) d on d.code = r.pump 
        where r.branch='$_SESSION[user]' AND r.approv='0' and r.status='0' order by r.id desc");  

  $statement->execute();
  $result = $statement->fetchAll();
  $count = $statement->rowCount();
  $data = array();

foreach($result as $row)
{ 
  $sub_array = array(); 

  if($row['done']==1){
    $stat = "".strtolower($row['done_user'])." <br> <sub>$row[done_time]</sub>";
        $btn=" <center> <button onclick='approv_req(".$row['id'].")' class='btn btn-sm btn-success'> <i class='fa fa-check'></i> <b>APPROV</b> </button> </center>";

  } else {
    $stat = " - ";
        $btn = " <center> <button onclick='delete_req(".$row['id'].")' class='btn btn-sm btn-danger'> <i class='fa fa-times-circle'></i> <b>DELETE</b> </button> </center>";

  }

  $sub_array[] = " $btn"; 
  $sub_array[] = $conn -> real_escape_string($row['id']);
  $sub_array[] = date('d/m/Y', strtotime($row['date']));
  $sub_array[] = $conn -> real_escape_string($row['tno']);
  $sub_array[] = $conn -> real_escape_string($row['name']);
  if($row['qty']=="0.00"){
    $qty = "TANK FULL";
  } else {
    $qty = $row['qty'];
  }
  $sub_array[] = $qty;

  $sub_array[] = $conn -> real_escape_string(strtolower($row['requser']))." <br> <sub>$row[reqtime]</sub>";
  $sub_array[] = $stat;
  $data[] = $sub_array;
} 

    $results = array(
    "sEcho" => 1,
    "iTotalRecords" => $count,
    "iTotalDisplayRecords" => $count,
    "aaData"=>$data);

echo json_encode($results); 
exit
?>