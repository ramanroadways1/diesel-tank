<?php 
   require 'header.php';
?>   
<script type="text/javascript" src="https://cdn.datatables.net/1.10.19/js/jquery.dataTables.min.js"></script>
<script type="text/javascript" src="https://cdn.datatables.net/buttons/1.5.6/js/dataTables.buttons.min.js"></script>
<script type="text/javascript" src="https://cdn.datatables.net/buttons/1.5.6/js/buttons.flash.min.js"></script>
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/jszip/3.1.3/jszip.min.js"></script>
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.53/vfs_fonts.js"></script> 
<script type="text/javascript" src="https://cdn.datatables.net/buttons/1.5.6/js/buttons.html5.min.js"></script>
<script type="text/javascript" src="https://cdn.datatables.net/buttons/1.5.6/js/buttons.print.min.js"></script>
<script type="text/javascript" src="https://cdn.datatables.net/searchbuilder/1.0.0/js/dataTables.searchBuilder.min.js"></script>
<link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/searchbuilder/1.0.0/css/searchBuilder.dataTables.min.css">

<style type="text/css">
   #user_data_paginate{background-color:#fff}a.dt-button,button.dt-button,div.dt-button{padding:.2em 1em}div.dt-button-collection{max-height:300px;overflow-y:scroll}.dataTables_scroll{margin-bottom:20px}.table{margin:0!important}.applyBtn{border-radius:0!important}table.table-bordered.dataTable td{padding:10px 5px 10px 10px}.dt-buttons{float:right!important}.user_data_filter{float:right}.dt-button{padding:5px 20px;text-transform:uppercase;font-size:12px;text-align:center;cursor:pointer;outline:0;color:#fff;background-color:#37474f;border:none;border-radius:2px;box-shadow:0 4px #999}.dt-button:hover{background-color:#3e8e41}.dt-button:active{background-color:#3e8e41;box-shadow:0 5px #666;transform:translateY(4px)}#user_data_wrapper{width:100%!important}.dt-buttons{margin-bottom:20px}#appenddiv,#appenddiv2{display:block;position:relative}.ui-autocomplete{position:absolute}.table-hover tbody tr:hover td,.table-hover tbody tr:hover th{background-color: #FFF5D7}.table td{vertical-align:middle!important;font-size:11px!important;color:#000;font-family:Verdana,Geneva,sans-serif;padding-top:4px;padding-right:4px;padding-bottom:4px;padding-left:10px}.table-bordered td{border:3px solid #e3e6f0}#user_data_info,#user_data_length{float:left}#user_data_filter,#user_data_paginate{float:right}.paginate_button{color:#000;float:left;padding:6px 12px;text-decoration:none;border:1px solid #ccc;cursor:pointer}.ellipsis{display:none}[type=search]{margin-right:10px;width:250px}.ui-autocomplete{z-index:2150000000!important}.container input{position:absolute;opacity:0;cursor:pointer;height:0;width:0}.checkmark{border-radius:2px;position:absolute;top:0;height:20px;width:20px;background-color:#fff;border:1px solid #000}.container:hover input~.checkmark{background-color:#fff}.container input:checked~.checkmark{background-color:#fff}.container input:disabled~.checkmark{background-color:#eaecf4}.checkmark:after{content:"";position:absolute;display:none}.container input:checked~.checkmark:after{display:block}.container .checkmark:after{left:6px;top:-1px;width:8px;height:16px;border:solid #000;border-width:0 3px 3px 0;-webkit-transform:rotate(45deg);-ms-transform:rotate(45deg);transform:rotate(45deg)}button:disabled,button[disabled]{border:1px solid #333!important;color:#333!important;cursor:no-drop}.table .thead-light th{text-align:center;font-size:11px;color:#444}.component{display:none}table{width:100%!important}table.table-bordered.dataTable td{white-space:nowrap;overflow:hidden;text-overflow:ellipsis}.table .thead-light th{text-transform:uppercase!important}label{text-transform:uppercase}#appenddivbill,#appenddivbillparty,#appenddivcons,#appenddivdo,#appenddivfrom,#appenddivinv,#appenddivitem,#appenddivlr,#appenddivship,#appenddivtno,#appenddivto{display:block;position:relative}.ui-autocomplete{position:absolute}.card label{color:#444}.card label{color:#444} .content{padding-bottom: 0px !important;}
   .main-panel>.content{
    padding: 0 20px 20px;
   }
   table.table-bordered.dataTable td{
    padding: 5px 5px 5px 10px;
   }
   #user_data2_info,#user_data2_length{float:left}#user_data2_filter,#user_data2_paginate{float:right}
   .user_data2_filter{float:right}
   #user_data2_wrapper{width:100%!important}
   #user_data2_paginate{background-color:#fff}

   #user_data3_info,#user_data3_length{float:left}#user_data3_filter,#user_data3_paginate{float:right}
   .user_data3_filter{float:right;}
   #user_data3_wrapper{width:100%!important}
   #user_data3_paginate{background-color:#fff}
</style>
<div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="memberModalLabel" aria-hidden="true">
<div class="modal-dialog modal-lg">
   <div class="modal-content"> 
       <div class="dash" id="modaldetail"> 
       </div> 
   </div>
</div>
</div> 
<div id="dataModal" class="modal fade in"  data-keyboard="false" data-backdrop="static">
 <div class="modal-dialog">
    <div class="modal-content" id="dataModal_detail">   
    </div>
 </div>
</div>
<div id="response"></div>

<div id="content-wrapper" class="d-flex flex-column">
   <div id="content">
      <div id="updatereq_status"></div>
      <div class="container-fluid">
         <div class="row">
            <div class="col-md-7" >
               <div class="card shadow mb-4" style="border-radius: 0px !important;">
                  <div class="card-header">
                     <!-- <div class="col-md-12" style="padding-top: 5px; padding-bottom: 10px;">
                        <h6>REJECTED POD - UPLOADS APPROVAL </h6> 
                        </div> -->
                 
                     <div class="card-body" style="padding-top: 0px; margin-bottom: 15px;">
                        <!-- style="  -->
                        <table id="user_data2" class="table table-bordered table-hover" style="background: #fff;">
                           <thead class="thead-light">
                              <tr>
                                 <th colspan="8" style="background: #fff; font-size: 13px; letter-spacing: 0.5px;"> PENDING TRIP APPROVAL </th>
                              </tr>
                              <tr>
                                 <th> # </th>
                                 <th> Date </th>
                                 <th> Time </th>
                                 <th> Tanker </th>
                                 <th> Vehicle </th>
                                 <th> Quantity</th>
                                 <th> User </th>
                                 <th> Txnid</th>
                              </tr>
                           </thead>
                        </table>
                     </div> 
                     <br> 
                  </div>
               </div>
            </div>


             <div class="col-md-5" >
               <div class="card shadow mb-4" style="border-radius: 0px !important;">
                  <div class="card-header">

                     <!-- <div class="col-md-12" style="padding-top: 5px; padding-bottom: 10px;">
                        <h6>REJECTED POD - UPLOADS APPROVAL </h6> 
                        </div> -->
                     <div class="card-body" style="padding-top: 0px; margin-bottom: 15px;">
                        <!-- style="  -->
                        <table id="user_data3" class="table table-bordered table-hover" style="background: #fff;">
                           <thead class="thead-light">
                              <tr>
                                 <th colspan="8" style="background: #fff; font-size: 13px; letter-spacing: 0.5px;"> available STOCK </th>
                              </tr>
                              <tr>
                                 <th> FUEL <br>TANKER </th>
                                 <th style="width: 160px;"> LEVEL<br>INDICATOR </th>
                                 <th> TOTAL <br> QTY </th>
                                 <th> BALANCE <br> QTY </th>
                              </tr>
                           </thead>
                        </table>
                     </div> 
                     <br> 
                  </div>
               </div>
            </div>

         </div>
      </div>
   </div>
</div>

<div id="content-wrapper" class="d-flex flex-column">
   <div id="content">
      <div id="updatereq_status"></div>
      <div class="container-fluid">
         <div class="row">
            <div class="col-md-12" >
               <div class="card shadow mb-4" style="border-radius: 0px !important;">
                  <div class="card-header">
                     <!-- <div class="col-md-12" style="padding-top: 5px; padding-bottom: 10px;">
                        <h6>REJECTED POD - UPLOADS APPROVAL </h6> 
                        </div> -->
                 
                     <div class="card-body" style="padding-top: 0px; margin-bottom: 15px;">
                        <!-- style="  -->
                        <table id="user_data" class="table table-bordered table-hover" style="background: #fff;">
                           <thead class="thead-light">
                            <tr>
                                 <th colspan="8" style="background: #fff; font-size: 13px; letter-spacing: 0.5px;">  DIESEL TANKER SUPPLIES APPROVAL </th>
                              </tr>
                              <tr>
                                 <th> Txnid</th>
                                 <th> Date </th>
                                 <th> Time </th>
                                 <th> Tanker </th>
                                 <th> Vehicle </th>
                                 <th> Operator </th>
                                 <th> Quantity</th>
                                 <th> # </th>
                                 <th> Narration</th>
                              </tr>
                           </thead>
                        </table>
                     </div> 
                     <br> 
                  </div>
               </div>
            </div>
         </div>
      </div>
   </div>
</div>
<div style="margin-bottom: 20px;">
  
</div>
                     <style type="text/css">
                        #appenddiv2 {
                        display: block; 
                        position:relative
                        } 
                     </style>
                     <style type="text/css">
                        .red {
                        /*background-color: #ffc7c7 !important;*/
                        }
                        .green {
                        /*background-color: #e1ffe1 !important;*/
                        } 
                     </style>
                     <script type="text/javascript">

jQuery( document ).ready(function() {

  var table = jQuery("#user_data2").dataTable({
  "lengthMenu": [ [10, 100, 500, -1], [10, 100, 500, "All"] ], 
  "bProcessing": true,
  "searchDelay": 1000,
  "scrollY": 150,
  "scrollX": true,
  "sAjaxSource": "tank_index_data_trip.php",
  "bPaginate": false,
  "sPaginationType":"full_numbers",
  "iDisplayLength": 10,
  "ordering": false,
  "buttons": [],
  "language": {
          "loadingRecords": "&nbsp;",
          "processing": "<center> <font color=brown> Please wait while Loading </font> <img src=https://www.mypsdtohtml.com/_ui/images/loading.gif height=20> </center>"
      },
  dom: "<\'toolbar\'>lBfrtip", 
  "columnDefs":[
  {

  "targets": [ 1 ],
  "visible": false 
  },
  ], 
  "initComplete": function( settings, json ) {
  }
  });   
  });

  $(document).ready(function() { 
  var table = $("#user_data2").DataTable(); 
  $("#table-filter").on("change", function(){
  table.search(this.value).draw();   
  });
  } );    


  jQuery( document ).ready(function() {

  var table = jQuery("#user_data3").dataTable({
  "lengthMenu": [ [10, 100, 500, -1], [10, 100, 500, "All"] ], 
  "bProcessing": true,
  "searchDelay": 1000,
  "scrollY": 162,
  "scrollX": true,
  "sAjaxSource": "tank_index_data_stock.php",
  "bPaginate": false,
  "searching" : false,
  "sPaginationType":"full_numbers",
  "iDisplayLength": 10,
  "ordering": false,
  "buttons": [],
  "language": {
          "loadingRecords": "&nbsp;",
          "processing": "<center> <font color=brown> Please wait while Loading </font> <img src=https://www.mypsdtohtml.com/_ui/images/loading.gif height=20> </center>"
      },
  dom: "<\'toolbar\'>lBfrtip", 
  "columnDefs":[

  ], 
  "initComplete": function( settings, json ) {
  }
  });   
  });

  $(document).ready(function() { 
  var table = $("#user_data3").DataTable(); 
  $("#table-filter").on("change", function(){
  table.search(this.value).draw();   
  });
  } );  
                        jQuery( document ).ready(function() {
                        
                            $("#loadicon").show(); 
                            var table = jQuery("#user_data").dataTable({
                        
                              // "createdRow": function( row, data, dataIndex ) {
                              //   if ( data[1] == "Rejected" ) {        
                              //   $(row).addClass('red'); 
                              //   }
                              //   if ( data[1] == "Approved" ) {        
                              //   $(row).addClass('green'); 
                              //   }
                              // },

                            "lengthMenu": [ [10, 100, 500, -1], [10, 100, 500, "All"] ], 
                            "bProcessing": true,
                            "searchDelay": 1000,
                            "scrollY": 350,
                            "scrollX": true,
                            "sAjaxSource": "tank_index_data.php",
                            "bPaginate": true,
                            "sPaginationType":"full_numbers",
                            "iDisplayLength": 10,
                            // "dom": "lBfrtip",
                            "ordering": false,
                            "buttons": [],
                            "language": {
                                    "loadingRecords": "&nbsp;",
                                    "processing": "<center> <font color=brown> Please wait while Loading </font> <img src=https://www.mypsdtohtml.com/_ui/images/loading.gif height=20> </center>"
                                },
                            dom: "Q<\'toolbar\'>lBfrtip", 
                            "columnDefs":[
                            {

                            "targets": [ 1 ],
                            "visible": false 
                            // "targets":[4],
                            // "orderable":false,
                            },
                            ],

                            "searchBuilder": {
                                    "preDefined": {
                                        "criteria": [
                                            {
                                                "data": "Tanker",
                                                "condition": "=",
                                                "value": ["TANK GJ-01-CU-4000"]
                                            }
                                        ]
                                    },
                                    "columns": [1,3,4,5,6]
                            },

                            "initComplete": function( settings, json ) {
                            $("#loadicon").hide();
                            }
                            });  
                        
                            // $("div.toolbar").html('<select class="form-control" style="width: 180px; max-height:28px; float:right; margin-left:10px;" id="table-filter"><option value="">All Voucher</option><option value="Pending">Pending</option> <option value="Approved">Approved</option><option value="Rejected">Rejected</option></select>');
                        
                            });
                        
                            $(document).ready(function() { 
                            var table = $("#user_data").DataTable(); 
                            $("#table-filter").on("change", function(){
                            table.search(this.value).draw();   
                            });
                            } );    
                         
                            // function Download(){
                            //   window.location.href="pod_reject_download.php"; 
                            // } 

              function AddQty(val){
                // alert(val);
                $('#loadicon').show(); 
                var id = val;
                $.ajax({
                  url:"tank_addstock_modal.php",  
                  method:"post",  
                  data:{id:id},  
                  success:function(data){  
                    $('#modaldetail').html(data);  
                    $('#exampleModal').modal("show");  
                    $('#loadicon').hide(); 
                  }
                });
              }

              function vehicle(val){
                $('#loadicon').show(); 
                var id = val;
                $.ajax({
                  url:"tank_vehicle_modal.php",  
                  method:"post",  
                  data:{id:id},  
                  success:function(data){  
                    $('#modaldetail').html(data);  
                    $('#exampleModal').modal("show");  
                    $('#loadicon').hide(); 
                  }
                });
              }

              function stock(val){
                $('#loadicon').show(); 
                var id = val;
                $.ajax({
                  url:"tank_stock_modal.php",  
                  method:"post",  
                  data:{id:id},  
                  success:function(data){  
                    $('#modaldetail').html(data);  
                    $('#exampleModal').modal("show");  
                    $('#loadicon').hide(); 
                  }
                });
              }


              function updateTrip(val){
                $('#loadicon').show(); 
                var id = val;
                $.ajax({
                  url:"tank_trip_modal.php",  
                  method:"post",  
                  data:{id:id},  
                  success:function(data){  
                    $('#modaldetail').html(data);  
                    $('#exampleModal').modal("show");  
                    $('#loadicon').hide(); 
                  }
                });
              }

              $(document).ready(function()
              { 
                $(document).on('submit', '#AddTrip', function()
                {  
                  $("#formbtn").attr("disabled", true);
                  $('#loadicon').show(); 
                  var data = $(this).serialize(); 
                  $.ajax({  
                    type : 'POST',
                    url  : 'tank_trip_save.php',
                    data : data,
                    success: function(data) {  
                      $('#exampleModal').modal("hide");  
                      $('#response').html(data);  
                      $('#user_data').DataTable().ajax.reload(null,false); 
                      $('#user_data2').DataTable().ajax.reload(null,false); 
                      $('#user_data3').DataTable().ajax.reload(null,false); 
                      $('#loadicon').hide();   
                    }
                  });
                  return false;
                });
              }); 


              $(document).ready(function()
              { 
                $(document).on('submit', '#updateSupplies', function()
                {  
                  $("#formbtn").attr("disabled", true);
                  $('#loadicon').show(); 
                  var data = $(this).serialize(); 
                  $.ajax({  
                    type : 'POST',
                    url  : 'tank_stock_save.php',
                    data : data,
                    success: function(data) {  
                      $('#exampleModal').modal("hide");  
                      $('#response').html(data);  
                      $('#user_data').DataTable().ajax.reload(null,false); 
                      $('#user_data2').DataTable().ajax.reload(null,false); 
                      $('#user_data3').DataTable().ajax.reload(null,false); 
                      $('#loadicon').hide();   
                    }
                  });
                  return false;
                });
              }); 

              $(document).ready(function()
              { 
                $(document).on('submit', '#updateTruck', function()
                {  
                  $("#formbtn").attr("disabled", true);
                  $('#loadicon').show(); 
                  var data = $(this).serialize(); 
                  $.ajax({  
                    type : 'POST',
                    url  : 'tank_vehicle_save.php',
                    data : data,
                    success: function(data) {  
                      $('#exampleModal').modal("hide");  
                      $('#response').html(data);  
                      $('#user_data').DataTable().ajax.reload(null,false); 
                      $('#user_data2').DataTable().ajax.reload(null,false); 
                      $('#user_data3').DataTable().ajax.reload(null,false); 
                      $('#loadicon').hide();   
                    }
                  });
                  return false;
                });
              }); 


              $(document).ready(function()
              { 
                $(document).on('submit', '#AddQtyOTP', function()
                {  
                  $("#formbtn").attr("disabled", true);
                  $('#loadicon').show(); 
                  var data = $(this).serialize(); 
                  $.ajax({  
                    type : 'POST',
                    url  : 'tank_addstock_save.php',
                    data : data,
                    success: function(data) {  
                      $('#exampleModal').modal("hide");  
                      $('#response').html(data);  
                      $('#user_data').DataTable().ajax.reload(null,false); 
                      $('#user_data2').DataTable().ajax.reload(null,false); 
                      $('#user_data3').DataTable().ajax.reload(null,false); 
                      $('#loadicon').hide();   
                    }
                  });
                  return false;
                });
              }); 


                $(document).ready(function()
              { 
                $(document).on('submit', '#SaveQtyOTP', function()
                {  
                  $("#formbtn").attr("disabled", true);
                  $('#loadicon').show(); 
                  var data = $(this).serialize(); 
                  $.ajax({  
                    type : 'POST',
                    url  : 'tank_addstock_submit.php',
                    data : data,
                    success: function(data) {  
                      $('#exampleModal').modal("hide");  
                      $('#response').html(data);  
                      $('#user_data').DataTable().ajax.reload(null,false); 
                      $('#user_data2').DataTable().ajax.reload(null,false); 
                      $('#user_data3').DataTable().ajax.reload(null,false); 
                      $('#loadicon').hide();   
                    }
                  });
                  return false;
                });
              }); 





                     </script>
<?php 
   include_once 'footer.php';
?>