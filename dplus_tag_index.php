<?php require('header.php');
?> 
<script type="text/javascript" src="https://cdn.datatables.net/1.10.19/js/jquery.dataTables.min.js"></script>
<script type="text/javascript" src="https://cdn.datatables.net/buttons/1.5.6/js/dataTables.buttons.min.js"></script>
<script type="text/javascript" src="https://cdn.datatables.net/buttons/1.5.6/js/buttons.flash.min.js"></script>
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/jszip/3.1.3/jszip.min.js"></script>
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.53/vfs_fonts.js"></script> 
<script type="text/javascript" src="https://cdn.datatables.net/buttons/1.5.6/js/buttons.html5.min.js"></script>
<script type="text/javascript" src="https://cdn.datatables.net/buttons/1.5.6/js/buttons.print.min.js"></script>
<script type="text/javascript" src="https://cdn.datatables.net/searchbuilder/1.0.0/js/dataTables.searchBuilder.min.js"></script>
<link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/searchbuilder/1.0.0/css/searchBuilder.dataTables.min.css">

<style type="text/css">
   #user_data_paginate{background-color:#fff}a.dt-button,button.dt-button,div.dt-button{padding:.2em 1em}div.dt-button-collection{max-height:300px;overflow-y:scroll}.dataTables_scroll{margin-bottom:20px}.table{margin:0!important}.applyBtn{border-radius:0!important}table.table-bordered.dataTable td{padding:10px 5px 10px 10px}.dt-buttons{float:right!important}.user_data_filter{float:right}.dt-button{padding:5px 20px;text-transform:uppercase;font-size:12px;text-align:center;cursor:pointer;outline:0;color:#fff;background-color:#37474f;border:none;border-radius:2px;box-shadow:0 4px #999}.dt-button:hover{background-color:#3e8e41}.dt-button:active{background-color:#3e8e41;box-shadow:0 5px #666;transform:translateY(4px)}#user_data_wrapper{width:100%!important}.dt-buttons{margin-bottom:20px}#appenddiv,#appenddiv2{display:block;position:relative}.ui-autocomplete{position:absolute}.table-hover tbody tr:hover td,.table-hover tbody tr:hover th{background-color: #FFF5D7}.table td{vertical-align:middle!important;font-size:11px!important;color:#000;font-family:Verdana,Geneva,sans-serif;padding-top:4px;padding-right:4px;padding-bottom:4px;padding-left:10px}.table-bordered td{border:2px solid #e3e6f0}#user_data_info,#user_data_length{float:left}#user_data_filter,#user_data_paginate{float:right}.paginate_button{color:#000;float:left;padding:6px 12px;text-decoration:none;border:1px solid #ccc;cursor:pointer}.ellipsis{display:none}[type=search]{margin-right:10px;width:250px}.ui-autocomplete{z-index:2150000000!important}.container input{position:absolute;opacity:0;cursor:pointer;height:0;width:0}.checkmark{border-radius:2px;position:absolute;top:0;height:20px;width:20px;background-color:#fff;border:1px solid #000}.container:hover input~.checkmark{background-color:#fff}.container input:checked~.checkmark{background-color:#fff}.container input:disabled~.checkmark{background-color:#eaecf4}.checkmark:after{content:"";position:absolute;display:none}.container input:checked~.checkmark:after{display:block}.container .checkmark:after{left:6px;top:-1px;width:8px;height:16px;border:solid #000;border-width:0 3px 3px 0;-webkit-transform:rotate(45deg);-ms-transform:rotate(45deg);transform:rotate(45deg)}button:disabled,button[disabled]{border:1px solid #333!important;color:#333!important;cursor:no-drop}.table .thead-light th{text-align:center;font-size:11px;color:#444}.component{display:none}table{width:100%!important}table.table-bordered.dataTable td{white-space:nowrap;overflow:hidden;text-overflow:ellipsis}.table .thead-light th{text-transform:uppercase!important}label{text-transform:uppercase}#appenddivbill,#appenddivbillparty,#appenddivcons,#appenddivdo,#appenddivfrom,#appenddivinv,#appenddivitem,#appenddivlr,#appenddivship,#appenddivtno,#appenddivto{display:block;position:relative}.ui-autocomplete{position:absolute}.card label{color:#444}.card label{color:#444} .content{padding-bottom: 0px !important;}
   .main-panel>.content{
    padding: 0 20px 20px;
   }
   table.table-bordered.dataTable td{
    padding: 5px 5px 5px 10px;
   }
   #user_data2_info,#user_data2_length{float:left}#user_data2_filter,#user_data2_paginate{float:right}
   .user_data2_filter{float:right}
   #user_data2_wrapper{width:100%!important}
   #user_data2_paginate{background-color:#fff}

   #user_data3_info,#user_data3_length{float:left}#user_data3_filter,#user_data3_paginate{float:right}
   .user_data3_filter{float:right;}
   #user_data3_wrapper{width:100%!important}
   #user_data3_paginate{background-color:#fff}

   table.table-bordered.dataTable td{
    text-align: center;
   }

.noselect {
    cursor: default;
    -webkit-touch-callout: none;
    -webkit-user-select: none;
    -khtml-user-select: none;
    -moz-user-select: none;
    -ms-user-select: none;
    user-select: none;
}

.inStyl{
  margin:10px;
}
</style>
  
  <div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="memberModalLabel" aria-hidden="true">
<div class="modal-dialog modal-lg">
   <div class="modal-content"> 
       <div class="dash" id="modaldetail"> 
       </div> 
   </div>
</div>
</div> 

<div id="layoutSidenav_content">
    <main>
        <div class="container-fluid">
            <div class="card mb-4">
                <div class="card-body" >
                    <form id="upl52" action="" method="post" autocomplete="off"  enctype="multipart/form-data">
                    <div class="col-md-10 offset-md-1" style="padding-bottom: 20px;">
<div class="" style="padding-top: 5px; padding-bottom: 10px;">
   <h6 class="">Vehicle Tag  </h6> 
</div>
<?php 


  // $sql = "select name from rrpl_database.emp_attendance where code = '$branch_emp'";
  // if($conn->query($sql) === FALSE) {
  // throw new Exception(mysqli_error($conn)." Code 008");             
  // } 
  // $res = $conn->query($sql);
  // $row = $res->fetch_assoc();
  // echo $empname = $row['name'];
?>

                    <div class="form-row" style="background-color: #fff; border: 1px solid #ccc;">
                    <div class="inStyl col-md-3">
                      <label for="">COMPANY</label>
                      <select class="form-control" name="company" required="">
                        <option value="">-- select --</option>
                        <?php
                        $sql = $conn->query("select * from diesel_api.dplus_vehicle_groups");
                        while ($row = $sql->fetch_assoc()) {
                            echo "<option value='$row[id]'> $row[full_name] </option>";
                        }
                        ?>
                      </select>
                    </div>             
                    <!-- accept="image/*" capture="camera"  -->
<!-- accept="image/*" capture="camera"         -->
                    <div class="inStyl col-md-3">
                      <label >TRUCK PHOTO</label>
                      <input type="file" class="form-control" name="truckphoto" accept="image/x-png,image/gif,image/jpeg,application/pdf" class="form-control" style="overflow: hidden;" required="">
                    </div>
                    <div class="inStyl col-md-3">
                      <label for="">TRUCK NO</label>
                      <input type="" style="text-transform: uppercase;" oninput="this.value=this.value.replace(/[^a-z A-Z,0-9-]/,'')"  class="form-control" name="truckno" placeholder="XX-00-XX-0000" required="">
                    </div> 
                    <div class="inStyl col-md-3">
                      <label for="">TAG PHOTO</label>
                      <input type="file" class="form-control" name="tagphoto" accept="image/x-png,image/gif,image/jpeg,application/pdf" required="">
                    </div>
                    <div class="inStyl col-md-3">
                      <label for="">TAG NO</label>
                      <input type="" class="form-control" name="tagno" pattern=".{12,12}" title="12 DIGIT TAG NO" oninput="this.value=this.value.replace(/[^0-9]/,'')" placeholder="0176254XXXXX" required="">
                    </div>

                    <div class="inStyl col-md-2">
                      <label for="">PASSWORD</label>
                      <input type="password" class="form-control" name="pin" oninput="this.value=this.value.replace(/[^0-9]/,'')" placeholder="****" required="">
                    </div>

                    <div class=" col-md-1 buttondesk" style="margin-top: 18px;">
                        <button type="submit" class="btn btn-primary"> <i class="fa fa-check-circle"></i> ADD</button>
                    </div>
                    </div>    
                    </div>    
                    </form>
                </div>
            </div>
            <div class="card mb-4">
                <div class="card-body ">
                       <table id="user_data" class="table table-bordered table-hover" style="background: #fff;">
                           <thead class="thead-light">
                          <!--   <tr>
                                 <th colspan="8" style="background: #fff; font-size: 13px; letter-spacing: 0.5px;">   INVOICE - PENDING  APPROVAL </th>
                              </tr> -->
                               <tr>
                        <th>Code</th>
                        <th>Company</th>
                        <th>Truck No</th>
                        <th>Tag No</th>
                        <th>Operator</th>
                        <th>Photo</th>
                        <th>Status</th>
                        <th>#</th>
                        </tr>
                           </thead>
                        </table>
                </div>
            </div>
        </div>
    </main> 
</div>
<div id="response"></div>
<script type="text/javascript">
$(document).ready(function()
  { 
    $(document).on('submit', '#upl52', function()
    {  
      $('#loadicon').show(); 
      $('#uplbtn3').attr('disabled', true); 
      var data = new FormData($("#upl52")[0]);
      $.ajax({  
        type : 'POST',
        url  : 'dplus_tag_save.php',
        data : data,
        processData: false,
        contentType: false,
        success: function(data) {  
          $('#loadicon').hide();
          $("#upl52")[0].reset();   
          $("#response").html(data);
          $('#user_data').DataTable().ajax.reload(null, false); 
          $('#uplbtn3').attr('disabled', false); 
          $('#dataModal').modal("hide");  
        }
      });
      return false;
    });  
 }); 
</script>
<script type="text/javascript">
  jQuery( document ).ready(function() {

  var table = jQuery("#user_data").dataTable({
  "lengthMenu": [ [10, 100, 500, -1], [10, 100, 500, "All"] ], 
  "bProcessing": true,
  "searchDelay": 1000,
    "scrollY": 300,

  "scrollX": true,

  "sAjaxSource": "dplus_tag_data.php",
  "bPaginate": false,
  "sPaginationType":"full_numbers",
  "iDisplayLength": 10,
  "ordering": false,
  "buttons": [],
  "language": {
          "loadingRecords": "&nbsp;",
          "processing": "<center> <font color=brown> Please wait while Loading </font> <img src=https://www.mypsdtohtml.com/_ui/images/loading.gif height=20> </center>"
      },
  dom: "Q<\'toolbar\'>lBfrtip",  
    "searchBuilder": {
                                    "preDefined": {
                                        "criteria": [
                                            {
                                                "data": "Status",
                                                "condition": "=",
                                                "value": [" ACTIVE "]
                                            }
                                        ]
                                    }
                            },


  "initComplete": function( settings, json ) {

  }
  });   
  });


            function TagUpdate(val){
                if (confirm('Are you sure you want to delete ?')) {
                  $('#loadicon').show(); 
                  var id = val;
                  $.ajax({
                    url:"dplus_tag_modal.php",  
                    method:"post",  
                    data:{id:id},  
                    success:function(data){  
                      $('#modaldetail').html(data);  
                      $('#exampleModal').modal("show");  
                      $('#loadicon').hide(); 
                    }
                  });
                } else {
                  console.log();
                }
              }



              $(document).ready(function()
              { 
                $(document).on('submit', '#UpdateOTP', function()
                {  
                  $("#formbtn").attr("disabled", true);
                  $('#loadicon').show(); 
                  var data = $(this).serialize(); 
                  $.ajax({  
                    type : 'POST',
                    url  : 'dplus_tag_modal_save.php',
                    data : data,
                    success: function(data) {  
                      $('#exampleModal').modal("hide");  
                      $('#response').html(data);  
                      $('#user_data').DataTable().ajax.reload(null,false); 
                      $('#loadicon').hide();   
                    }
                  });
                  return false;
                });
              }); 
</script>
<?php include('footer.php'); ?>