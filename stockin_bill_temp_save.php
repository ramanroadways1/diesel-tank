<?php
   	
	require('connect.php'); 
 
    $dbid = $conn->real_escape_string($_POST['dbid']);
    $temp = $conn->real_escape_string($_POST['temp']);
     
try {
	$conn->query("START TRANSACTION"); 
  
			$sql = "select * from dairy.diesel_pump_stock where id='$dbid'";
			if ($conn->query($sql) === FALSE) {
				throw new Exception(mysqli_error($conn)." Code 002");             
         	}
			$row = $conn->query($sql)->fetch_assoc(); 
			$stockbalance = $row['balance']-($temp); 

			if($stockbalance<0){
					throw new Exception("Stock balance cannot be negative !");             
			}

			$sql = "update dairy.diesel_pump_stock set balance='$stockbalance', expshort='$temp' where id='$dbid'";
			if ($conn->query($sql) === FALSE) {
					throw new Exception(mysqli_error($conn)." Code 001");             
			}
 
			$conn->query("COMMIT");
			echo "
			<script>
			Swal.fire({
			position: 'top-end',
			icon: 'success',
			title: 'Updated Successfully',
			showConfirmButton: false,
			timer: 1000
			})
			</script>";

} catch(Exception $e) { 

			$conn->query("ROLLBACK"); 
			$content = $e->getMessage();
			$content = preg_replace("/[^0-9a-zA-Z_\.\- ]/", "", $content);  
			echo "
			<script>
			Swal.fire({
			icon: 'error',
			title: 'Error !!!',
			text: '$content'
			})
			</script>";		
} 