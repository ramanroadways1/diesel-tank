<?php
	require('connect.php'); 
 
    $dbid = $conn->real_escape_string($_POST['dbid']);
    //$tripno = $conn->real_escape_string($_POST['tripno']);
    //$today = date('Y-m-d');

try {
	$conn->query("START TRANSACTION"); 

		    $sql = "update diesel_api.cons_fuel set qty=0, rate=0, amount=0, done=0, reject=1 where id='$dbid'";
			if ($conn->query($sql) === FALSE) {
				throw new Exception(mysqli_error($conn)." Code 001");             
         	}
			
	$conn->query("COMMIT");
	echo "
	<script>
	Swal.fire({
	position: 'top-end',
	icon: 'success',
	title: 'Rejected Successfully',
	showConfirmButton: false,
	timer: 1000
	})
	</script>";

} catch(Exception $e) { 

	$conn->query("ROLLBACK"); 
	$content = $e->getMessage();
	$content = preg_replace("/[^0-9a-zA-Z_\.\- ]/", "", $content);  
	echo "
	<script>
	Swal.fire({
	icon: 'error',
	title: 'Error !!!',
	text: '$content'
	})
	</script>";		
} 