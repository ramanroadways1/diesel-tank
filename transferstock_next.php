<?php
 require('connect.php');

    $tanker = $conn->real_escape_string($_POST['tanker']);
    $fuel = $conn->real_escape_string($_POST['fuel']);
    $total = $conn->real_escape_string($_POST['total']);
    $hsdpump = $conn->real_escape_string($_POST['hsdpump']);
    $hsdbal = $conn->real_escape_string($_POST['hsdbal']);
    $hsdqty = $conn->real_escape_string($_POST['hsdqty']);
    $adipump = $conn->real_escape_string($_POST['adipump']);
    $adibal = $conn->real_escape_string($_POST['adibal']);
    $adiqty = $conn->real_escape_string($_POST['adiqty']);
    $hsdcode = $conn->real_escape_string($_POST['hsdcode']);
    $adicode = $conn->real_escape_string($_POST['adicode']);
    $TankerName = "";

          $otp = rand(111111,999999);
          $sql = "select name from rrpl_database.emp_attendance where code = '$branch_emp'";
          if($conn->query($sql) === FALSE) {
                throw new Exception(mysqli_error($conn)." Code 008");             
          } 
          $res = $conn->query($sql);
          $row = $res->fetch_assoc();
          $empname = $row['name'];

          $sql = "select * from diesel_api.dsl_cpanel where company='SMSAUTH'";
          if($conn->query($sql) === FALSE) {
                echo mysqli_error($conn);         
          }
          $resk = $conn->query($sql);
          $row = $resk->fetch_assoc();
          $smsvalue = $row['value'];
          $sms = $row['title'];
          $sms = json_decode($sms, true);
          $sms = $sms['data'];

$message="".strtolower($empname)." of ".strtolower($branch_name)." transferring stock qty $total (in litres) OTP For Approval: ".$otp."
RamanRoadways"; 
          
          $messagebackup = htmlentities($message); 
          // $message = urlencode($message);
          $senderId=$sms['senderid'];
          $route=$sms['route'];
          $country=$sms['country'];
          $authKey=$sms['authkey'];
          if($smsvalue=='1'){
                $sql = "select * from dairy.diesel_pump_branch where code='$tanker'";
                if($conn->query($sql) === FALSE) {
                    echo mysqli_error($conn);         
                }
                $res = $conn->query($sql);
                $row = $res->fetch_assoc();
                $mobileNumber = $row['mobileno'];
                $TankerName = $row['name'];
          } else {
            $mobileNumber="9549199160";
          }

                $postData = array(
                    'apikey' => $authKey,
                    'numbers' => $mobileNumber,
                    'message' => $message,
                    'sender' => $senderId,
                    'messagetype' => 'TXT'
                  );
                  $url=$sms['url'];
                  $ch = curl_init();
                  curl_setopt_array($ch, array(
                    CURLOPT_URL => $url,
                    CURLOPT_RETURNTRANSFER => true,
                    CURLOPT_POST => true,
                    CURLOPT_POSTFIELDS => $postData
                  ));

                  curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
                  curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);

                  $output = curl_exec($ch);
                  
                if(curl_errno($ch))
                {
                  echo 'error:' . curl_error($ch);
                } else {

                    // $sql = "update dairy.diesel_pump_stock set otp='$token', dplusbal='$pumpvolume' where masterid='$MasterID'";
                    // if($conn->query($sql) === FALSE) {
                    //     echo mysqli_error($conn);         
                    // }

                    $sql = "insert into dairy.diesel_pump_log (type, content) values ('message','$messagebackup')";
                    if($conn->query($sql) === FALSE) {
                        echo mysqli_error($conn);         
                    }

                }

                curl_close($ch);

  echo '
    <div id="content-wrapper" class="d-flex flex-column"> 
    <div id="content">

    <div id="updatereq_status"></div> 
     <div class="container-fluid"> 
    <div class="row"> 

    <div class="col-md-12" >
    <div class="card shadow mb-4" style="border-radius: 0px !important;"> 
    <div class="card-header">
      
    <form method="post" action="" id="saveOTP" autocomplete="off" enctype=\'multipart/form-data\'> 
    <div class="col-md-10 offset-md-1" >

    <div class="card-body"  style="background-color: #fff; border: 1px solid #ccc;">
     
      <div class="row" >

<input type="hidden" name="tanker" oninput="this.value=this.value.replace(/[^a-z A-Z 0-9.]/,\'\')" value="'.$tanker.'">
<input type="hidden" name="fuel" oninput="this.value=this.value.replace(/[^a-z A-Z 0-9.]/,\'\')" value="'.$fuel.'">
<input type="hidden" name="total" oninput="this.value=this.value.replace(/[^a-z A-Z 0-9.]/,\'\')" value="'.$total.'">
<input type="hidden" name="hsdpump" oninput="this.value=this.value.replace(/[^a-z A-Z 0-9.]/,\'\')" value="'.$hsdpump.'">
<input type="hidden" name="hsdbal" oninput="this.value=this.value.replace(/[^a-z A-Z 0-9.]/,\'\')" value="'.$hsdbal.'">
<input type="hidden" name="hsdqty" oninput="this.value=this.value.replace(/[^a-z A-Z 0-9.]/,\'\')" value="'.$hsdqty.'">
<input type="hidden" name="adipump" oninput="this.value=this.value.replace(/[^a-z A-Z 0-9.]/,\'\')" value="'.$adipump.'">
<input type="hidden" name="adibal" oninput="this.value=this.value.replace(/[^a-z A-Z 0-9.]/,\'\')" value="'.$adibal.'">
<input type="hidden" name="adiqty" oninput="this.value=this.value.replace(/[^a-z A-Z 0-9.]/,\'\')" value="'.$adiqty.'">
<input type="hidden" name="hsdcode" oninput="this.value=this.value.replace(/[^a-z A-Z 0-9.]/,\'\')" value="'.$hsdcode.'">
<input type="hidden" name="adicode" oninput="this.value=this.value.replace(/[^a-z A-Z 0-9.]/,\'\')" value="'.$adicode.'">
<input type="hidden" name="getotp" oninput="this.value=this.value.replace(/[^a-z A-Z 0-9.]/,\'\')" value="'.$otp.'">

       <div class="col-md-2">
          <label>FROM PUMP</label>
          <input type="text" class="form-control" readonly="" style="background-color: #fff;" value="'.$hsdpump.'">
        </div> 

        <div class="col-md-2">
          <label>TO PUMP</label>
          <input type="text" class="form-control" readonly="" style="background-color: #fff;" value="'.$TankerName.'">
        </div> 

        <div class="col-md-2">
          <label>FUEL</label>
          <input type="text" class="form-control" readonly="" style="background-color: #fff;" value="'.$fuel.'">
        </div> 

        <div class="col-md-2">
          <label>QTY</label>
          <input type="text" class="form-control" readonly="" style="background-color: #fff;" value="'.$total.'">
        </div> 

        <div class="col-md-2">
          <label>OTP</label>
          <input type="text" name="otp"  id="" class="form-control" oninput="this.value=this.value.replace(/[^0-9.]/,\'\')" required>
        </div> 
        
        <div class="col-md-2">
            <label> </label>
            <button class="btn btn-success" type="submit" style="margin-top: 20px;">  <i class=\'fa fa-check\'></i> SAVE  </button>
        </div>

       </div>
     
    </div>
    </form>  
    <br>  
    </div> 
    </div>
    </div>   
    </div>
    </div> 
    </div>
    </div>
    <script>
        $("form#saveOTP").submit(function(e) {
        e.preventDefault();    
        var formData = new FormData(this);
        $(\'#loadicon\').show(); 
        $.ajax({
            url: \'transferstock_save.php\',
            type: \'POST\',
            data: formData,
            success: function (data) {
                $(\'#response\').html(data);  
                $(\'#loadicon\').hide(); 
                $("#saveOTP")[0].reset(); 
            },
            cache: false,
            contentType: false,
            processData: false
        });
        });
    </script>';