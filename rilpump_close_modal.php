<?php
      require('connect.php'); 
      $id = $conn->real_escape_string($_REQUEST['id']);
      $sql = "select * from diesel_api.dsl_ril_stock where id='$id'";
      $row = $conn->query($sql)->fetch_assoc();

      $sqlcard = $conn->query("select * from diesel_api.dsl_cards where cardno='$row[cardno]'");
      $rowcard = mysqli_fetch_assoc($sqlcard); 

      $currentBAL = $getBal = $rowcard['balance'];
      $cardkano = $rowcard['cardno'];
      $cardbalance = $getBal; 
      $function = ""; 

      if($rowcard["company"]=="RELIANCE"){
        $function = "reli_donereq";
        $curl = curl_init(); 
        curl_setopt_array($curl, array(
          CURLOPT_URL => $urlGetBalance,
          CURLOPT_RETURNTRANSFER => true,
          CURLOPT_ENCODING => "",
          CURLOPT_MAXREDIRS => 10,
          CURLOPT_TIMEOUT => 0,
          CURLOPT_FOLLOWLOCATION => true,
          CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
          CURLOPT_CUSTOMREQUEST => "POST",
          CURLOPT_POSTFIELDS =>"{\n    \"CardBalance\": {\n        \"Username\": \"$reliuser\",\n        \"Password\": \"$relipass\",\n        \"SFCustomerID\": \"$relisfid\",\n        \"RILCustomerID\": \"$relirilid\",\n        \"VehicleRegistrationNumber\": \"TRIPCARD\",\n        \"CardNumber\": \"$cardkano\"\n    }\n}",
          CURLOPT_HTTPHEADER => array(
            "Content-Type: application/json"
          ),
        ));

        $response = curl_exec($curl); 
        curl_close($curl);   
        $response = json_decode($response, true); 
        $cardbalance = $response['CardBalance']['CardData'][0]['NotionalCardlimit'];

      } else if($rowcard["company"]=="RIL"){
        $function = "ril_donereq";
        $curl = curl_init(); 
        curl_setopt_array($curl, array(
          CURLOPT_URL => $urlGetBalance,
          CURLOPT_RETURNTRANSFER => true,
          CURLOPT_ENCODING => "",
          CURLOPT_MAXREDIRS => 10,
          CURLOPT_TIMEOUT => 0,
          CURLOPT_FOLLOWLOCATION => true,
          CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
          CURLOPT_CUSTOMREQUEST => "POST",
          CURLOPT_POSTFIELDS =>"{\n    \"CardBalance\": {\n        \"Username\": \"$riluser\",\n        \"Password\": \"$rilpass\",\n        \"SFCustomerID\": \"$rilsfid\",\n        \"RILCustomerID\": \"$rilrilid\",\n        \"VehicleRegistrationNumber\": \"TRIPCARD\",\n        \"CardNumber\": \"$cardkano\"\n    }\n}",
          CURLOPT_HTTPHEADER => array(
            "Content-Type: application/json"
          ),
        ));

        $response = curl_exec($curl); 
        curl_close($curl);   
        $response = json_decode($response, true); 
        $cardbalance = $response['CardBalance']['CardData'][0]['NotionalCardlimit'];
      }

      if($cardbalance==""){
        echo "GET Balance - API Error !";
        exit();
      }

      $systemcardbalance = $getBal;
      if($systemcardbalance<0){
        $cardbalance = $getBal;
      }

      $currentBAL = $cardbalance;
?>
<style type="text/css"> 
   .modal-backdrop
   {
   opacity:0.9 !important;
   background: #e9ecef;
   }

	#appenddiv, #appenddiv2 {
		display: block; 
		position:relative
	} 
	.ui-autocomplete {
		position: absolute;
	}
</style>
 
<form method="post" action="" id="CloseStockSave" role="form" autocomplete="off">
   <input type="hidden" value="<?php echo $id; ?>" name="id" oninput="this.value=this.value.replace(/[^a-z A-Z 0-9]/,'')">
   <div class="modal-body">
      <p style="color: #444;"> Close Stock (FIX PUMP CARD) <button type="button" class="close" data-dismiss="modal"> &times; </button> 
      <p style="border-bottom: 1px solid #ccc;"></p>
      </p>
      <div class="row">
         <div class="form-group col-md-3">
         	<label>Date</label>
            <input style="background: #f2f2f2; color: #000;" type="text" oninput="this.value=this.value.replace(/[^a-z A-Z 0-9]/,'')" class="form-control" id="" name="" readonly="" value="<?php echo date('d/m/Y H:i:s', strtotime($row['stamp'])) ?>">
         </div>
         <div class="form-group col-md-3">
         	<label>Cardno</label>
            <input style="background: #f2f2f2; color: #000;" type="text" oninput="this.value=this.value.replace(/[^a-z A-Z 0-9]/,'')" class="form-control" id="" name="" readonly="" value="<?php echo $row["cardno"] ?>">
         </div>
         <div class="form-group col-md-3">
         	<label>Stock Balance</label>
            <input style="background: #f2f2f2; color: #000;" type="text" oninput="this.value=this.value.replace(/[^a-z A-Z 0-9]/,'')" class="form-control" id="" name="" readonly="" value="<?php echo $row["balance"] ?>">
         </div>
         <div class="form-group col-md-3">
         	<label>Card Balance</label>
            <input style="background: #f2f2f2; color: #000;" type="text" oninput="this.value=this.value.replace(/[^a-z A-Z 0-9]/,'')" class="form-control" id="" name="" readonly="" value="<?php echo $currentBAL; ?>">
         </div> 
      </div>
   </div>
   <div class="modal-footer">
    <p style=" font-size: 12px; line-height: 18px; float: left; text-align: justify; padding-right: 20px;">रिलायंस पंप पर दिए हुए कार्ड में पैसा डालने के लिए amount डालकर request add करे पेमेंट स्टेटस H.O. से done होने पर ही पैसा निकलेगा और उस कार्ड पर stock बन जाएगा | वापस request add करने के लिए पुराने स्टॉक को क्लोज करना होगा उसके लिए आपका स्टॉक बैलेंस और कार्ड का बैलेंस same होना चाइए अगर same नहीं है तोह recheck करे कोई एंट्री सिस्टम में डालना पेंडिंग होगा |</p>
      <button type="button" id="hidemodal" class="btn btn-danger" data-dismiss="modal">CLOSE</button>
   	  <?php 
      $sub = abs(($row["balance"]-$currentBAL));
 
      if($sub>10 and $row["manual"]=='0'){
              echo '<input type="submit" id="" class="btn btn-success" name="submit" value="SAVE" disabled />';  

      } else {
              echo '<input type="submit" id="" class="btn btn-success" name="submit" value="SAVE" />';  
      }
      ?>
   </div>
</form>