<?php
 
 require('connect.php');
  
    try {
    $conn->query("START TRANSACTION"); 

      $id = $conn->real_escape_string($_REQUEST['id']);
      $sql = "select * from diesel_api.dsl_ril_stock where id='$id'";
      if ($conn->query($sql) === FALSE) {
                throw new Exception(mysqli_error($conn)." Code 001");             
      }
      $row = $conn->query($sql)->fetch_assoc();

      $sqlcard = "select * from diesel_api.dsl_cards where cardno='$row[cardno]'";
      if ($conn->query($sqlcard) === FALSE) {
                throw new Exception(mysqli_error($conn)." Code 002");             
      }
      $rowcard = $conn->query($sqlcard)->fetch_assoc();

      $currentBAL = $getBal = $rowcard['balance'];
      $cardkano = $rowcard['cardno'];
      $cardbalance = $getBal; 

      if($rowcard["company"]=="RELIANCE"){
        $curl = curl_init(); 
        curl_setopt_array($curl, array(
          CURLOPT_URL => $urlGetBalance,
          CURLOPT_RETURNTRANSFER => true,
          CURLOPT_ENCODING => "",
          CURLOPT_MAXREDIRS => 10,
          CURLOPT_TIMEOUT => 0,
          CURLOPT_FOLLOWLOCATION => true,
          CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
          CURLOPT_CUSTOMREQUEST => "POST",
          CURLOPT_POSTFIELDS =>"{\n    \"CardBalance\": {\n        \"Username\": \"$reliuser\",\n        \"Password\": \"$relipass\",\n        \"SFCustomerID\": \"$relisfid\",\n        \"RILCustomerID\": \"$relirilid\",\n        \"VehicleRegistrationNumber\": \"TRIPCARD\",\n        \"CardNumber\": \"$cardkano\"\n    }\n}",
          CURLOPT_HTTPHEADER => array(
            "Content-Type: application/json"
          ),
        ));

        $response = curl_exec($curl); 
        curl_close($curl);   
        $response = json_decode($response, true); 
        $cardbalance = $response['CardBalance']['CardData'][0]['NotionalCardlimit'];

      } else if($rowcard["company"]=="RIL"){
        $curl = curl_init(); 
        curl_setopt_array($curl, array(
          CURLOPT_URL => $urlGetBalance,
          CURLOPT_RETURNTRANSFER => true,
          CURLOPT_ENCODING => "",
          CURLOPT_MAXREDIRS => 10,
          CURLOPT_TIMEOUT => 0,
          CURLOPT_FOLLOWLOCATION => true,
          CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
          CURLOPT_CUSTOMREQUEST => "POST",
          CURLOPT_POSTFIELDS =>"{\n    \"CardBalance\": {\n        \"Username\": \"$riluser\",\n        \"Password\": \"$rilpass\",\n        \"SFCustomerID\": \"$rilsfid\",\n        \"RILCustomerID\": \"$rilrilid\",\n        \"VehicleRegistrationNumber\": \"TRIPCARD\",\n        \"CardNumber\": \"$cardkano\"\n    }\n}",
          CURLOPT_HTTPHEADER => array(
            "Content-Type: application/json"
          ),
        ));

        $response = curl_exec($curl); 
        curl_close($curl);   
        $response = json_decode($response, true); 
        $cardbalance = $response['CardBalance']['CardData'][0]['NotionalCardlimit'];
      } 

      if($cardbalance==""){
                throw new Exception("GET Balance - API Error !");             
      }

      $systemcardbalance = $getBal;
      if($systemcardbalance<0){
                throw new Exception("Card Balance Negative - Contact H.O. for clearance");             
      }

      $currentBAL = $cardbalance;

      $Difference = $currentBAL-$row["balance"];

      if(abs($Difference)>10 and $row["manual"]=='0'){
                throw new Exception("Card balance and Stock balance should be same !");             
      }

  
        $sql = "update diesel_api.dsl_ril_stock set balance='0', oldbal='$row[balance]', sysbal='$currentBAL', excess='$Difference', status='1' where id='$id'";
        if ($conn->query($sql) === FALSE) {
            throw new Exception(mysqli_error($conn)." Code 003");             
        }

        $sysdatetime = date('Y-m-d H:i:s');

        $sql = "INSERT INTO diesel_api.dsl_cards_txn (systxn, debit, card_vehno, ttype, company, cardno, credit_ref, balance, datetim) values ('1', '$getBal', '$cardkano', 'STOCK', '$rowcard[company]', '$cardkano', '$id', '0', '$sysdatetime')";
        if ($conn->query($sql) === FALSE) {
                throw new Exception(mysqli_error($conn)." Code 004");             
        } 

          $sql = "select name from rrpl_database.emp_attendance where code = '$branch_emp'";
          if($conn->query($sql) === FALSE) {
                throw new Exception(mysqli_error($conn)." Code 007");             
          } 
          $res = $conn->query($sql);
          $data = $res->fetch_assoc();
          $empname = $data['name'];

        $sql = "insert into diesel_api.dsl_logs (vehno, dsltype, cardbal, user, status, content, timestamp, trucktype, dslcomp, cardno, amount, reqid) values ('$cardkano', 'CARD',  '$getBal','$empname', 'SUCCESS', 'Stock Closer Reset', '$sysdatetime', 'STOCK', '$rowcard[company]', '$cardkano', '0', '$id')";     
        if ($conn->query($sql) === FALSE) {
                throw new Exception(mysqli_error($conn)." Code 005");             
        }

        $sql = "update diesel_api.dsl_cards set balance='0' where cardno='$cardkano'";
        if ($conn->query($sql) === FALSE) {
                throw new Exception(mysqli_error($conn)." Code 009");             
        }

        function ReliUpdateLimit($urlCreateCard,$reliuser,$relipass,$relisfid,$relirilid,$card,$amount,$sysdatetime)
        {  
            $sysdatetime = date('d-m-Y H:i:s'); 

            $curl = curl_init();

            curl_setopt_array($curl, array(
              CURLOPT_URL => $urlCreateCard,
              CURLOPT_RETURNTRANSFER => true,
              CURLOPT_ENCODING => "",
              CURLOPT_MAXREDIRS => 10,
              CURLOPT_TIMEOUT => 0,
              CURLOPT_FOLLOWLOCATION => true,
              CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
              CURLOPT_CUSTOMREQUEST => "POST",
              CURLOPT_POSTFIELDS =>"{ \"SFLCardReq\":{ \"Username\":\"$reliuser\", \"Password\":\"$relipass\", \"CardReq\":[ { \"VehicleFormType\":\"N\", \"SFCustomerID\":\"$relisfid\", \"RILCustomerID\":$relirilid, \"AssetSerialNumber\":1, \"VehicleRegistrationNumber\":\"TRIPCARD\", \"VehicleAssetType\":\"TATA\", \"VehicleMake\":\"TATA\", \"YearofRegistration\":2007, \"Ownership\":\"S\", \"MajorRoutes1\":\"ALL INDIA\", \"MajorRoutes2\":\"ALL\", \"ItemsEnabledDiesel\":\"Y\", \"ItemsEnabledPetrol\":\"1\", \"ItemsEnabledCNG\":\"1\", \"ItemsEnabledAutoGas\":\"1\", \"ItemsEnabledLubes\":\"1\", \"FacilityEnabled\":\"1\", \"PrepaidTransactionLimit\":50000, \"PrepaidDailySaleLimit\":50000, \"CreditDailysueLimit\":$amount, \"FwdCashPurseLimit\":50000, \"Recordstatus\":\"S\", \"TransactionDate\":\"$sysdatetime\", \"CardNumber\":\"$card\",  \"DriverName\":\"\", \"MobileNumber\":\"\" }]}}",
              CURLOPT_HTTPHEADER => array(
                "Content-Type: application/json"
              ),
            ));

            $response = curl_exec($curl); 
            return $response; 
            curl_close($curl); 
        }
 
        function RilUpdateLimit($urlCreateCard,$riluser,$rilpass,$rilsfid,$rilrilid,$card,$amount,$sysdatetime)
        {  
            $sysdatetime = date('d-m-Y H:i:s');   
            
            $curl = curl_init();

            curl_setopt_array($curl, array(
              CURLOPT_URL => $urlCreateCard,
              CURLOPT_RETURNTRANSFER => true,
              CURLOPT_ENCODING => "",
              CURLOPT_MAXREDIRS => 10,
              CURLOPT_TIMEOUT => 0,
              CURLOPT_FOLLOWLOCATION => true,
              CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
              CURLOPT_CUSTOMREQUEST => "POST",
              CURLOPT_POSTFIELDS =>"{ \"SFLCardReq\":{ \"Username\":\"$riluser\", \"Password\":\"$rilpass\", \"CardReq\":[ { \"VehicleFormType\":\"N\", \"SFCustomerID\":\"$rilsfid\", \"RILCustomerID\":$rilrilid, \"AssetSerialNumber\":1, \"VehicleRegistrationNumber\":\"TRIPCARD\", \"VehicleAssetType\":\"TATA\", \"VehicleMake\":\"TATA\", \"YearofRegistration\":2007, \"Ownership\":\"S\", \"MajorRoutes1\":\"ALL INDIA\", \"MajorRoutes2\":\"ALL\", \"ItemsEnabledDiesel\":\"Y\", \"ItemsEnabledPetrol\":\"1\", \"ItemsEnabledCNG\":\"1\", \"ItemsEnabledAutoGas\":\"1\", \"ItemsEnabledLubes\":\"1\", \"FacilityEnabled\":\"1\", \"PrepaidTransactionLimit\":50000, \"PrepaidDailySaleLimit\":50000, \"CreditDailysueLimit\":$amount, \"FwdCashPurseLimit\":50000, \"Recordstatus\":\"S\", \"TransactionDate\":\"$sysdatetime\", \"CardNumber\":\"$card\",  \"DriverName\":\"\", \"MobileNumber\":\"\" }]}}",
              CURLOPT_HTTPHEADER => array(
                "Content-Type: application/json"
              ),
            ));

            $response = curl_exec($curl); 
            return $response; 
            curl_close($curl); 
        }

        $card = $cardkano;
        $amount = '1';
        $sysdatetime = date('d-m-Y H:i:s');   

        if($rowcard["company"]=="RELIANCE"){
            $output = ReliUpdateLimit($urlCreateCard,$reliuser,$relipass,$relisfid,$relirilid,$card,$amount,$sysdatetime);  
        } else  if($rowcard["company"]=="RIL"){
            $output = RilUpdateLimit($urlCreateCard,$riluser,$rilpass,$rilsfid,$rilrilid,$card,$amount,$sysdatetime);  
        }

        $output = json_decode($output, true);
        $output = $output['CardRes']['CardDetails'];
         $RespMsg = $output[0]['Message'];
         $RespCode = $output[0]['StatusCode'];           

        if($RespCode=="0"){
           $RespCode = "1";
        } else {
           $RespCode = "0";
        }

        if($RespCode!="1"){
                throw new Exception("SET Limit - API Error $RespMsg !");             
        }
 
 
      $conn->query("COMMIT");
      echo "
      <script>
      Swal.fire({
      position: 'top-end',
      icon: 'success',
      title: 'Stock Closed Successfully',
      showConfirmButton: false,
      timer: 1000
      })
      </script>";
    }
    catch(Exception $e) {
        $conn->query("ROLLBACK"); 
        $content = $e->getMessage();
        $content = preg_replace("/[^0-9a-zA-Z_\.\- ]/", "", $content);  
        echo "
        <script>
        Swal.fire({
        icon: 'error',
        title: 'Error !!!',
        text: '$content'
        })
        </script>"; 
    }
 
?>