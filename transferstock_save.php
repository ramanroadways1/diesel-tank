<?php
 require('connect.php');

    $tanker = $conn->real_escape_string($_POST['tanker']);
    $fuel = $conn->real_escape_string($_POST['fuel']);
    $total = $conn->real_escape_string($_POST['total']);
    $hsdpump = $conn->real_escape_string($_POST['hsdpump']);
    $hsdbal = $conn->real_escape_string($_POST['hsdbal']);
    $hsdqty = $conn->real_escape_string($_POST['hsdqty']);
    $adipump = $conn->real_escape_string($_POST['adipump']);
    $adibal = $conn->real_escape_string($_POST['adibal']);
    $adiqty = $conn->real_escape_string($_POST['adiqty']);
    $hsdcode = $conn->real_escape_string($_POST['hsdcode']);
    $adicode = $conn->real_escape_string($_POST['adicode']);
    $getotp = $conn->real_escape_string($_POST['getotp']);
    $otp = $conn->real_escape_string($_POST['otp']);
 
try {
    $conn->query("START TRANSACTION"); 
        
        if($getotp!=$otp){
                throw new Exception("Incorrect OTP");             
        }

        $multiTank = False;

        $sql = "select * from dairy.diesel_pump_stock where status='0' and pumpcode in (SELECT code FROM dairy.diesel_pump_branch where code='$tanker')";
        if($conn->query($sql) === FALSE) {
                    throw new Exception(mysqli_error($conn)." Code 001");             
        }
        if($conn->query($sql)->num_rows>0){
                    throw new Exception("Please clear $tanker pending stocks !");             
        }

///////////////start - get balance from main stocks
       $sql = "select COALESCE(SUM(balance),0) as balance from dairy.diesel_pump_stock where pumpcode='$hsdcode' and status='0' and adi='0' group by pumpcode";
      if ($conn->query($sql) === FALSE) {
            throw new Exception(mysqli_error($conn)." Code 002");             
      }
      $res = $conn->query($sql)->fetch_assoc();
      $hsdbal = $res['balance'];

      if(($fuel=='HSD' or $fuel=='MIX') and $hsdqty>$hsdbal){
            throw new Exception("HSD qty is greater than Pump HSD Balance");             
      }

      $sql = "select COALESCE(SUM(balance),0) as balance from dairy.diesel_pump_stock where pumpcode='$adicode' and status='0' and adi='1' group by pumpcode";
      if ($conn->query($sql) === FALSE) {
            throw new Exception(mysqli_error($conn)." Code 003");             
      }
      $res = $conn->query($sql)->fetch_assoc();
      $adibal = $res['balance'];
      
      if(($fuel=='ADI' or $fuel=='MIX') and $adiqty>$adibal){
            throw new Exception("ADI qty is greater than Pump ADI Balance");             
      }
///////////////end - get balance from main stocks

                $sql = "select id+1 as id from dairy.diesel_pump_stock ORDER by id desc limit 1";
                if($conn->query($sql) === FALSE) {
                        throw new Exception(mysqli_error($conn)." Code 004");             
                }
                $res = $conn->query($sql)->fetch_assoc();
                $SetMaster = $res['id'];

/////////////// Fuel Type HSD *********************************************
      if($fuel=='HSD' or $fuel=='MIX'){
///////////////start - fuel type single/multiple entry
            $qty = $hsdqty;
            $pump_code = $hsdcode;
            $split_entry = "0";

            $sql = "SELECT id,purchaseid,masterid,totalqty,rate,balance FROM dairy.diesel_pump_stock WHERE pumpcode='$pump_code' and balance>0 and status='0' and adi='0'";
            if ($conn->query($sql) === FALSE) {
                throw new Exception(mysqli_error($conn)." Code 005");             
            } 
            $stockcount = $conn->query($sql)->num_rows;

            if($stockcount==1)
            {
                $res = $conn->query($sql)->fetch_assoc();
                
                if($qty>$res['balance'])
                {
                    throw new Exception("Out of Stock - quantity is greater than stock balance");             
                }
                
                $UpdateStockBalance = sprintf("%.2f",$res['balance']-$qty);
                $StockId = $res['id'];
                $PurchaseId = $res['purchaseid'];
                $StockQty = $res['totalqty'];
                $StockMasterId = $res['masterid'];
                
                $rate = $res['rate'];
                $amount = round($qty*$rate);
            }
            else if($stockcount==2)
            {
                
                $sql = "SELECT id,purchaseid,masterid,totalqty,rate,balance FROM dairy.diesel_pump_stock WHERE pumpcode='$pump_code' AND balance>0 and status='0' and adi='0' ORDER BY id ASC LIMIT 1";
                if ($conn->query($sql) === FALSE) {
                    throw new Exception(mysqli_error($conn)." Code 006");             
                } 
                $rowFirstPump = $conn->query($sql)->fetch_assoc();

                $sql = "SELECT id,purchaseid,masterid,totalqty,rate,balance FROM dairy.diesel_pump_stock WHERE pumpcode='$pump_code' AND balance>0 and status='0' and adi='0' ORDER BY id DESC LIMIT 1";
                if ($conn->query($sql) === FALSE) {
                    throw new Exception(mysqli_error($conn)." Code 007");             
                } 
                $rowSecondPump = $conn->query($sql)->fetch_assoc();
                 
                if(($rowFirstPump['balance']+$rowSecondPump['balance'])<$qty)
                {
                    throw new Exception("Out of Stock - quantity is greater than stock balance");             
                }
                
                if($rowFirstPump['balance']>=$qty)
                {
                    $rate = $rowFirstPump['rate'];
                    $amount = round($qty*$rate);
                    $UpdateStockBalance = sprintf("%.2f",$rowFirstPump['balance']-$qty);
                    $StockId = $rowFirstPump['id'];
                    $PurchaseId = $rowFirstPump['purchaseid'];
                    $StockQty = $rowFirstPump['totalqty'];
                    $StockMasterId = $rowFirstPump['masterid'];
                }
                else
                {
                    $split_entry="1";
                    
                    $rate = $rowFirstPump['rate'];
                    $rate2 = $rowSecondPump['rate'];
                    $qty1 = $rowFirstPump['balance'];
                    $qty2 = sprintf("%.2f",$qty-$qty1);
                    $amount = round($rate * $qty1);
                    $amount2 = round($rate2 * $qty2);
                    $StockId = $rowFirstPump['id'];
                    $StockId2 = $rowSecondPump['id'];
                    $UpdateStockBalance = sprintf("%.2f", 0);
                    $UpdateStockBalance2 = sprintf("%.2f",$rowSecondPump['balance']-$qty2);
                    $PurchaseId = $rowFirstPump['purchaseid'];
                    $StockQty = $rowFirstPump['totalqty'];
                    $PurchaseId2 = $rowSecondPump['purchaseid'];
                    $StockQty2 = $rowSecondPump['totalqty'];
                    $StockMasterId = $rowFirstPump['masterid'];
                    $StockMasterId2 = $rowSecondPump['masterid'];

                }
            }
            else
            {
                throw new Exception("There should be atleast 1 stock and atmost 2 stocks !");             
            }

///////////////end - fuel type single/multiple entry

///////////////start - insert records 
 
            $pid = md5(time()).'HSD';

            if($split_entry=='0'){

                $sql = "insert into dairy.diesel_pump_stock (openuser, purchasedate, purchaseid, purchaseamt, purchaserate, pumpcode, rate, purchaseqty, totalqty, balance, invoice, stamp, vendor, branch, adi, status, parentstock, masterid) values ('$branch_emp' ,now(), '$pid', '$amount', '$rate', '$tanker', '$rate', '$qty', '$qty', '$qty', NULL, now(), '$pump_code', NULL, '0', '0', '$StockId', '$SetMaster')";
                if($conn->query($sql) === FALSE) {
                    throw new Exception(mysqli_error($conn)." Code 008");             
                }

                $sql = "insert into diesel_api.stock_transfer (`refid`, `creditor`, `debitor`, `branch`, `creditor_comp`, `debitor_comp`, `qty`, `rate`, `amount`, `dated`, `stockid`, `timestamp`) values ('$pid','$tanker', '$pump_code', '$branch_name', 'RELIANCE', 'RELIANCE', '$qty', '$rate', '$amount', now(), '$PurchaseId', now())";
                if($conn->query($sql) === FALSE) {
                    throw new Exception(mysqli_error($conn)." Code 009");             
                }

                $sql = "Update dairy.diesel_pump_stock set balance='$UpdateStockBalance' where purchaseid='$PurchaseId'";
                if($conn->query($sql) === FALSE) {
                    throw new Exception(mysqli_error($conn)." Code 010");             
                }

            } else {
                
                $sql = "insert into dairy.diesel_pump_stock (openuser, purchasedate, purchaseid, purchaseamt, purchaserate, pumpcode, rate, purchaseqty, totalqty, balance, invoice, stamp, vendor, branch, adi, status, parentstock, masterid) values ('$branch_emp' ,now(), '$pid', '$amount', '$rate', '$tanker', '$rate', '$qty1', '$qty1', '$qty1', NULL, now(), '$pump_code', NULL, '0', '0', '$StockId', '$SetMaster')";
                if($conn->query($sql) === FALSE) {
                    throw new Exception(mysqli_error($conn)." Code 044");             
                }

                $sql = "insert into diesel_api.stock_transfer (`refid`, `creditor`, `debitor`, `branch`, `creditor_comp`, `debitor_comp`, `qty`, `rate`, `amount`, `dated`, `stockid`, `timestamp`) values ('$pid', '$tanker', '$pump_code', '$branch_name', 'RELIANCE', 'RELIANCE', '$qty1', '$rate', '$amount', now(), '$PurchaseId', now())";
                if($conn->query($sql) === FALSE) {
                    throw new Exception(mysqli_error($conn)." Code 045");             
                }

                $sql = "Update dairy.diesel_pump_stock set balance='$UpdateStockBalance' where purchaseid='$PurchaseId'";
                if($conn->query($sql) === FALSE) {
                    throw new Exception(mysqli_error($conn)." Code 046");             
                }

                $pid = $pid."002";

                $sql = "insert into dairy.diesel_pump_stock (openuser, purchasedate, purchaseid, purchaseamt, purchaserate, pumpcode, rate, purchaseqty, totalqty, balance, invoice, stamp, vendor, branch, adi, status, parentstock, masterid) values ('$branch_emp' ,now(), '$pid', '$amount2', '$rate2', '$tanker', '$rate2', '$qty2', '$qty2', '$qty2', NULL, now(), '$pump_code', NULL, '0', '0', '$StockId2', '$SetMaster')";
                if($conn->query($sql) === FALSE) {
                    throw new Exception(mysqli_error($conn)." Code 011");             
                }

                $sql = "insert into diesel_api.stock_transfer (`refid`, `creditor`, `debitor`, `branch`, `creditor_comp`, `debitor_comp`, `qty`, `rate`, `amount`, `dated`, `stockid`, `timestamp`) values ('$pid', '$tanker', '$pump_code', '$branch_name', 'RELIANCE', 'RELIANCE', '$qty2', '$rate2', '$amount2', now(), '$PurchaseId2', now())";
                if($conn->query($sql) === FALSE) {
                    throw new Exception(mysqli_error($conn)." Code 012");             
                }

                $sql = "Update dairy.diesel_pump_stock set status='1', closeuser='$branch_emp' where purchaseid='$PurchaseId'";
                if($conn->query($sql) === FALSE) {
                    throw new Exception(mysqli_error($conn)." Code 013");             
                }

                $sql = "Update dairy.diesel_pump_stock set balance='$UpdateStockBalance2' where purchaseid='$PurchaseId2'";
                if($conn->query($sql) === FALSE) {
                    throw new Exception(mysqli_error($conn)." Code 014");             
                }
            } 
///////////////end - insert records 
      }

/////////////// Fuel Type ADI *********************************************
      if($fuel=='ADI' or $fuel=='MIX'){
///////////////start - fuel type single/multiple entry
            $qty = $adiqty;
            $pump_code = $adicode;
            $split_entry = "0";

            $sql = "SELECT id,purchaseid,masterid,totalqty,rate,balance FROM dairy.diesel_pump_stock WHERE pumpcode='$pump_code' and balance>0 and status='0' and adi='1'";
            if ($conn->query($sql) === FALSE) {
                throw new Exception(mysqli_error($conn)." Code 015");             
            } 
            $stockcount = $conn->query($sql)->num_rows;

            if($stockcount==1)
            {
                $res = $conn->query($sql)->fetch_assoc();
                
                if($qty>$res['balance'])
                {
                    throw new Exception("Out of Stock - quantity is greater than stock balance");             
                }
                
                $UpdateStockBalance = sprintf("%.2f",$res['balance']-$qty);
                $StockId = $res['id'];
                $PurchaseId = $res['purchaseid'];
                $StockQty = $res['totalqty'];
                $StockMasterId = $res['masterid'];
                
                $rate = $res['rate'];
                $amount = round($qty*$rate);
            }
            else if($stockcount==2)
            {
                
                $sql = "SELECT id,purchaseid,masterid,totalqty,rate,balance FROM dairy.diesel_pump_stock WHERE pumpcode='$pump_code' AND balance>0 and status='0' and adi='1' ORDER BY id ASC LIMIT 1";
                if ($conn->query($sql) === FALSE) {
                    throw new Exception(mysqli_error($conn)." Code 016");             
                } 
                $rowFirstPump = $conn->query($sql)->fetch_assoc();

                $sql = "SELECT id,purchaseid,masterid,totalqty,rate,balance FROM dairy.diesel_pump_stock WHERE pumpcode='$pump_code' AND balance>0 and status='0' and adi='1' ORDER BY id DESC LIMIT 1";
                if ($conn->query($sql) === FALSE) {
                    throw new Exception(mysqli_error($conn)." Code 017");             
                } 
                $rowSecondPump = $conn->query($sql)->fetch_assoc();
                 
                if(($rowFirstPump['balance']+$rowSecondPump['balance'])<$qty)
                {
                    throw new Exception("Out of Stock - quantity is greater than stock balance");             
                }
                
                if($rowFirstPump['balance']>=$qty)
                {
                    $rate = $rowFirstPump['rate'];
                    $amount = round($qty*$rate);
                    $UpdateStockBalance = sprintf("%.2f",$rowFirstPump['balance']-$qty);
                    $StockId = $rowFirstPump['id'];
                    $PurchaseId = $rowFirstPump['purchaseid'];
                    $StockQty = $rowFirstPump['totalqty'];
                    $StockMasterId = $rowFirstPump['masterid'];
                }
                else
                {
                    $split_entry="1";
                    
                    $rate = $rowFirstPump['rate'];
                    $rate2 = $rowSecondPump['rate'];
                    $qty1 = $rowFirstPump['balance'];
                    $qty2 = sprintf("%.2f",$qty-$qty1);
                    $amount = round($rate * $qty1);
                    $amount2 = round($rate2 * $qty2);
                    $StockId = $rowFirstPump['id'];
                    $StockId2 = $rowSecondPump['id'];
                    $UpdateStockBalance = sprintf("%.2f", 0);
                    $UpdateStockBalance2 = sprintf("%.2f",$rowSecondPump['balance']-$qty2);
                    $PurchaseId = $rowFirstPump['purchaseid'];
                    $StockQty = $rowFirstPump['totalqty'];
                    $PurchaseId2 = $rowSecondPump['purchaseid'];
                    $StockQty2 = $rowSecondPump['totalqty'];
                    $StockMasterId = $rowFirstPump['masterid'];
                    $StockMasterId2 = $rowSecondPump['masterid'];

                }
            }
            else
            {
                throw new Exception("There should be atleast 1 stock and atmost 2 stocks !");             
            }

///////////////end - fuel type single/multiple entry

///////////////start - insert records 

                $pid = md5(time()).'ADI';

            if($split_entry=='0'){

                $sql = "insert into dairy.diesel_pump_stock (openuser, purchasedate, purchaseid, purchaseamt, purchaserate, pumpcode, rate, purchaseqty, totalqty, balance, invoice, stamp, vendor, branch, adi, status, parentstock, masterid) values ('$branch_emp' ,now(), '$pid', '$amount', '$rate', '$tanker', '$rate', '$qty', '$qty', '$qty', NULL, now(), '$pump_code', NULL, '1', '0', '$StockId', '$SetMaster')";
                if($conn->query($sql) === FALSE) {
                    throw new Exception(mysqli_error($conn)." Code 018");             
                }

                $sql = "insert into diesel_api.stock_transfer (`refid`, `creditor`, `debitor`, `branch`, `creditor_comp`, `debitor_comp`, `qty`, `rate`, `amount`, `dated`, `stockid`, `timestamp`) values ('$pid', '$tanker', '$pump_code', '$branch_name', 'RELIANCE', 'RELIANCE', '$qty', '$rate', '$amount', now(), '$PurchaseId', now())";
                if($conn->query($sql) === FALSE) {
                    throw new Exception(mysqli_error($conn)." Code 019");             
                }

                $sql = "Update dairy.diesel_pump_stock set balance='$UpdateStockBalance' where purchaseid='$PurchaseId'";
                if($conn->query($sql) === FALSE) {
                    throw new Exception(mysqli_error($conn)." Code 020");             
                }

            } else {
                
                $sql = "insert into dairy.diesel_pump_stock (openuser, purchasedate, purchaseid, purchaseamt, purchaserate, pumpcode, rate, purchaseqty, totalqty, balance, invoice, stamp, vendor, branch, adi, status, parentstock, masterid) values ('$branch_emp' ,now(), '$pid', '$amount', '$rate', '$tanker', '$rate', '$qty1', '$qty1', '$qty1', NULL, now(), '$pump_code', NULL, '1', '0', '$StockId', '$SetMaster')";
                if($conn->query($sql) === FALSE) {
                    throw new Exception(mysqli_error($conn)." Code 041");             
                }

                $sql = "insert into diesel_api.stock_transfer (`refid`, `creditor`, `debitor`, `branch`, `creditor_comp`, `debitor_comp`, `qty`, `rate`, `amount`, `dated`, `stockid`, `timestamp`) values ('$pid', '$tanker', '$pump_code', '$branch_name', 'RELIANCE', 'RELIANCE', '$qty1', '$rate', '$amount', now(), '$PurchaseId', now())";
                if($conn->query($sql) === FALSE) {
                    throw new Exception(mysqli_error($conn)." Code 042");             
                }

                $sql = "Update dairy.diesel_pump_stock set balance='$UpdateStockBalance' where purchaseid='$PurchaseId'";
                if($conn->query($sql) === FALSE) {
                    throw new Exception(mysqli_error($conn)." Code 043");             
                }
 
                $pid = $pid."002";

                $sql = "insert into dairy.diesel_pump_stock (openuser, purchasedate, purchaseid, purchaseamt, purchaserate, pumpcode, rate, purchaseqty, totalqty, balance, invoice, stamp, vendor, branch, adi, status, parentstock, masterid) values ('$branch_emp' ,now(), '$pid', '$amount2', '$rate2', '$tanker', '$rate2', '$qty2', '$qty2', '$qty2', NULL, now(), '$pump_code', NULL, '1', '0', '$StockId2', '$SetMaster')";
                if($conn->query($sql) === FALSE) {
                    throw new Exception(mysqli_error($conn)." Code 021");             
                }

                $sql = "insert into diesel_api.stock_transfer (`refid`, `creditor`, `debitor`, `branch`, `creditor_comp`, `debitor_comp`, `qty`, `rate`, `amount`, `dated`, `stockid`, `timestamp`) values ('$pid', '$tanker', '$pump_code', '$branch_name', 'RELIANCE', 'RELIANCE', '$qty2', '$rate2', '$amount2', now(), '$PurchaseId2', now())";
                if($conn->query($sql) === FALSE) {
                    throw new Exception(mysqli_error($conn)." Code 022");             
                }

                $sql = "Update dairy.diesel_pump_stock set status='1', closeuser='$branch_emp' where purchaseid='$PurchaseId'";
                if($conn->query($sql) === FALSE) {
                    throw new Exception(mysqli_error($conn)." Code 023");             
                }

                $sql = "Update dairy.diesel_pump_stock set balance='$UpdateStockBalance2' where purchaseid='$PurchaseId2'";
                if($conn->query($sql) === FALSE) {
                    throw new Exception(mysqli_error($conn)." Code 024");             
                }
            } 
///////////////end - insert records 
      }

 
    $conn->query("COMMIT");
    echo "
    <script>
    Swal.fire({
    position: 'top-end',
    icon: 'success',
    title: 'Updated Successfully',
    showConfirmButton: false,
    timer: 1000
    })
    </script>";

} catch(Exception $e) { 

    $conn->query("ROLLBACK"); 
    $content = $e->getMessage();
    $content = preg_replace("/[^0-9a-zA-Z_\.\- ]/", "", $content);  
    echo "
    <script>
    Swal.fire({
    icon: 'error',
    title: 'Error !!!',
    text: '$content'
    })
    </script>";     
} 

?>