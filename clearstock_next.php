<?php
 require('connect.php');

    $pumpcode = $conn->real_escape_string($_POST['pumpcode']);
 
try {
    $conn->query("START TRANSACTION"); 

            $sql = "select * from dairy.diesel_pump_branch WHERE code = '$_POST[pumpcode]'";
            if($conn->query($sql) === FALSE) {
                  throw new Exception(mysqli_error($conn)." Code 091");             
            }
            $resulti = $conn->query($sql);
            $rowi = $resulti->fetch_assoc();
            $pumpname = $rowi['name'];
   
            $sql = "select * from diesel_api.dplus_supplies where tank_name='$pumpname' and status='0'";
            if($conn->query($sql) === FALSE) {
                  throw new Exception(mysqli_error($conn)." Code 092");             
            }
            $count = $conn->query($sql)->num_rows;
            if($count>0){
                  throw new Exception("Approve all pending tanker entries");             
            }

        $sql = "SELECT title FROM diesel_api.dsl_cpanel where company='DIESELPLUS_LOGIN'";
        if ($conn->query($sql) === FALSE) {
                throw new Exception(mysqli_error($conn)." Code 010");             
        }
        $resx = $conn->query($sql);
        $rowx = $resx->fetch_assoc();
        define("dplus_login",$rowx["title"]);

        $sql = "SELECT title FROM diesel_api.dsl_cpanel where company='DIESELPLUS_TOKEN'";
        if ($conn->query($sql) === FALSE) {
                throw new Exception(mysqli_error($conn)." Code 011");             
        }
        $resx = $conn->query($sql);
        $rowx = $resx->fetch_assoc();
        define("dplus_token",$rowx["title"]);

        $curl = curl_init();
        curl_setopt_array($curl, array(
          CURLOPT_URL => 'https://api.dieselplus.net/api/v1/raman-roadways/tanks.json',
          CURLOPT_RETURNTRANSFER => true,
          CURLOPT_ENCODING => '',
          CURLOPT_MAXREDIRS => 10,
          CURLOPT_TIMEOUT => 30,
          CURLOPT_FOLLOWLOCATION => true,
          CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
          CURLOPT_CUSTOMREQUEST => 'GET',
          CURLOPT_HTTPHEADER => array(
            "X-Login-Name: ".dplus_login,
            "X-Auth-Token: ".dplus_token,
            "Content-Type: application/json"
          ),
        ));

        $response = curl_exec($curl);
        curl_close($curl);
        
        @$output = json_decode($response , true); 
        @$tanks = $output['tanks'];
        @$perpage = $output['meta']['per_page'];

        if($perpage!='50'){
                throw new Exception("Error in dieselplus API");             
        }

        foreach($tanks as $K=>$S){  
            $sql = "update dairy.diesel_pump_branch set volume='$S[actual_volume]' where tankid='$S[id]' ";
            if ($conn->query($sql) === FALSE) {
                throw new Exception(mysqli_error($conn)." Code 012");             
            } 
        }


          $sql = "select name from rrpl_database.emp_attendance where code = '$branch_emp'";
          if($conn->query($sql) === FALSE) {
                throw new Exception(mysqli_error($conn)." Code 008");             
          } 
          $res = $conn->query($sql);
          $row = $res->fetch_assoc();
          $empname = $row['name'];

          $sql = "select * from dairy.diesel_pump_branch WHERE code = '$_POST[pumpcode]'";
          if($conn->query($sql) === FALSE) {
                throw new Exception(mysqli_error($conn)." Code 001");             
          }
          $resulti = $conn->query($sql);
          $rowi = $resulti->fetch_assoc();
          $pumpname = $rowi['name'];
          $pumpvolume = $rowi['volume'];

          $sql = "select masterid, purchasedate, sum(expshort) as tempshort, sum(purchaseqty) as purchaseqty from dairy.diesel_pump_stock WHERE status='0' and pumpcode='$_POST[pumpcode]' group by masterid order by id asc LIMIT 1";
          if($conn->query($sql) === FALSE) {
                throw new Exception(mysqli_error($conn)." Code 002");             
          }          
          $resulto = $conn->query($sql);
          $rowo = $resulto->fetch_assoc(); 
          $purchaseqty = $rowo['purchaseqty'];
          $purchasedate = $rowo['purchasedate'];
          $MasterID = $rowo['masterid'];
          $TempShortage = $rowo['tempshort'];
          //$mobileNumber = "9549199160,9879202745,8600997434";  
          // $mobileNumber = "9549199160";  

          $otp = date('dm').substr($MasterID, -2);
          $token = sha1($otp);

          $sql = "SELECT COALESCE(sum(qty),0) as qty FROM dairy.diesel where stockid in (select purchaseid from dairy.diesel_pump_stock where masterid='$MasterID')";
          if($conn->query($sql) === FALSE) {
                throw new Exception(mysqli_error($conn)." Code 003");             
          } 
          $resk = $conn->query($sql);
          $rowk = $resk->fetch_assoc();
          $ownstock = $rowk['qty'];

          $sql = "SELECT COALESCE(sum(qty),0) as qty FROM rrpl_database.diesel_fm where stockid in (select purchaseid from dairy.diesel_pump_stock where masterid='$MasterID')";
          if($conn->query($sql) === FALSE) {
                throw new Exception(mysqli_error($conn)." Code 004");             
          }          
          $resk = $conn->query($sql);
          $rowk = $resk->fetch_assoc();
          $mktstock = $rowk['qty'];

          $sql = "SELECT COALESCE(sum(qty),0) as qty FROM diesel_api.cons_dsl where stockid in (select purchaseid from dairy.diesel_pump_stock where masterid='$MasterID')";
          if($conn->query($sql) === FALSE) {
                throw new Exception(mysqli_error($conn)." Code 005");             
          }
          $resk = $conn->query($sql);
          $rowk = $resk->fetch_assoc();
          $othstock = $rowk['qty'];

          $sql = "SELECT COALESCE(sum(qty),0) as qty FROM diesel_api.stock_transfer where stockid in (select purchaseid from dairy.diesel_pump_stock where masterid='$MasterID')";
          if($conn->query($sql) === FALSE) {
                throw new Exception(mysqli_error($conn)." Code 051");             
          }
          $resk = $conn->query($sql);
          $rowk = $resk->fetch_assoc();
          $trasferqty = $rowk['qty'];

          $totalconsum = $ownstock + $mktstock + $othstock + $trasferqty;
    
          $ActualShortage = $purchaseqty - $totalconsum - $TempShortage;

          $ActualShortage = sprintf("%.2f",$ActualShortage);

          if($ActualShortage<0){
            $shortsign = 'Excess';
          } else {
            $shortsign = 'Shortage';
          }

          if($pumpvolume<0){
            $VOLsign = 'Excess';
          } else {
            $VOLsign = 'Shortage';
          }

          if($TempShortage<0){
            $Tempsign = 'Excess';
          } else {
            $Tempsign = 'Shortage';
          }

          $ownstock = $ownstock + $trasferqty;

          $sql = "select * from diesel_api.dsl_cpanel where company='SMSAUTH'";
          if($conn->query($sql) === FALSE) {
                throw new Exception(mysqli_error($conn)." Code 092");             
          }
          $resk = $conn->query($sql);
          $row = $resk->fetch_assoc();
          $smsvalue = $row['value'];
          $sms = $row['title'];
          $sms = json_decode($sms, true);
          $sms = $sms['data'];

$message="Pump Name: ".$pumpname."
Purchase Date: ".$purchasedate."
Purchase Qty: ".$purchaseqty."
RRPL Consumption: ".$ownstock."
SCPL Consumption: ".$othstock."
Total Consumption: ".$totalconsum."
Temp. $Tempsign: ".abs($TempShortage)."
Actual $shortsign: ".abs($ActualShortage)."
DPlus $VOLsign: ".abs($pumpvolume)."
OTP For Approval: ".$otp." 
by ".strtolower($empname)." of ".strtolower($branch_name)."
RamanRoadways"; 
          
          $messagebackup = htmlentities($message); 
          // $message = urlencode($message);
          $senderId=$sms['senderid'];
          $route=$sms['route'];
          $country=$sms['country'];
          $authKey=$sms['authkey'];
          if($smsvalue=='1'){
          $mobileNumber=$sms['mobileno'];
          } else {
          $mobileNumber="9549199160";
          }
 
MsgDieselTankOTP($mobileNumber,$branch_name,strtolower($empname),$pumpname,$purchasedate,$purchaseqty,$ownstock,$othstock,$totalconsum,$Tempsign,abs($TempShortage),$shortsign,abs($ActualShortage),$VOLsign,abs($pumpvolume),$otp);

                    $sql = "update dairy.diesel_pump_stock set otp='$token', dplusbal='$pumpvolume' where masterid='$MasterID'";
                    if($conn->query($sql) === FALSE) {
                        throw new Exception(mysqli_error($conn)." Code 006");             
                    }

                    $sql = "insert into dairy.diesel_pump_log (type, content) values ('message','$messagebackup')";
                    if($conn->query($sql) === FALSE) {
                        throw new Exception(mysqli_error($conn)." Code 007");             
                    }

         
         
    $conn->query("COMMIT");
    echo "
    <script>
    Swal.fire({
    position: 'top-end',
    icon: 'success',
    title: 'OTP Sended Successfully',
    showConfirmButton: false,
    timer: 1000
    })
    </script>";

    echo '
    <div id="content-wrapper" class="d-flex flex-column"> 
    <div id="content">

    <div id="updatereq_status"></div> 
     <div class="container-fluid"> 
    <div class="row"> 

    <div class="col-md-12" >
    <div class="card shadow mb-4" style="border-radius: 0px !important;"> 
    <div class="card-header">
      
    <form method="post" action="" id="saveOTP" autocomplete="off" enctype=\'multipart/form-data\'> 
    <div class="col-md-10 offset-md-1" >

    <div class="card-body"  style="background-color: #fff; border: 1px solid #ccc;">
     
      <div class="row" >

    <input type="hidden" name="pumpcode" oninput="this.value=this.value.replace(/[^0-9.]/,\'\')" value="'.$pumpcode.'">
    <input type="hidden" name="shortage" oninput="this.value=this.value.replace(/[^0-9.]/,\'\')" value="'.$ActualShortage.'">

        <div class="col-md-2">
          <label>Pump</label>
          <input type="text" name=""  id="" class="form-control" oninput="this.value=this.value.replace(/[^0-9.]/,\'\')" readonly="" style="background-color: #fff;" value="'.$pumpname.'">
        </div> 

        <div class="col-md-2">
          <label>Purchase QTY</label>
          <input type="text" name=""  id="" class="form-control" oninput="this.value=this.value.replace(/[^0-9.]/,\'\')" readonly="" style="background-color: #fff;" value="'.$purchaseqty.'">
        </div> 

        <div class="col-md-2">
          <label>Consumption QTY</label>
          <input type="text" name=""  id="" class="form-control" oninput="this.value=this.value.replace(/[^0-9.]/,\'\')" readonly="" style="background-color: #fff;" value="'.$totalconsum.'">
        </div> 

        <div class="col-md-2">
          <label>Shortage</label>
          <input type="text" name=""  id="" class="form-control" oninput="this.value=this.value.replace(/[^0-9.]/,\'\')" readonly="" style="background-color: #fff;" value="'.$ActualShortage.'">
        </div> 

        <div class="col-md-2">
          <label>OTP</label>
          <input type="text" name="otp"  id="" class="form-control" oninput="this.value=this.value.replace(/[^0-9.]/,\'\')" required>
        </div> 
        
        <div class="col-md-2">
            <label> </label>
            <button class="btn btn-success" type="submit" style="margin-top: 20px;">  <i class=\'fa fa-check\'></i> SAVE  </button>
        </div>

       </div>
     
    </div>
    </form>  
    <br>  
    </div> 
    </div>
    </div>   
    </div>
    </div> 
    </div>
    </div>
    <script>
        $("form#saveOTP").submit(function(e) {
        e.preventDefault();    
        var formData = new FormData(this);
        $(\'#loadicon\').show(); 
        $.ajax({
            url: \'clearstock_save.php\',
            type: \'POST\',
            data: formData,
            success: function (data) {
                $(\'#response\').html(data);  
                $(\'#loadicon\').hide(); 
                $("#saveOTP")[0].reset(); 
            },
            cache: false,
            contentType: false,
            processData: false
        });
        });
    </script>';

} catch(Exception $e) { 

    $conn->query("ROLLBACK"); 
    $content = $e->getMessage();
    $content = preg_replace("/[^0-9a-zA-Z_\.\- ]/", "", $content);  
    echo "
    <script>
    Swal.fire({
    icon: 'error',
    title: 'Error !!!',
    text: '$content'
    })
    </script>";     
} 

?>