<?php include('header.php'); 
   
?>  
<script type="text/javascript" src="https://cdn.datatables.net/1.10.19/js/jquery.dataTables.min.js"></script>
<script type="text/javascript" src="https://cdn.datatables.net/buttons/1.5.6/js/dataTables.buttons.min.js"></script>
<script type="text/javascript" src="https://cdn.datatables.net/buttons/1.5.6/js/buttons.flash.min.js"></script>
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/jszip/3.1.3/jszip.min.js"></script>
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.53/vfs_fonts.js"></script> 
<script type="text/javascript" src="https://cdn.datatables.net/buttons/1.5.6/js/buttons.html5.min.js"></script>
<script type="text/javascript" src="https://cdn.datatables.net/buttons/1.5.6/js/buttons.print.min.js"></script>
<script type="text/javascript" src="https://cdn.datatables.net/searchbuilder/1.0.0/js/dataTables.searchBuilder.min.js"></script>
<link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/searchbuilder/1.0.0/css/searchBuilder.dataTables.min.css">

<style type="text/css">
  #user_data_paginate{background-color:#fff}a.dt-button,button.dt-button,div.dt-button{padding:.2em 1em}div.dt-button-collection{max-height:300px;overflow-y:scroll}.dataTables_scroll{margin-bottom:20px}.table{margin:0!important}.applyBtn{border-radius:0!important}table.table-bordered.dataTable td{padding:10px 5px 10px 10px}.dt-buttons{float:right!important}.user_data_filter{float:right}.dt-button{padding:5px 20px;text-transform:uppercase;font-size:12px;text-align:center;cursor:pointer;outline:0;color:#fff;background-color:#37474f;border:none;border-radius:2px;box-shadow:0 4px #999}.dt-button:hover{background-color:#3e8e41}.dt-button:active{background-color:#3e8e41;box-shadow:0 5px #666;transform:translateY(4px)}#user_data_wrapper{width:100%!important}.dt-buttons{margin-bottom:20px}#appenddiv,#appenddiv2{display:block;position:relative}.ui-autocomplete{position:absolute}.table-hover tbody tr:hover td,.table-hover tbody tr:hover th{background-color:#fff5d7}.table td{vertical-align:middle!important;font-size:11px!important;color:#000;font-family:Verdana,Geneva,sans-serif;padding-top:4px;padding-right:4px;padding-bottom:4px;padding-left:10px}.table-bordered td{border:3px solid #e3e6f0}#user_data_info,#user_data_length{float:left}#user_data_filter,#user_data_paginate{float:right}.paginate_button{color:#000;float:left;padding:6px 12px;text-decoration:none;border:1px solid #ccc;cursor:pointer}.ellipsis{display:none}[type=search]{margin-right:10px}.ui-autocomplete{z-index:2150000000!important}.container input{position:absolute;opacity:0;cursor:pointer;height:0;width:0}.checkmark{border-radius:2px;position:absolute;top:0;height:20px;width:20px;background-color:#fff;border:1px solid #000}.container:hover input~.checkmark{background-color:#fff}.container input:checked~.checkmark{background-color:#fff}.container input:disabled~.checkmark{background-color:#eaecf4}.checkmark:after{content:"";position:absolute;display:none}.container input:checked~.checkmark:after{display:block}.container .checkmark:after{left:6px;top:-1px;width:8px;height:16px;border:solid #000;border-width:0 3px 3px 0;-webkit-transform:rotate(45deg);-ms-transform:rotate(45deg);transform:rotate(45deg)}button:disabled,button[disabled]{border:1px solid #333!important;color:#333!important;cursor:no-drop}.table .thead-light th{text-align:center;font-size:11px;color:#444}.component{display:none}table{width:100%!important}table.table-bordered.dataTable td{white-space:nowrap;overflow:hidden;text-overflow:ellipsis}.table .thead-light th{text-transform:uppercase!important}label{text-transform:uppercase}#appenddivbill,#appenddivbillparty,#appenddivcons,#appenddivdo,#appenddivfrom,#appenddivinv,#appenddivitem,#appenddivlr,#appenddivship,#appenddivtno,#appenddivto{display:block;position:relative}.ui-autocomplete{position:absolute}.card label{color:#444}.card label{color:#444}.content{padding-bottom:0!important}.main-panel>.content{padding:0 20px 20px}table.table-bordered.dataTable td{padding:5px 5px 5px 10px}#user_data2_info,#user_data2_length{float:left}#user_data2_filter,#user_data2_paginate{float:right}.user_data2_filter{float:right}#user_data2_wrapper{width:100%!important}#user_data2_paginate{background-color:#fff}#user_data3_info,#user_data3_length{float:left}#user_data3_filter,#user_data3_paginate{float:right}.user_data3_filter{float:right}#user_data3_wrapper{width:100%!important}#user_data3_paginate{background-color:#fff}table.table-bordered.dataTable td{text-align:center}.noselect{cursor:default;-webkit-touch-callout:none;-webkit-user-select:none;-khtml-user-select:none;-moz-user-select:none;-ms-user-select:none;user-select:none}.modal-backdrop{opacity:.9!important;background:#e9ecef}#appenddiv,#appenddiv2{display:block;position:relative}.ui-autocomplete{position:absolute}.main-panel>.content{margin-top:80px}
</style>
 
<div id="response"></div>
 

<script>
$( function() {  
  $( "#getcardno" ).autocomplete({
    source: function(request, response) { 
      $.ajax({
      url: "rilpump_autocard.php",
      type: 'post',
      dataType: "json",
      data: {
      search: request.term
      },
      success: function( data ) {
      response( data );
      }
      });
    },
    select: function (event, ui) { 
      $('#getcardno').val(ui.item.value);   
      $('#balance').val(ui.item.balance);     
      $('#company').val(ui.item.company);     
      return false;
    }, 
    appendTo: '#appenddiv2',
    change: function (event, ui) {  
      if(!ui.item){
      $(event.target).val(""); 
      $(event.target).focus();
      Swal.fire({
      icon: 'error',
      title: 'Error !!!',
      text: 'Card No does not exists !'
      })} 
    },
  }); 
});
</script> 

<style type="text/css">
  label{
    text-transform: uppercase;
  }
</style>
    <!-- Content Wrapper -->
    <div id="content-wrapper" class="d-flex flex-column">

      <!-- Main Content -->
      <div id="content">

        <!-- Topbar -->

        <!-- End of Topbar -->

        <!-- Begin Page Content -->
        <div class="container-fluid" >
 
 
          <!-- Content Row -->
          <div class="row"  style="padding: 15px;">

            <div class="col-lg-12 mb-8">

               <div class="card shadow mb-4" style="border-radius: 0px;">
               
                <div class="card-body" style="padding-bottom: 0px;">
<div class="" style="padding-top: 5px; padding-bottom: 5px;">
   <h6 style=" text-transform: none;"> &nbsp; FIX PUMP CARD  </h6> 
</div>          <form  id="addreq" action="" method="POST" autocomplete="off">
            <div class="row" style="border: 1px solid #ccc; margin: 5px; padding: 15px;" >

 
<div class="col-md-1" > <img src="https://careers.ril.com/assets/images/logo.png" width="80px"></div>
             
    <div class="col-md-2">
      <label style="text-transform: uppercase;"> CARD NO </label>
       <input class="form-control" type="text" id="getcardno" name="cardno" required="">
       <div id="appenddiv2"></div>
    </div>
 
    <div class="col-md-2">
      <label> AMOUNT</label>
        <input type="number" name="amount" id="pamt" class="form-control" required="required" onkeyup="toWords()" oninput="this.value=this.value.replace(/[^0-9]/,'')" max="200000">
        
    </div> 
 
     <div class="col-md-2">
      <label style="text-transform: uppercase;"> COMPANY </label>
       <input class="form-control" type="text" id="company" name="company" style="background: #fff;" readonly="">
    </div>

    <div class="col-md-3">
      <label>AMOUNT (IN WORDS)</label>
      <input type="text" name="" min="1" readonly="" id="output" class="form-control" style="background: #fff;"> 
    </div> 

 
          <div class="col-md-2">
            <button class="btn btn-primary " style="margin-top: 20px; text-transform: none;"> <i class="fa fa-plus-circle"></i> ADD  Request</button>
          </div>

           </div>   

            <br>
 
          </form>
                </div>
              </div>


            <div class="col-md-12 " style="padding: 0px;">
            <div class="row " style="">
                  <div class="col-md-6 "  style="">
                    <div class="card shadow mb-4" style="border-radius: 0px;">
                      <div class="card-body" style="padding-bottom: 20px;">
                        <div class="col-md-12 " >
                          <table id="user_data" class="table table-bordered table-hover" style="background-color: #fff;">
                          <thead class="thead-light">
                            <tr>
                              <th> # </th> 
                              <th> Company </th> 
                              <th> Date </th> 
                              <th> CardNo </th>
                              <th> Amount </th> 
                              <th> Balance </th>  
                              <th> Status </th>  
                              <th> Payment </th> 
                            </tr>
                          </thead> 
                          </table>
                        </div>
                      </div>
                    </div>
                  </div>

                  <div class="col-md-6" style="">
                    <div class="card shadow mb-4" style="border-radius: 0px;">
                      <div class="card-body" style="padding-bottom: 20px;">
                        <div class="col-md-12">
                          <table id="user_data2" class="table table-bordered table-hover" style="background-color: #fff;">
                          <thead class="thead-light">
                            <tr>
                              <th style=""> Date </th>
                              <th style=""> Company </th>
                              <th style=""> CardNo </th> 
                              <th style=""> Qty  </th>
                              <th style=""> Rate </th>
                              <th style=""> Amount </th>
                              <th style=""> VehicleNo </th>
                              <th style=""> Location </th> 
                              <th style=""> RO Name </th> 
                            </tr>
                          </thead> 
                          </table>
                        </div>
                      </div>
                    </div>
                  </div>
            </div>
            </div>




            </div>
               
              <div id="dataModal" class="modal fade">  
                    <div class="modal-dialog">  
                         <div class="modal-content" id="employee_detail">   
                         </div>  
                    </div>  
              </div>    
                <div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="memberModalLabel" aria-hidden="true">
              <div class="modal-dialog modal-lg">
                 <div class="modal-content"> 
                     <div class="dash" id="modaldetail"> 
                     </div> 
                 </div>
              </div>
              </div>  
          </div> 
        </div>
        <!-- /.container-fluid --> 
      </div>
      <!-- End of Main Content --> 
    </div>
    <!-- End of Content Wrapper -->

  <div id="addreq_status"> </div>
  <script type="text/javascript"> 

      function convert_number(number) {
            if ((number < 0) || (number > 999999999)) {
                return "Number is out of range";
            }
            var Gn = Math.floor(number / 10000000); /* Crore */
            number -= Gn * 10000000;
            var kn = Math.floor(number / 100000); /* lakhs */
            number -= kn * 100000;
            var Hn = Math.floor(number / 1000); /* thousand */
            number -= Hn * 1000;
            var Dn = Math.floor(number / 100); /* Tens (deca) */
            number = number % 100; /* Ones */
            var tn = Math.floor(number / 10);
            var one = Math.floor(number % 10);
            var res = "";

            if (Gn > 0) {
                res += (convert_number(Gn) + " Crore");
            }
            if (kn > 0) {
                res += (((res == "") ? "" : " ") +
                    convert_number(kn) + " Lakhs");
            }
            if (Hn > 0) {
                res += (((res == "") ? "" : " ") +
                    convert_number(Hn) + " Thousand");
            }

            if (Dn) {
                res += (((res == "") ? "" : " ") +
                    convert_number(Dn) + " Hundred");
            }


            var ones = Array("", "One", "Two", "Three", "Four", "Five", "Six", "Seven", "Eight", "Nine", "Ten", "Eleven", "Twelve", "Thirteen", "Fourteen", "Fifteen", "Sixteen", "Seventeen", "Eightteen", "Nineteen");
            var tens = Array("", "", "Twenty", "Thirty", "Fourty", "Fifty", "Sixty", "Seventy", "Eigthy", "Ninety");

            if (tn > 0 || one > 0) {
                if (!(res == "")) {
                    res += " and ";
                }
                if (tn < 2) {
                    res += ones[tn * 10 + one];
                } else {

                    res += tens[tn];
                    if (one > 0) {
                        res += ("-" + ones[one]);
                    }
                }
            }

            if (res == "") {
                res = "zero";
            }
            return res;
        }

        function toWords() { 
            document.getElementById("output").value = convert_number(document.getElementById("pamt").value); 
        }


  jQuery( document ).ready(function() {

  $('#loadicon').show(); 
  var table = jQuery('#user_data').dataTable({ 
  "lengthMenu": [ [10, 500, 1000, -1], [10, 500, 1000, "All"] ], 
   "scrollY": 250,

  "scrollX": true,
  "bProcessing": false,
  "sAjaxSource": "rilpump_stock_data.php",
  "bPaginate": false,
  "sPaginationType":"full_numbers",
  "iDisplayLength": 10,
  "order": [[ 1, "desc" ]], 
    "dom": 'lBfrtip',
    "ordering": false,
    "buttons": [
      'csv', 'excel', 'print'
    ],
  "aaSorting": [],
  "initComplete": function( settings, json ) {
  $('#loadicon').hide();
  }
  });  

  }); 


  jQuery( document ).ready(function() {

  $('#loadicon').show(); 
  var table = jQuery('#user_data2').dataTable({ 
  "lengthMenu": [ [10, 500, 1000, -1], [10, 500, 1000, "All"] ], 
   "scrollY": 250,

  "scrollX": true,
  "bProcessing": false,
  "sAjaxSource": "rilpump_txn_data.php",
  "bPaginate": false,
  "sPaginationType":"full_numbers",
  "iDisplayLength": 10,
  "order": [[ 1, "desc" ]], 
    "dom": 'lBfrtip',
    "ordering": false,
    "buttons": [
      'csv', 'excel', 'print'
    ],
  "aaSorting": [],
  "initComplete": function( settings, json ) {
  $('#loadicon').hide();
  }
  });  

  }); 


    $(document).on('submit', '#addreq', function()
    {  
    $('#loadicon').show(); 
      var data = $(this).serialize(); 
      $.ajax({  
        type : 'POST',
        url  : 'rilpump_card_save.php',
        data : data,
        success: function(data) {  
        $('#response').html(data);  
        $('#user_data').DataTable().ajax.reload();  
        $('#loadicon').hide(); 
        $("#addreq")[0].reset() 
        }
      });
      return false;
    });

    //   function delete_req(val){
    //   var id = val;  
    //   if (confirm('Are You Sure to Delete ?')){ 
    //     $.ajax({
    //         type: 'POST',
    //         url: 'scpl_fuel_delete.php',
    //         data: {id: val},
    //         success: function(data){ 
    //         $('#response').html(data);  
    //         $('#user_data').DataTable().ajax.reload();
    //         }
    //     });
    //   } else {

    //   }
    // }




    //  function updt(sel) { 
    //   if(sel.value == "0")
    //   {
    //     $("#qty").prop('required',true);
    //     $("#qty").prop('readonly',false);   
    //     $("#qty").val('');   
    //   } else { 
    //     $("#qty").prop('required',false);
    //     $("#qty").prop('readonly',true);   
    //     $("#qty").val('0');  
    //   } 
    // }


            function closestock(val){
                $('#loadicon').show(); 
                var id = val;
                $.ajax({
                  url:"rilpump_close_modal.php",  
                  method:"post",  
                  data:{id:id},  
                  success:function(data){  
                    $('#modaldetail').html(data);  
                    $('#exampleModal').modal("show");  
                    $('#loadicon').hide(); 
                  }
                });
              }



    $(document).on('submit', '#CloseStockSave', function()
    {  
    $('#loadicon').show(); 
      var data = $(this).serialize(); 
      $.ajax({  
        type : 'POST',
        url  : 'rilpump_close_save.php',
        data : data,
        success: function(data) {  
        $('#exampleModal').modal("hide");  
        $('#user_data').DataTable().ajax.reload();  
        $('#loadicon').hide(); 
        $('#response').html(data);  
        $("#CloseStockSave")[0].reset() 
        }
      });
      return false;
    });

  </script>
<?php include('footer.php'); ?>