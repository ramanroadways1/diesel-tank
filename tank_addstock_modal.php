<?php
   require('connect.php'); 
    $id = $conn->real_escape_string($_REQUEST['id']);
    $sql = "select * from dairy.diesel_pump_stock where masterid='$id' order by id desc";
    $row = $conn->query($sql)->fetch_assoc();
?>
<style type="text/css"> 
   .modal-backdrop
   {
   opacity:0.9 !important;
   background: #e9ecef;
   }

	#appenddiv, #appenddiv2 {
		display: block; 
		position:relative
	} 
	.ui-autocomplete {
		position: absolute;
	}
</style>

<?php
   if($row['errorcode']=='0'){
?>
 
<form method="post" action="" id="AddQtyOTP" role="form" autocomplete="off">
   <input type="hidden" value="<?php echo $id; ?>" name="dbid" oninput="this.value=this.value.replace(/[^a-z A-Z 0-9]/,'')">
   <div class="modal-body">
      <p style="color: #444;"> ADD BALANCE QTY <button type="button" class="close" data-dismiss="modal"> &times; </button> 
      <p style="border-bottom: 1px solid #ccc;"></p>
      </p>
      <div class="row">
         <div class="form-group col-md-3">
         	<label>Dated</label>
            <input type="text" oninput="this.value=this.value.replace(/[^a-z A-Z 0-9]/,'')" class="form-control" id="" name="" readonly="" value="<?php echo date('d/m/Y H:i:s', strtotime($row['stamp'])) ?>">
         </div>
         <div class="form-group col-md-3">
         	<label>PUMP</label>
            <input type="text" oninput="this.value=this.value.replace(/[^a-z A-Z 0-9]/,'')" class="form-control" id="" name="tank" readonly="" value="<?php echo $row["pumpcode"] ?>">
         </div>
         <div class="form-group col-md-3">
         	<label>Balance</label>
            <input type="text" oninput="this.value=this.value.replace(/[^a-z A-Z 0-9]/,'')" class="form-control" id="" name="" readonly="" value="<?php echo $row["balance"] ?>">
         </div>
       
         <div class="form-group col-md-3">
         	<label> Quantity</label>
            <input type="number" oninput="this.value=this.value.replace(/[^0-9]./,'')" class="form-control" id="" name="qty" required="" max="100" min="1">
         </div>
         
      </div>
   </div>
   <div class="modal-footer">
      <button type="button" id="hidemodal" class="btn btn-danger" data-dismiss="modal">CLOSE</button>
   	  <?php echo '<input type="submit" id="" class="btn btn-warning" name="submit" value="SEND OTP" />'; ?>
   </div>
</form>
<?php 
   } else if($row['errorcode']=='1'){

      ?>
 <div class="modal-body">
      <p style="color: #444;"> ADD BALANCE QTY <button type="button" class="close" data-dismiss="modal"> &times; </button> 
      <p style="border-bottom: 1px solid #ccc;"></p>
      </p>
      <div class="row">
         
         <center><p style=" padding: 40px; color: red;"> Manually balance can be updated only once !</p></center>
         
      </div>
   </div> 
      <?php

   } else {
      ?>
<form method="post" action="" id="SaveQtyOTP" role="form" autocomplete="off">
   <input type="hidden" value="<?php echo $row["id"]; ?>" name="dbid" oninput="this.value=this.value.replace(/[^a-z A-Z 0-9]/,'')">
   <input type="hidden" value="<?php echo $row["oldbal"]; ?>" name="qty" oninput="this.value=this.value.replace(/[^a-z A-Z 0-9]/,'')">
   <div class="modal-body">
      <p style="color: #444;"> ADD BALANCE QTY <button type="button" class="close" data-dismiss="modal"> &times; </button> 
      <p style="border-bottom: 1px solid #ccc;"></p>
      </p>
      <div class="row">
        
         <div class="form-group col-md-3">
            <label>PUMP</label>
            <input type="text" oninput="this.value=this.value.replace(/[^a-z A-Z 0-9]/,'')" class="form-control" id="" name="tank" readonly="" value="<?php echo $row["pumpcode"] ?>">
         </div>
         <div class="form-group col-md-3">
            <label>Balance</label>
            <input type="text" oninput="this.value=this.value.replace(/[^a-z A-Z 0-9]/,'')" class="form-control" id="" name="" readonly="" value="<?php echo $row["balance"] ?>">
         </div>
       
         <div class="form-group col-md-3">
            <label> Quantity</label>
            <input type="number" oninput="this.value=this.value.replace(/[^0-9]./,'')" class="form-control" id="" name="" value="<?php echo $row["oldbal"] ?>" max="100" min="1" readonly="">
         </div>
         
          <div class="form-group col-md-3">
            <label> OTP</label>
            <input type="number" oninput="this.value=this.value.replace(/[^0-9]/,'')" class="form-control" id="" name="otp"  required="">
         </div>

      </div>
   </div>
   <div class="modal-footer">
      <button type="button" id="hidemodal" class="btn btn-danger" data-dismiss="modal">CLOSE</button>
        <?php echo '<input type="submit" id="" class="btn btn-success" name="submit" value="SAVE" />'; ?>
   </div>
</form>


      <?php
   }
?>