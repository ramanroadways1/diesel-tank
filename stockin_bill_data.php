<?php
 
require('connect.php');
 
    $connection = new PDO('mysql:host='.$host.';dbname='.$db_name.';', $username, $password );
    $statement = $connection->prepare("select * from dairy.diesel_pump_stock where status!='1' and expshort='0' and branch='$branch_name'");
    $statement->execute();
    $result = $statement->fetchAll();
    $count = $statement->rowCount();
    $data = array();

foreach($result as $row)
{
    $sub_array = array(); 
    $sub_array[] = $row["branch"]; 

    $sub_array[] = date('d/m/Y', strtotime($row['purchasedate'])); 
    $sub_array[] = $row["purchaseqty"]; 
    $sub_array[] = $row["purchaseamt"].""; 

    $sub_array[] = $row['adi']=='0' ? 'HSD' : 'ADI';
    $sub_array[] = $row["vendor"]; 
    $sub_array[] = "<a href='".$row["invoice"]."' target='_blank'> VIEW </a>"; 

    $btn = "";
    if($row["status"]=='-1'){
        $btn = "<center><button onclick='approvstock(\"".$row['id']."\")' class='btn btn-sm btn-success' style='margin-left: 10px; color: #fff; letter-spacing: 1px;'> <i class='fa fa-check'></i> APPROVE </button></center>";
    }

    if($row["status"]=='0'){
        $btn = "<center><button onclick='updatetemp(\"".$row['id']."\")' class='btn btn-sm btn-warning' style='margin-left: 10px; color: #fff; letter-spacing: 1px;'> <i class='fa fa-edit'></i> UPDATE </button></center>";
    }

    $sub_array[] = $btn; 

    $data[] = $sub_array;
} 

    $results = array(
      "sEcho" => 1,
    "iTotalRecords" => $count,
    "iTotalDisplayRecords" => $count,
    "aaData"=>$data);

echo json_encode($results); 
exit
?>
 