<?php
 require('connect.php');

    $stockadmin = $conn->real_escape_string($_POST['stockadmin']);
    $idate = $conn->real_escape_string($_POST['idate']);
    $pqty = $conn->real_escape_string($_POST['pqty']);
    $pamt = $conn->real_escape_string($_POST['pamt']);
    $prate = $conn->real_escape_string($_POST['prate']);
    $ftype = $conn->real_escape_string($_POST['ftype']);
    $supp = $conn->real_escape_string(strtoupper($_POST['supp']));
    $pin = $conn->real_escape_string($_POST['pin']);


try {
    $conn->query("START TRANSACTION"); 

                $sql = "select title from diesel_api.dsl_cpanel where company='STOCKBILLPIN'";
                if($conn->query($sql) === FALSE) {
                        throw new Exception(mysqli_error($conn)." Code 004");             
                }
                $res = $conn->query($sql)->fetch_assoc();
                $getPIN = $res['title'];

    if($pin!=$getPIN){
        throw new Exception("Incorrect Bill Password !");             
    }

    // Configure upload directory and allowed file types 
    $upload_dir = 'uploads/'; //.DIRECTORY_SEPARATOR
    $allowed_types = array('jpg', 'png', 'jpeg', 'gif', 'pdf'); 
      
    // Define maxsize for files i.e 2MB 
    $maxsize = 2 * 1024 * 1024;  
  
    // Checks if user sent an empty form  
    if(!empty(array_filter($_FILES['file']['name']))) { 
  		
  		$bothfiles = array(); 
        // Loop through each file in files[] array 
        foreach ($_FILES['file']['tmp_name'] as $key => $value) { 
              
            $file_tmpname = $_FILES['file']['tmp_name'][$key]; 
            $file_name = $_FILES['file']['name'][$key]; 
            $file_size = $_FILES['file']['size'][$key]; 
            $file_ext = pathinfo($file_name, PATHINFO_EXTENSION); 
  			
  			$file_name = preg_replace("/[^a-zA-Z0-9.]+/", "", $file_name);
            // Set upload file path 
            $filepath = $upload_dir.time().$file_name; 
  
            // Check file type is allowed or not 
            if(in_array(strtolower($file_ext), $allowed_types)) { 
  
                // Verify file size - 2MB max  
				if ($file_size > $maxsize){
				
                throw new Exception("Error: {$file_name} - File size is larger than the allowed limit.");             

                } else {

                	 if( move_uploaded_file($file_tmpname, $filepath)) {
                    	$bothfiles[] = $filepath;
                    }  
                    else {      

                        throw new Exception("Error uploading {$file_name}");             
                    } 
                }    
            } 
            else {  
                // If file extention not valid 
                throw new Exception("Error uploading {$file_name} ({$file_ext} file type is not allowed)");             
            }  
        } 
    }  
    else { 
          
        // If no files selected 
        throw new Exception("No files selected.");             
    } 

    $pid = md5(time());

    $sql = "insert into dairy.diesel_pump_stock (purchasedate, purchaseid, purchaseamt, purchaserate, pumpcode, rate, purchaseqty, totalqty, balance, invoice, stamp, vendor, branch, adi, status) values ('$idate', '$pid', '$pamt', '$prate', 'NULL', '$prate', '$pqty', '$pqty', '0', '$bothfiles[0]', now(), '$supp', '$stockadmin', '$ftype', '-1')";
    if($conn->query($sql) === FALSE) {
        throw new Exception(mysqli_error($conn)." Code 001");             
    }

    $conn->query("COMMIT");
    echo "
    <script>
    Swal.fire({
    position: 'top-end',
    icon: 'success',
    title: 'Updated Successfully',
    showConfirmButton: false,
    timer: 1000
    })
    </script>";

} catch(Exception $e) { 

    $conn->query("ROLLBACK"); 
    $content = $e->getMessage();
    $content = preg_replace("/[^0-9a-zA-Z_\.\- ]/", "", $content);  
    echo "
    <script>
    Swal.fire({
    icon: 'error',
    title: 'Error !!!',
    text: '$content'
    })
    </script>";     
} 

?>