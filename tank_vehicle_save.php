<?php
   	
	require('connect.php'); 
 
    $dbid = $conn->real_escape_string($_POST['dbid']);

	if(isset($_POST['truckno']))
	{
		$truckno = $conn->real_escape_string(strtoupper($_POST['truckno']));
	} else {
		$truckno = NULL;
	} 
	

try {
	$conn->query("START TRANSACTION"); 


			$sql = "update diesel_api.dplus_supplies set vehicle_name='$truckno' where dbid='$dbid'";
			if ($conn->query($sql) === FALSE) {
					throw new Exception(mysqli_error($conn)." Code 001");             
			}
 
			$conn->query("COMMIT");
			echo "
			<script>
			Swal.fire({
			position: 'top-end',
			icon: 'success',
			title: 'Updated Successfully',
			showConfirmButton: false,
			timer: 1000
			})
			</script>";

} catch(Exception $e) { 

			$conn->query("ROLLBACK"); 
			$content = $e->getMessage();
			$content = preg_replace("/[^0-9a-zA-Z_\.\- ]/", "", $content);  
			echo "
			<script>
			Swal.fire({
			icon: 'error',
			title: 'Error !!!',
			text: '$content'
			})
			</script>";		
} 