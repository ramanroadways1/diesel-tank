<?php 
include_once 'header.php';
?>  

<script type="text/javascript" src="https://cdn.datatables.net/1.10.19/js/jquery.dataTables.min.js"></script>
<script type="text/javascript" src="https://cdn.datatables.net/buttons/1.5.6/js/dataTables.buttons.min.js"></script>
<script type="text/javascript" src="https://cdn.datatables.net/buttons/1.5.6/js/buttons.flash.min.js"></script>
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/jszip/3.1.3/jszip.min.js"></script>
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.53/vfs_fonts.js"></script> 
<script type="text/javascript" src="https://cdn.datatables.net/buttons/1.5.6/js/buttons.html5.min.js"></script>
<script type="text/javascript" src="https://cdn.datatables.net/buttons/1.5.6/js/buttons.print.min.js"></script>
<script type="text/javascript" src="https://cdn.datatables.net/searchbuilder/1.0.0/js/dataTables.searchBuilder.min.js"></script>
<link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/searchbuilder/1.0.0/css/searchBuilder.dataTables.min.css">

<style type="text/css">
   #user_data_paginate{background-color:#fff}a.dt-button,button.dt-button,div.dt-button{padding:.2em 1em}div.dt-button-collection{max-height:300px;overflow-y:scroll}.dataTables_scroll{margin-bottom:20px}.table{margin:0!important}.applyBtn{border-radius:0!important}table.table-bordered.dataTable td{padding:10px 5px 10px 10px}.dt-buttons{float:right!important}.user_data_filter{float:right}.dt-button{padding:5px 20px;text-transform:uppercase;font-size:12px;text-align:center;cursor:pointer;outline:0;color:#fff;background-color:#37474f;border:none;border-radius:2px;box-shadow:0 4px #999}.dt-button:hover{background-color:#3e8e41}.dt-button:active{background-color:#3e8e41;box-shadow:0 5px #666;transform:translateY(4px)}#user_data_wrapper{width:100%!important}.dt-buttons{margin-bottom:20px}#appenddiv,#appenddiv2{display:block;position:relative}.ui-autocomplete{position:absolute}.table-hover tbody tr:hover td,.table-hover tbody tr:hover th{background-color: #FFF5D7}.table td{vertical-align:middle!important;font-size:11px!important;color:#000;font-family:Verdana,Geneva,sans-serif;padding-top:4px;padding-right:4px;padding-bottom:4px;padding-left:10px}.table-bordered td{border:3px solid #e3e6f0}#user_data_info,#user_data_length{float:left}#user_data_filter,#user_data_paginate{float:right}.paginate_button{color:#000;float:left;padding:6px 12px;text-decoration:none;border:1px solid #ccc;cursor:pointer}.ellipsis{display:none}[type=search]{margin-right:10px;width:250px}.ui-autocomplete{z-index:2150000000!important}.container input{position:absolute;opacity:0;cursor:pointer;height:0;width:0}.checkmark{border-radius:2px;position:absolute;top:0;height:20px;width:20px;background-color:#fff;border:1px solid #000}.container:hover input~.checkmark{background-color:#fff}.container input:checked~.checkmark{background-color:#fff}.container input:disabled~.checkmark{background-color:#eaecf4}.checkmark:after{content:"";position:absolute;display:none}.container input:checked~.checkmark:after{display:block}.container .checkmark:after{left:6px;top:-1px;width:8px;height:16px;border:solid #000;border-width:0 3px 3px 0;-webkit-transform:rotate(45deg);-ms-transform:rotate(45deg);transform:rotate(45deg)}button:disabled,button[disabled]{border:1px solid #333!important;color:#333!important;cursor:no-drop}.table .thead-light th{text-align:center;font-size:11px;color:#444}.component{display:none}table{width:100%!important}table.table-bordered.dataTable td{white-space:nowrap;overflow:hidden;text-overflow:ellipsis}.table .thead-light th{text-transform:uppercase!important}label{text-transform:uppercase}#appenddivbill,#appenddivbillparty,#appenddivcons,#appenddivdo,#appenddivfrom,#appenddivinv,#appenddivitem,#appenddivlr,#appenddivship,#appenddivtno,#appenddivto{display:block;position:relative}.ui-autocomplete{position:absolute}.card label{color:#444}.card label{color:#444} .content{padding-bottom: 0px !important;}
   .main-panel>.content{
    padding: 0 20px 20px;
   }
   table.table-bordered.dataTable td{
    padding: 5px 5px 5px 10px;
   }
   #user_data2_info,#user_data2_length{float:left}#user_data2_filter,#user_data2_paginate{float:right}
   .user_data2_filter{float:right}
   #user_data2_wrapper{width:100%!important}
   #user_data2_paginate{background-color:#fff}

   #user_data3_info,#user_data3_length{float:left}#user_data3_filter,#user_data3_paginate{float:right}
   .user_data3_filter{float:right;}
   #user_data3_wrapper{width:100%!important}
   #user_data3_paginate{background-color:#fff}

   table.table-bordered.dataTable td{
    text-align: center;
   }

.noselect {
    cursor: default;
    -webkit-touch-callout: none;
    -webkit-user-select: none;
    -khtml-user-select: none;
    -moz-user-select: none;
    -ms-user-select: none;
    user-select: none;
}
</style>

<script type="text/javascript">
  $(function() {
    $("#hsd1").autocomplete({
      source: 'addstock_gethsd.php',
      appendTo: '#appenddiv',
      select: function (event, ui) { 
         $('#hsd1').val(ui.item.value);   
         $('#hsd2').val(ui.item.balance);
         $('#hsd4').val(ui.item.pumpcode);      
         return false;
      },
      change: function (event, ui) {
        if(!ui.item){
          $(event.target).val("");
          Swal.fire({
            icon: 'error',
            title: 'Error !!!',
            text: 'Pump does not exists !'
          })
          $("#hsd2").val("");
          $("#hsd4").val("");
          $("#hsd1").val("");
          $("#hsd1").focus();
        }
      }, 
      focus: function (event, ui){
        return false;
      }
    });
  });

  $(function() {
    $("#adi1").autocomplete({
      source: 'addstock_getadi.php',
      appendTo: '#appenddiv2',
      select: function (event, ui) { 
         $('#adi1').val(ui.item.value);   
         $('#adi2').val(ui.item.balance);
         $('#adi4').val(ui.item.pumpcode);      
         return false;
      },
      change: function (event, ui) {
        if(!ui.item){
          $(event.target).val("");
          Swal.fire({
            icon: 'error',
            title: 'Error !!!',
            text: 'Pump does not exists !'
          })
          $("#adi2").val("");
          $("#adi4").val("");
          $("#adi1").val("");
          $("#adi1").focus();
        }
      }, 
      focus: function (event, ui){
        return false;
      }
    });
  });
</script>

<div id="response"></div>

<div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="memberModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content"> 
            <div class="dash" id="modaldetail"> 
            </div> 
        </div>
    </div>
</div>

<div id="content-wrapper" class="d-flex flex-column"> 
<div id="content">

<div id="updatereq_status"></div> 
 <div class="container-fluid"> 
<div class="row"> 

<div class="col-md-12" >
<div class="card shadow mb-4" style="border-radius: 0px !important;"> 
<div class="card-header">
 

   
 
<form method="post" action="" id="UploadBill" autocomplete="off" enctype='multipart/form-data'> 
<div class="col-md-10 offset-md-1" >
<div class="" style="padding-top: 5px; padding-bottom: 10px;">
   <h6> ADD STOCK (CONSUMER PUMP   <i class="fa fa-arrow-circle-right" aria-hidden="true"></i> TANKER) </h6> 
</div>
<div class="card-body "  style="background-color: #fff; border: 1px solid #ccc;">

  <div class="row">
	<div class="col-md-3">
      <label style="text-transform: uppercase;"> TANKER </label>
      <select id="" class="form-control" name="tanker" required="required">
        <option value=""> -- select -- </option>
        <?php 
        $sql = "SELECT * FROM dairy.diesel_pump_branch where admin='$branch_name' and billtype='2'";
        $res = $conn->query($sql);
        while($row = $res->fetch_assoc()){
          echo "<option value='".$row['code']."'>".$row['name']."</option>";
        }

        ?> 
      </select>
    </div>

    <div class="col-md-2">
      <label style="text-transform: uppercase;"> Fuel  </label>
      <select id="" class="form-control" name="fuel" onChange="fueltype(this)" required="required">
        <option value=""> -- select -- </option>
        <option value="HSD"> HSD </option>
        <option value="ADI"> ADI </option>
        <option value="MIX"> MIX </option>
      </select>
    </div>
 
     <div class="col-md-2">
      <label>Total Qty</label>
      <input type="text" id="total" name="total" class="form-control" oninput="this.value=this.value.replace(/[^0-9.]/,'')" readonly="">
    </div>
    <div class="col-md-4">
      <label>(IN WORDS)</label>
      <input type="text" name="" readonly="" id="output1" class="form-control" style="background: #fff;">
    </div> 


  </div>
  <div class="row" style="margin-top: 10px;">

      <div class="col-md-3">
      <label>HSD PUMP</label>
      <input type="text" id="hsd1" name="hsdpump" class="form-control" oninput="this.value=this.value.replace(/[^a-z A-Z 0-9]/,'')" readonly="">
      <div id="appenddiv"></div>
    </div>
    <div class="col-md-2">
      <label>HSD Balance</label>
      <input type="text" id="hsd2" name="hsdbal" class="form-control" oninput="this.value=this.value.replace(/[^0-9.]/,'')" readonly="">
    </div>
    <div class="col-md-2">
      <label>HSD Qty</label>
      <input type="text" id="hsd3" name="hsdqty" class="form-control" oninput="this.value=this.value.replace(/[^0-9.]/,'')" readonly="" onkeyup="getTotal()" value="0">
    </div>

        <div class="col-md-4">
      <label>(IN WORDS)</label>
      <input type="text" name="" readonly="" id="output2" class="form-control" style="background: #fff;">
    </div> 

      </div>
  <div class="row" style="margin-top: 10px;">

    <div class="col-md-3">
      <label>ADI PUMP</label>
      <input type="text" id="adi1" name="adipump" class="form-control" oninput="this.value=this.value.replace(/[^a-z A-Z 0-9]/,'')" readonly="">
      <div id="appenddiv2"></div>
    </div>
    <div class="col-md-2">
      <label>ADI Balance</label>
      <input type="text" id="adi2" name="adibal" class="form-control" oninput="this.value=this.value.replace(/[^0-9.]/,'')" readonly="">
    </div>
    <div class="col-md-2">
      <label>ADI Qty</label>
      <input type="text" id="adi3" name="adiqty" class="form-control" oninput="this.value=this.value.replace(/[^0-9.]/,'')" readonly="" onkeyup="getTotal()" value="0">
    </div>
    <div class="col-md-4">
      <label>(IN WORDS)</label>
      <input type="text" name="" readonly="" id="output3" class="form-control" style="background: #fff;">
    </div> 
  </div>

  <div class="row" style="margin-top: 15px;">

          <div class="col-md-3">
            <label style="text-align: justify;">
              <b>महत्वपूर्ण सूचना: </b>टैंकर से जो पहला डीजल टेस्टिंग या डीजल दिया हो उसका सटीक समय, अन्यथा अगर नहीं दिआ हो तोह अभी का समय डाले ओर डीजल  भरने वाले ऑपरेटर से सुनिशचित करे |
            </label>
          </div>

      <div class="col-md-2">
      <label>OPENING DATE TIME</label>
      <input type="date" name="dd" id="" class="form-control" min="<?= date('Y-m-d',strtotime("-2 days")); ?>" max="<?= date('Y-m-d',strtotime("+0 days")); ?>"  required="">
      </div>

      <div class="col-md-1">
      <label>HH</label>
        <select class="form-control" required="" name="hh">
          <option value=""> -- </option>
          <?php
            for ($i=0;$i<24;$i++) {
              echo "<option value='".sprintf('%02d', $i)."'> ".sprintf('%02d', $i)." </option>";
            }
          ?>
        </select>
      </div>
       <div class="col-md-1">
      <label>MM</label>
        <select class="form-control" required="" name="mm">
          <option value=""> -- </option>
          <?php
            for ($i=0;$i<60;$i++) {
              echo "<option value='".sprintf('%02d', $i)."'> ".sprintf('%02d', $i)." </option>";
            }
          ?>
        </select>
      </div>

  </div>


  
  <input type="hidden" id="hsd4" name="hsdcode" value="">
  <input type="hidden" id="adi4" name="adicode" value="">

  <button class="btn btn-warning" type="submit" style="margin-top: 20px;">  <i class='fa fa-check '></i> Save </button>

</div>
</form> 

<br> 

</div> 
</div>
</div> 
<!-- 
show list of pending stocks that are not clear yet
<div id="content-wrapper" class="d-flex flex-column">
   <div id="content">
      <div id="updatereq_status"></div>
      <div class="">
         <div class="row">
            <div class="col-md-12" >
               <div class="card shadow mb-4" style="border-radius: 0px !important;">
                  <div class="card-header">
                     <div class="card-body col-md-10 offset-md-1" style="padding-top: 0px; margin-bottom: 15px;">
                        <table id="user_data" class="table table-bordered table-hover" style="background: #fff;">
                           <thead class="thead-light">
                            <tr>
                                 <th colspan="8" style="background: #fff; font-size: 13px; letter-spacing: 0.5px;">   INVOICE - PENDING  APPROVAL </th>
                              </tr>
                              <tr>
                                 <th>  BRANCH</th>
                                 <th>  DATE </th>
                                 <th>  QTY  </th>
                                 <th>  AMOUNT </th>
                                 <th>  FUEL </th>
                                 <th>  SUPPLIER </th>
                                 <th>  INVOICE</th>
                              </tr>
                           </thead>
                        </table>
                     </div> 
                     <br> 
                  </div>
               </div>
            </div>
         </div>
      </div>
   </div>
</div> -->
</div>
</div> 
</div>
</div>

<script> 

    function convert_number(number) {
            if ((number < 0) || (number > 999999999)) {
                return "Number is out of range";
            }
            var Gn = Math.floor(number / 10000000); /* Crore */
            number -= Gn * 10000000;
            var kn = Math.floor(number / 100000); /* lakhs */
            number -= kn * 100000;
            var Hn = Math.floor(number / 1000); /* thousand */
            number -= Hn * 1000;
            var Dn = Math.floor(number / 100); /* Tens (deca) */
            number = number % 100; /* Ones */
            var tn = Math.floor(number / 10);
            var one = Math.floor(number % 10);
            var res = "";

            if (Gn > 0) {
                res += (convert_number(Gn) + " Crore");
            }
            if (kn > 0) {
                res += (((res == "") ? "" : " ") +
                    convert_number(kn) + " Lakhs");
            }
            if (Hn > 0) {
                res += (((res == "") ? "" : " ") +
                    convert_number(Hn) + " Thousand");
            }

            if (Dn) {
                res += (((res == "") ? "" : " ") +
                    convert_number(Dn) + " Hundred");
            }


            var ones = Array("", "One", "Two", "Three", "Four", "Five", "Six", "Seven", "Eight", "Nine", "Ten", "Eleven", "Twelve", "Thirteen", "Fourteen", "Fifteen", "Sixteen", "Seventeen", "Eightteen", "Nineteen");
            var tens = Array("", "", "Twenty", "Thirty", "Fourty", "Fifty", "Sixty", "Seventy", "Eigthy", "Ninety");

            if (tn > 0 || one > 0) {
                if (!(res == "")) {
                    res += " and ";
                }
                if (tn < 2) {
                    res += ones[tn * 10 + one];
                } else {

                    res += tens[tn];
                    if (one > 0) {
                        res += ("-" + ones[one]);
                    }
                }
            }

            if (res == "") {
                res = "zero";
            }
            return res;
        }

function getTotal() {
  var sum = parseFloat($('#hsd3').val())+parseFloat($('#adi3').val())+0.00;
  sum = sum.toFixed(2);
  $('#total').val(sum);

  
  document.getElementById("output1").value = convert_number(document.getElementById("total").value); 
  document.getElementById("output2").value = convert_number(document.getElementById("hsd3").value); 
  document.getElementById("output3").value = convert_number(document.getElementById("adi3").value); 

}

$("form#UploadBill").submit(function(e) {
    e.preventDefault();    
    var formData = new FormData(this);
                $('#loadicon').show(); 

    $.ajax({
        url: 'addstock_save.php',
        type: 'POST',
        data: formData,
        success: function (data) {
            $('#response').html(data);  
            document.getElementById("UploadBill").reset();
            // $('#user_data').DataTable().ajax.reload(null,false); 
                            $('#loadicon').hide(); 

        },
        cache: false,
        contentType: false,
        processData: false
    });
});

jQuery( document ).ready(function() {

  var table = jQuery("#user_data").dataTable({
  "lengthMenu": [ [10, 100, 500, -1], [10, 100, 500, "All"] ], 
  "bProcessing": true,
  "searchDelay": 1000,
  "scrollY": 100,
  "scrollX": true,
  "sAjaxSource": "stock_bill_data.php",
  "bPaginate": false,
  "sPaginationType":"full_numbers",
  "iDisplayLength": 10,
  "ordering": false,
  "buttons": [],
  "language": {
          "loadingRecords": "&nbsp;",
          "processing": "<center> <font color=brown> Please wait while Loading </font> <img src=https://www.mypsdtohtml.com/_ui/images/loading.gif height=20> </center>"
      },
  dom: "<\'toolbar\'>lBfrtip", 
  "columnDefs":[
  {

  "targets": [ 1 ],
  "visible": false 
  },
  ], 
  "initComplete": function( settings, json ) {
  }
  });   
  });

  $(document).ready(function() { 
  var table = $("#user_data").DataTable(); 
  $("#table-filter").on("change", function(){
  table.search(this.value).draw();   
  });
  } );   

</script>



<script type="text/javascript">
  function fueltype(sel){

    if(sel.value == "HSD"){

      $("#hsd1").prop('required', true);
      $("#hsd1").prop('readonly', false);

      $("#hsd3").prop('required', true);
      $("#hsd3").prop('readonly', false);

      $("#adi1").prop('required', false);
      $("#adi1").prop('readonly', true);

      $("#adi3").prop('required', false);
      $("#adi3").prop('readonly', true);
      $("#adi3").val('0');

      getTotal();

    } else if(sel.value == "ADI"){

      $("#adi1").prop('required', true);
      $("#adi1").prop('readonly', false);

      $("#adi3").prop('required', true);
      $("#adi3").prop('readonly', false);

      $("#hsd1").prop('required', false);
      $("#hsd1").prop('readonly', true);

      $("#hsd3").prop('required', false);
      $("#hsd3").prop('readonly', true);
      $("#hsd3").val('0');

      getTotal();

    } else if(sel.value == "MIX"){

      $("#hsd1").prop('required', true);
      $("#hsd1").prop('readonly', false);

      $("#hsd3").prop('required', true);
      $("#hsd3").prop('readonly', false);

      $("#adi1").prop('required', true);
      $("#adi1").prop('readonly', false);

      $("#adi3").prop('required', true);
      $("#adi3").prop('readonly', false);
   
   } else {

      $("#hsd1").prop('required', false);
      $("#hsd1").prop('readonly', true);

      $("#hsd3").prop('required', false);
      $("#hsd3").prop('readonly', true);

      $("#adi1").prop('required', false);
      $("#adi1").prop('readonly', true);

      $("#adi3").prop('required', false);
      $("#adi3").prop('readonly', true);

    }

  }
</script>
<?php 
  include_once 'footer.php';
?>  
 