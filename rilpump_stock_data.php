<?php

      require('connect.php');

      $DATABASE = $DATABASE_rrpl; 

      $connection = new PDO('mysql:host='.$DATABASE_HOST.';dbname='.$DATABASE.';', $DATABASE_USER, $DATABASE_PASS );
      $statement = $connection->prepare("SELECT diesel_api.dsl_ril_stock.*, d.company from diesel_api.dsl_ril_stock
        left join diesel_api.dsl_cards d on d.cardno = diesel_api.dsl_ril_stock.cardno
        where diesel_api.dsl_ril_stock.cardno in (SELECT cardno FROM diesel_api.dsl_cards  where rilpre='1') and branch='$branch_name' order by id desc");  
// and branch='$branch_name'
  $statement->execute();
  $result = $statement->fetchAll();
  $count = $statement->rowCount();
  $data = array();

foreach($result as $row)
{ 
  $sub_array = array(); 

  // if($row['done']==1){
  //   $stat = "".strtolower($row['done_user'])." <br> <sub>$row[done_time]</sub>";
  //       $btn=" <center> <button onclick='approv_req(".$row['id'].")' class='btn btn-sm btn-success'> <i class='fa fa-check'></i> <b>APPROV</b> </button> </center>";

  // } else { 
  //   $stat = " - ";
  //       $btn = " <center> <button onclick='delete_req(".$row['id'].")' class='btn btn-sm btn-danger'> <i class='fa fa-times-circle'></i> <b>DELETE</b> </button> </center>";

  // }

  if($row['status']=='0'){
    $stat2 = 'active';
    $btn = "<button onclick='closestock($row[id])' class='btn btn-sm btn-warning' style='padding: 1px 8px; font-size: 12px;'> <i class='fa fa-close'> </i> </button>"; 
  } else {
    $stat2 = 'closed';
    $btn = "<button onclick='closestock($row[id])' class='btn btn-sm btn-warning' style='padding: 1px 8px; font-size: 12px;' disabled> <i class='fa fa-close'> </i> </button>"; 
  }

  $view = "<a class='btn btn-sm btn-primary' style='padding: 1px 8px; font-size: 12px;' href='rilpump_print.php?p=$row[stockid]' target='_blank'> <i class='fa fa-list'> </i> </a>";

  $sub_array[] = " $view $btn"; 
  $sub_array[] = $conn -> real_escape_string($row['company']);
  $sub_array[] = date('d/m/Y', strtotime($row['reqdate']));
  $sub_array[] = "'".$conn -> real_escape_string($row['cardno']);
  $sub_array[] = $conn -> real_escape_string($row['totalamt']);
  $sub_array[] = $conn -> real_escape_string($row['balance']);


  $sub_array[] = $stat2;

  if($row['payment']=='1'){
    $stat = 'done';
  } else {
    $stat = 'not done';
  }
  $sub_array[] = $stat;


  $data[] = $sub_array;
} 

    $results = array(
    "sEcho" => 1,
    "iTotalRecords" => $count,
    "iTotalDisplayRecords" => $count,
    "aaData"=>$data);

echo json_encode($results); 
exit
?>