<?php

  require('connect.php'); 
  
  $p = $_REQUEST['p'];

  $connection = new PDO('mysql:host='.$DATABASE_HOST.';dbname='.$DATABASE_rrpl.';', $DATABASE_USER, $DATABASE_PASS );

$statement = $connection->prepare("SELECT s.*, o.name as pumpname, truncate((COALESCE(b1.qty,0) + COALESCE(b2.qty,0)+ COALESCE(b6.qty,0) + COALESCE(b3.qty,0)),2) as fuel FROM dairy.diesel_pump_stock s 
left join (SELECT * from dairy.diesel_pump_own group by code) o on o.code = s.pumpcode
LEFT JOIN (SELECT COALESCE(sum(qty),0) as qty, stockid FROM dairy.diesel group by stockid) b1 on b1.stockid = s.purchaseid
LEFT JOIN (SELECT COALESCE(sum(qty),0) as qty, stockid FROM rrpl_database.diesel_fm group by stockid) b2 on b2.stockid = s.purchaseid
LEFT JOIN (SELECT COALESCE(sum(qty),0) as qty, stockid FROM diesel_api.cons_dsl where tno!='SHORTAGE' group by stockid) b3 on b3.stockid = s.purchaseid
LEFT JOIN (SELECT COALESCE(sum(qty),0) as qty, stockid FROM  diesel_api.stock_transfer group by stockid) b6 on b6.stockid = s.purchaseid
where s.pumpcode='$p' ORDER BY s.id desc"); 
 
  $statement->execute();
  $result = $statement->fetchAll();
  $count = $statement->rowCount();
  $data = array();

foreach($result as $row)
{ 
  $sub_array = array(); 
  
  $sub_array[] = $row["masterid"]; 

  $sub_array[] = "<center> <button onclick=\"window.open('inputs_print.php?p=".$row["purchaseid"]."', '_blank')\" class='btn btn-sm btn-warning' style='padding: 3px 8px; font-size:12px !important;'> <i class='fa fa-list-ol '></i>   </button> </center>";  

  $sub_array[] = date('d/m/Y', strtotime($row["purchasedate"]));

  $sub_array[] = $row["purchaseqty"]; 
  $purchaseid =  $row["purchaseid"]; 

  $sub_array[] = $row["fuel"];
  $sub_array[] = $row["balance"];  


if($row["expshort"]>0){
  $s1 = "S";
} else if($row["expshort"]<0){
  $s1 = "E";
} else {
  $s1 = "";
}

if($row["excess"]>0){
  $s2 = "S";
} else if($row["excess"]<0){
  $s2 = "E";
} else {
  $s2 = "";
}


  $sub_array[] = $row["expshort"]."<sub> ".$s1." </sub>"; 
  $sub_array[] = $row["excess"]."<sub> ".$s2." </sub>";  


  $data[] = $sub_array;

} 

$results = array(
  "sEcho" => 1,
    "iTotalRecords" => $count,
    "iTotalDisplayRecords" => $count,
    "aaData"=>$data);

echo json_encode($results); 
exit
?>