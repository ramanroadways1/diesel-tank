<?php

  require('connect.php'); 
  
  $p = $_REQUEST['p'];

  $connection = new PDO('mysql:host='.$DATABASE_HOST.';dbname='.$DATABASE_rrpl.';', $DATABASE_USER, $DATABASE_PASS );
 
   $statement = $connection->prepare("(
SELECT diesel_entry.branch as branch, diesel_entry.timestamp as stamp, diesel_entry.date as date, diesel_pump_branch.name, own_truck.comp, diesel_entry.tno, diesel.qty, diesel.rate, diesel.amount from dairy.diesel 
LEFT JOIN dairy.diesel_entry on diesel_entry.id=diesel.id
LEFT join dairy.own_truck on own_truck.tno=diesel.tno
LEFT join dairy.diesel_pump_branch on diesel_pump_branch.code = diesel_entry.card
where stockid='$p'
)
UNION
(
SELECT user as branch, done_time as stamp, date as date, diesel_pump_branch.name, company, tno, qty, rate, amount from diesel_api.cons_dsl
LEFT join dairy.diesel_pump_branch on diesel_pump_branch.code = diesel_api.cons_dsl.pump
where stockid='$p' and tno!='SHORTAGE'
)
UNION
(
SELECT branch as branch, donedate as stamp, dated as date, diesel_pump_branch.name, '', d2.name, qty, rate, amount from diesel_api.stock_transfer
LEFT join dairy.diesel_pump_branch on diesel_pump_branch.code = diesel_api.stock_transfer.debitor
LEFT join dairy.diesel_pump_branch as d2 on d2.code = diesel_api.stock_transfer.creditor
where stockid='$p'
)
order by date");
 
  $statement->execute();
  $result = $statement->fetchAll();
  $count = $statement->rowCount();
  $data = array();

foreach($result as $row)
{ 
  $sub_array = array(); 
 
 if($row["comp"]=="RAMAN_ROADWAYS"){
  $comp = "RR";
 } else {
  $comp = $row["comp"];
 }

  $sub_array[] = date('d-m-Y', strtotime($row["date"]));
  $sub_array[] = date('d-m-Y', strtotime($row["date"]));
  $sub_array[] = $row["branch"];  
  $sub_array[] = $row["name"];  
  $sub_array[] = $comp;
  $sub_array[] = $row["tno"];
  $sub_array[] = $row["qty"];
  $sub_array[] = 0;
  $sub_array[] = 0;

  $data[] = $sub_array;
} 

$results = array(
  "sEcho" => 1,
    "iTotalRecords" => $count,
    "iTotalDisplayRecords" => $count,
    "aaData"=>$data);

echo json_encode($results); 
exit
?>