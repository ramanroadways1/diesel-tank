<?php
 
require('connect.php');
 
    $connection = new PDO('mysql:host='.$host.';dbname='.$db_name.';', $username, $password );
    $statement = $connection->prepare("SELECT s.* from diesel_api.dplus_supplies s left join dairy.diesel_pump_branch d on s.tank_name=d.name where s.status='0' and automode!='0' and d.admin='$branch_name' order by s.dbid asc");
    $statement->execute();
    $result = $statement->fetchAll();
    $count = $statement->rowCount();
    $data = array();

foreach($result as $row)
{
    $sub_array = array(); 

    $sub_array[] = $row["number"]; 
    $sub_array[] = date('Y-m-d', strtotime($row['datetime'])); 
    $sub_array[] = date('d/m/Y H:i:s', strtotime($row['datetime'])); 
    $sub_array[] = $row["tank_name"]; 
    $sub_array[] = $row["vehicle_name"]."<button onclick='vehicle(\"".$row['dbid']."\")' class='btn btn-sm btn-warning' style='margin-left: 10px; color: #fff; letter-spacing: 1px; padding-top:2px; padding-bottom:2px;'> <i style='font-size:14px' class='fa fa-edit'></i>  </button>"; 
    $sub_array[] = $row["personnel_name"]; 
    $sub_array[] = $row["quantity"]; 
    $sub_array[] = "<center><button onclick='stock(\"".$row['dbid']."\")' class='btn btn-sm btn-success' style='margin-left: 10px; color: #fff; letter-spacing: 1px;'> <i class='fa fa-check'></i> APPROVE </button></center>"; 
    $sub_array[] = "<sup> $row[narration] </sup>"; 

    $data[] = $sub_array;
} 

    $results = array(
      "sEcho" => 1,
    "iTotalRecords" => $count,
    "iTotalDisplayRecords" => $count,
    "aaData"=>$data);

echo json_encode($results); 
exit
?>
 