<?php
   	
	require('connect.php'); 
	$timestamp = date("Y-m-d H:i:s");
	
    $dbid = $conn->real_escape_string($_POST['dbid']);

	if(isset($_POST['description']))
	{
		$description = $conn->real_escape_string(ucwords(strtolower(($_POST['description']))));
	} else {
		$description = NULL;
	}

	if(isset($_POST['tripno']))
	{
		$tripno = $conn->real_escape_string(explode("_",$_POST['tripno'])[0]);
		$tripno_new = $conn->real_escape_string(explode("_",$_POST['tripno'])[1]);
	} else {
		$tripno = NULL;
		$tripno_new = "";
	}

	if(isset($_POST['reqno']))
	{
		$reqno = $conn->real_escape_string($_POST['reqno']);
	} else {
		$reqno = NULL;
	}
	
	function displayMSG($data){
		echo "<font color='red'>".$data."</font><br>";
	}

try {
	$conn->query("START TRANSACTION"); 

///////////////start - check what type of truck it is
			$sql = "select * from diesel_api.dplus_supplies where dbid='$dbid'";
			if ($conn->query($sql) === FALSE) {
				throw new Exception(mysqli_error($conn)." Code 001");             
         	}
			$supplies = $conn->query($sql)->fetch_assoc();

              $trucktype = "Unknown Vehicle";
              
              $sql = "select tno from dairy.own_truck where tno='".preg_replace("/[^0-9a-zA-Z]/", "", $supplies["vehicle_name"])."'"; 
              if ($conn->query($sql) === FALSE) {
					throw new Exception(mysqli_error($conn)." Code 002");             
              }
              if($conn->query($sql)->num_rows>0){
                $trucktype = "RRPL Vehicle";
              }

              $sql = "select vehno from diesel_api.pump_vehicle where vehno='".preg_replace("/[^0-9a-zA-Z]/", "", $supplies["vehicle_name"])."'"; 
              if ($conn->query($sql) === FALSE) {
					throw new Exception(mysqli_error($conn)." Code 003");             
              }
              if($conn->query($sql)->num_rows>0){
                $trucktype = "RRPL Non-Trip Vehicle";
              }

              $sql = "select tno from diesel_api.cons_truck where tno='".preg_replace("/[^0-9a-zA-Z]/", "", $supplies["vehicle_name"])."'";
              if ($conn->query($sql) === FALSE) {
					throw new Exception(mysqli_error($conn)." Code 004");             
              }
              if($conn->query($sql)->num_rows>0){
                $trucktype = "SCPL Vehicle";
              }
///////////////end - check what type of truck it is

///////////////start - pump code/stock balance availability
              $sql = "select * from dairy.diesel_pump_branch where name='".trim($supplies['tank_name'])."'";
              if ($conn->query($sql) === FALSE) {
					throw new Exception(mysqli_error($conn)." Code 005");             
              }
              $res = $conn->query($sql)->fetch_assoc();
              $pump_code = $res['code'];

              if($pump_code==""){
					throw new Exception("Pump code & Pump admin does not exist");             
              }

              $sql = "SELECT COALESCE(SUM(balance),0) as balance, errorcode FROM dairy.diesel_pump_stock WHERE pumpcode='$pump_code' and status='0'";
              if ($conn->query($sql) === FALSE) {
					throw new Exception(mysqli_error($conn)." Code 006");             
              }
              $res = $conn->query($sql)->fetch_assoc();
              $pump_balance = $res['balance'];

              if($supplies['quantity']>$pump_balance and $trucktype!='Unknown Vehicle'){
					throw new Exception("Out of Stock - fuel quantity is greater than stock balance");             
              }
///////////////end - pump code/stock balance availability

///////////////start - fuel type single/multiple entry
            $qty = $supplies['quantity'];
			$split_entry = "0";

			$sql = "SELECT id,purchaseid,masterid,totalqty,rate,balance,errorcode FROM dairy.diesel_pump_stock WHERE pumpcode='$pump_code' and balance>0 and status='0'";
			if ($conn->query($sql) === FALSE) {
				throw new Exception(mysqli_error($conn)." Code 007");             
			} 
			$stockcount = $conn->query($sql)->num_rows;

			if($stockcount==1)
			{
				$res = $conn->query($sql)->fetch_assoc();
				
				if($qty>$res['balance'])
				{
					throw new Exception("Out of Stock - fuel quantity is greater than stock balance");             
				}
				
				$UpdateStockBalance = sprintf("%.2f",$res['balance']-$qty);
				$ErrorStatus = $res['errorcode'];
				$StockId = $res['id'];
				$PurchaseId = $res['purchaseid'];
				$StockQty = $res['totalqty'];
				$StockMasterId = $res['masterid'];
				
				$rate = $res['rate'];
				$amount = round($qty*$rate);
			}
			else if($stockcount==2)
			{
				
				$sql = "SELECT id,purchaseid,masterid,totalqty,rate,balance,errorcode  FROM dairy.diesel_pump_stock WHERE pumpcode='$pump_code' AND balance>0 and status='0' ORDER BY id ASC LIMIT 1";
				if ($conn->query($sql) === FALSE) {
					throw new Exception(mysqli_error($conn)." Code 008");             
				} 
				$rowFirstPump = $conn->query($sql)->fetch_assoc();

				$sql = "SELECT id,purchaseid,masterid,totalqty,rate,balance,errorcode  FROM dairy.diesel_pump_stock WHERE pumpcode='$pump_code' AND balance>0 and status='0' ORDER BY id DESC LIMIT 1";
				if ($conn->query($sql) === FALSE) {
					throw new Exception(mysqli_error($conn)." Code 009");             
				} 
				$rowSecondPump = $conn->query($sql)->fetch_assoc();
				 
				if(($rowFirstPump['balance']+$rowSecondPump['balance'])<$qty)
				{
					throw new Exception("Out of Stock - fuel quantity is greater than stock balance");             
				}
				
				if($rowFirstPump['balance']>=$qty)
				{
					$rate = $rowFirstPump['rate'];
					$amount = round($qty*$rate);
					$UpdateStockBalance = sprintf("%.2f",$rowFirstPump['balance']-$qty);
					$ErrorStatus = $rowFirstPump['errorcode'];
					$StockId = $rowFirstPump['id'];
					$PurchaseId = $rowFirstPump['purchaseid'];
					$StockQty = $rowFirstPump['totalqty'];
					$StockMasterId = $rowFirstPump['masterid'];
				}
				else
				{
					$split_entry="1";
					
					$rate = $rowFirstPump['rate'];
					$rate2 = $rowSecondPump['rate'];
					$qty1 = $rowFirstPump['balance'];
					$qty2 = sprintf("%.2f",$qty-$qty1);
					$amount = round($rate * $qty1);
					$amount2 = round($rate2 * $qty2);
					$StockId = $rowFirstPump['id'];
					$StockId2 = $rowSecondPump['id'];
					$UpdateStockBalance = sprintf("%.2f", 0);
					$ErrorStatus = $rowFirstPump['errorcode'];
					$UpdateStockBalance2 = sprintf("%.2f",$rowSecondPump['balance']-$qty2);
					$PurchaseId = $rowFirstPump['purchaseid'];
					$StockQty = $rowFirstPump['totalqty'];
					$PurchaseId2 = $rowSecondPump['purchaseid'];
					$StockQty2 = $rowSecondPump['totalqty'];
					$StockMasterId = $rowFirstPump['masterid'];
					$ErrorStatus2 = $rowSecondPump['errorcode'];
					$StockMasterId2 = $rowSecondPump['masterid'];

				}
			}
			else 
			{
                throw new Exception("There should be atleast 1 stock and atmost 2 stocks !");             
			}
///////////////end - fuel type single/multiple entry
	
///////////////start - insert records 

		$sql = "select comp from dairy.diesel_pump_own where code ='$pump_code' limit 1";
	  	if ($conn->query($sql) === FALSE) {
			throw new Exception(mysqli_error($conn)." Code 010");             
	  	}
	  	$res = $conn->query($sql)->fetch_assoc();
	  	$pump_company = $res['comp'];
 		$unq_id = $supplies['vehicle_name']."_".date('dmYHis');
 		$narration = $pump_company."-".$pump_code;
 		$today = date('Y-m-d');
 		$request = 'PUMP';
 		$truckno = preg_replace("/[^0-9a-zA-Z]/", "", $supplies["vehicle_name"]);
 		$branch_name = $branch_name;
 		$branch_emp = $branch_emp;

		if($tripno!='NA' and $tripno!=''){

			$supplies_status = '1';
		} else {

			$supplies_status = '-1';
		}


			if($trucktype == "Unknown Vehicle"){

				$sql = "update diesel_api.dplus_supplies set status='$supplies_status', trucktype='$trucktype', narration='$description' where dbid='$dbid'";
				if ($conn->query($sql) === FALSE) {
						throw new Exception(mysqli_error($conn)." Code 011");             
				}

			} else if($trucktype == "RRPL Vehicle" || $trucktype == "RRPL Non-Trip Vehicle"){
 
				if($split_entry=="1"){
					///////////////////entry 1 start
					$sql = "INSERT INTO dairy.diesel (trans_id, trip_id,trip_no, narration, stockid, unq_id, tno, amount, date, branch, 
					rate, qty, branch_user,timestamp) VALUES ('$dbid', '$tripno', '$tripno_new','$narration' ,'$PurchaseId', '$unq_id',
					'$truckno', '$amount', '$today', '$branch_name', '$rate', '$qty1', '$branch_emp','$timestamp')";
					
					if ($conn->query($sql) === FALSE) {
						throw new Exception(mysqli_error($conn)." Code 012");             
					}
					$L1 = $conn->insert_id;

					$sql = "INSERT INTO dairy.diesel_entry (narration, unq_id, tno, diesel, date, branch, card_pump, card, dsl_company, 
					veh_no,timestamp) VALUES ('$narration', '$unq_id', '$truckno', '$amount', '$today', '$branch_name', '$request', 
					'$pump_code', '$pump_company', '$pump_code','$timestamp')";
					
					
					if ($conn->query($sql) === FALSE) {
						throw new Exception(mysqli_error($conn)." Code 013");             
					}
					$L2 = $conn->insert_id;

					if($L1!=$L2){
						throw new Exception("Unable to add Diesel and Diesel Entry - ID Mismatched");             
					}
					///////////////////entry 1 end
					///////////////////entry 2 start
					$sql = "INSERT INTO dairy.diesel (trans_id, trip_id,trip_no, narration, stockid, unq_id, tno, amount, date, branch, 
					rate, qty, branch_user,timestamp) VALUES ('$dbid', '$tripno','$tripno_new', '$narration' ,'$PurchaseId2', '$unq_id',
					'$truckno', '$amount2', '$today', '$branch_name', '$rate2', '$qty2', '$branch_emp','$timestamp')";
					
					if ($conn->query($sql) === FALSE) {
						throw new Exception(mysqli_error($conn)." Code 014");             
					}
					$M1 = $conn->insert_id;

					$sql = "INSERT INTO dairy.diesel_entry (narration, unq_id, tno, diesel, date, branch, card_pump, card, dsl_company, 
					veh_no,timestamp) VALUES ('$narration', '$unq_id', '$truckno', '$amount2', '$today', '$branch_name', '$request', 
					'$pump_code', '$pump_company', '$pump_code','$timestamp')";
					
					if ($conn->query($sql) === FALSE) {
						throw new Exception(mysqli_error($conn)." Code 015");             
					}
					$M2 = $conn->insert_id;

					if($M1!=$M2){
						throw new Exception("Unable to add Diesel and Diesel Entry - ID Mismatched");             
					}
					///////////////////entry 2 end
					$sql = "update diesel_api.dplus_supplies set sale_price='$rate', sale_amount ='$amount', status='$supplies_status', trucktype='$trucktype', narration='$description', reqid='$L1\,$M1', tripid='$tripno', purchaseid='$PurchaseId\,$PurchaseId2', masterid='$StockMasterId' where dbid='$dbid'";
					if ($conn->query($sql) === FALSE) {
						throw new Exception(mysqli_error($conn)." Code 016");             
					}
				} else{

					$sql = "INSERT INTO dairy.diesel (trans_id, trip_id,trip_no, narration, stockid, unq_id, tno, amount, date, branch,
					rate, qty, branch_user,timestamp) VALUES ('$dbid', '$tripno','$tripno_new', '$narration' ,'$PurchaseId', '$unq_id', 
					'$truckno', '$amount', '$today', '$branch_name', '$rate', '$qty', '$branch_emp','$timestamp')";
					
					if ($conn->query($sql) === FALSE) {
						throw new Exception(mysqli_error($conn)." Code 017");             
					}
					$L1 = $conn->insert_id;

					$sql = "INSERT INTO dairy.diesel_entry (narration, unq_id, tno, diesel, date, branch, card_pump, card, dsl_company,
					veh_no,timestamp) VALUES ('$narration', '$unq_id', '$truckno', '$amount', '$today', '$branch_name', '$request', 
					'$pump_code', '$pump_company', '$pump_code','$timestamp')";
					
					if ($conn->query($sql) === FALSE) {
						throw new Exception(mysqli_error($conn)." Code 018");             
					}
					$L2 = $conn->insert_id;

					if($L1!=$L2){
						throw new Exception("Unable to add Diesel and Diesel Entry - ID Mismatched");             
					}

					$sql = "update diesel_api.dplus_supplies set sale_price='$rate', sale_amount ='$amount', status='$supplies_status', trucktype='$trucktype', narration='$description', reqid='$L1', tripid='$tripno', purchaseid='$PurchaseId', masterid='$StockMasterId' where dbid='$dbid'";
					if ($conn->query($sql) === FALSE) {
						throw new Exception(mysqli_error($conn)." Code 019");             
					}

				}

			} else if($trucktype == "SCPL Vehicle"){

				$sql = "select * from diesel_api.cons_dsl where id='$reqno'";
				if ($conn->query($sql) === FALSE) {
					throw new Exception(mysqli_error($conn)." Code 020");             
	         	}
				$cons = $conn->query($sql)->fetch_assoc();

				$cons_date = $cons['date'];
				$cons_company = $cons['company']; 
				$cons_tno = $cons['tno'];
				$todayTime = date('Y-m-d H:i:s');

				if($split_entry=="1"){
					
					$sql = "update diesel_api.cons_dsl set pump='$pump_code', qty='$qty1', rate='$rate', amount='$amount', stockid='$PurchaseId', done_branch='$branch_name', done='1', done_time='$todayTime' where id='$reqno'";
					if ($conn->query($sql) === FALSE) {
						throw new Exception(mysqli_error($conn)." Code 022");             
		         	}

		         	$sql = "INSERT INTO diesel_api.cons_dsl (`date`, `company`, `tno`, `pump`, `qty`, `rate`, `amount`, `stockid`, `done`, `done_time`, `done_branch`) VALUES ('$cons_date','$cons_company','$cons_tno','$pump_code','$qty2','$rate2','$amount2','$PurchaseId2','1','$todayTime','$branch_name')";
					if ($conn->query($sql) === FALSE) {
						throw new Exception(mysqli_error($conn)." Code 023");             
		         	}
					$Lastid = $conn->insert_id;
 
		         	$sql = "update diesel_api.dplus_supplies set sale_price='$rate', sale_amount ='$amount', status='1', trucktype='$trucktype', narration='$description', reqid='$reqno\,$Lastid', tripid='$tripno', purchaseid='$PurchaseId\,$PurchaseId2', masterid='$StockMasterId' where dbid='$dbid'";
					if ($conn->query($sql) === FALSE) {
						throw new Exception(mysqli_error($conn)." Code 024");             
					}

	         	} else {
					
					$sql = "update diesel_api.cons_dsl set pump='$pump_code', qty='$qty', rate='$rate', amount='$amount', stockid='$PurchaseId', done_branch='$branch_name', done='1', done_time='$todayTime' where id='$reqno'";
					if ($conn->query($sql) === FALSE) {
						throw new Exception(mysqli_error($conn)." Code 025");             
		         	}

		         	$sql = "update diesel_api.dplus_supplies set sale_price='$rate', sale_amount ='$amount', status='1', trucktype='$trucktype', narration='$description', reqid='$reqno', tripid='$tripno', purchaseid='$PurchaseId', masterid='$StockMasterId' where dbid='$dbid'";
					if ($conn->query($sql) === FALSE) {
						throw new Exception(mysqli_error($conn)." Code 026");             
					}
	         	} 

			}
///////////////end - insert records 

///////////////start - Trip Quantity Update

			if($tripno!='NA' and $tripno!=''){

				$sql = "select count(*) as rowcount, COALESCE(SUM(diesel_qty),0) as diesel_qty, COALESCE(SUM(diesel),0) as diesel from dairy.trip where id='$tripno' and (status='0' or active_trip='1')";	 
				if ($conn->query($sql) === FALSE) {
						throw new Exception(mysqli_error($conn)." Code 027");             
				}
				$trp = $conn->query($sql)->fetch_assoc();

				if($split_entry=='1'){
					$getamount = $amount + $amount2;
				} else {
					$getamount = $amount;
				}

				$trip_qty = sprintf("%.2f", $trp['diesel_qty'] + $qty);
				$trip_amount =  sprintf("%.2f", $trp['diesel'] + $getamount);

				if($trp['rowcount'] > 0){

					$sql = "update dairy.trip set diesel='$trip_amount', diesel_qty='$trip_qty' where id='$tripno'"; 
					if ($conn->query($sql) === FALSE) {
						throw new Exception(mysqli_error($conn)." Code 028");             
	         		}

         		} else{
					throw new Exception("Running trip does not exist");             
         		}

				$sql = "select sum(qty) as qty, sum(amount) as amount from dairy.diesel where trip_id='$tripno'";	
				if ($conn->query($sql) === FALSE) {
					throw new Exception(mysqli_error($conn)." Code 029");             
				}
				$res = $conn->query($sql)->fetch_assoc();

				if($res['qty']!=$trip_qty or $res['amount']!=$trip_amount){
					throw new Exception("Mismatched Diesel Qty or Amount in Trip $tripno");             
				}

			}

///////////////end - Trip Quantity Update

///////////////start - Stock Update

			if($trucktype!='Unknown Vehicle'){
				
				if($split_entry=="1"){

					$sql = "UPDATE dairy.diesel_pump_stock SET balance='$UpdateStockBalance' WHERE id='$StockId'";
					if ($conn->query($sql) === FALSE) {
						throw new Exception(mysqli_error($conn)." Code 030");             
					}

					$sql = "UPDATE dairy.diesel_pump_stock SET balance='$UpdateStockBalance2' WHERE id='$StockId2'";
					if ($conn->query($sql) === FALSE) {
						throw new Exception(mysqli_error($conn)." Code 031");             
					}
				} else {

					$sql = "UPDATE dairy.diesel_pump_stock SET balance='$UpdateStockBalance' WHERE id='$StockId'";
					if ($conn->query($sql) === FALSE) {
						throw new Exception(mysqli_error($conn)." Code 032");             
					}
				}
					///////////////check whether updated stock 1 balance is correct ?
					$sql = "select COALESCE(SUM(qty),0) as qty from dairy.diesel where stockid='$PurchaseId'";
					if ($conn->query($sql) === FALSE) {
						throw new Exception(mysqli_error($conn)." Code 033");             
					}
					$res = $conn->query($sql)->fetch_assoc();
					$stock_own = $res['qty'];

					$sql = "select COALESCE(SUM(qty),0) as qty from diesel_api.cons_dsl where stockid='$PurchaseId'";
					if ($conn->query($sql) === FALSE) {
						throw new Exception(mysqli_error($conn)." Code 034");             
					}
					$res = $conn->query($sql)->fetch_assoc();
					$stock_con = $res['qty'];
 
					$CalculatedStock = sprintf("%.2f", $StockQty-($stock_own+$stock_con));

					if($UpdateStockBalance!=$CalculatedStock && $ErrorStatus!='1'){
						throw new Exception("Stock 1 balance and Consumption Qty Mismatched");             
					}
					///////////////check whether updated stock 2 balance is correct ?
					if($split_entry=="1"){

						$sql = "select COALESCE(SUM(qty),0) as qty from dairy.diesel where stockid='$PurchaseId2'";
						if ($conn->query($sql) === FALSE) {
							throw new Exception(mysqli_error($conn)." Code 035");             
						}
						$res = $conn->query($sql)->fetch_assoc();
						$stock_own = $res['qty'];

						$sql = "select COALESCE(SUM(qty),0) as qty from diesel_api.cons_dsl where stockid='$PurchaseId2'";
						if ($conn->query($sql) === FALSE) {
							throw new Exception(mysqli_error($conn)." Code 036");             
						}
						$res = $conn->query($sql)->fetch_assoc();
						$stock_con = $res['qty'];

						$CalculatedStock = sprintf("%.2f", $StockQty2-($stock_own+$stock_con));

						if($UpdateStockBalance2!=$CalculatedStock && $ErrorStatus2!='1'){
							// $TextMsg = "Stock 2 balance and Consumption Qty Mismatched";
							// displayMSG($TextMsg);
							throw new Exception("Stock 2 balance and Consumption Qty Mismatched");             
						}
					}

			}

///////////////end - Stock Update
  
	$conn->query("COMMIT");
	echo "
	<script>
	Swal.fire({
	position: 'top-end',
	icon: 'success',
	title: 'Updated Successfully',
	showConfirmButton: false,
	timer: 1000
	})
	</script>";

} catch(Exception $e) { 

	$conn->query("ROLLBACK"); 
	$content = $e->getMessage();
	$content = preg_replace("/[^0-9a-zA-Z_\.\- ]/", "", $content);  
	echo "
	<script>
	Swal.fire({
	icon: 'error',
	title: 'Error !!!',
	text: '$content'
	})
	</script>";		
} 