<?php
    require('connect.php'); 
    
    $id = $conn->real_escape_string($_REQUEST['id']);
    $sql = "select * from diesel_api.dplus_supplies where dbid='$id'";
    if ($conn->query($sql) === FALSE) {
        echo mysqli_error($conn);
        exit();                  
    }
    $row = $conn->query($sql)->fetch_assoc();
?>
<style type="text/css"> 
   .modal-backdrop
   {
   opacity:0.9 !important;
   background: #e9ecef;
   }

	#appenddiv, #appenddiv2 {
		display: block; 
		position:relative
	} 
	.ui-autocomplete {
		position: absolute;
	}
</style>

<script type="text/javascript">
  $(function() {
  $("#newveh").autocomplete({
  source: 'tank_vehicle_auto.php',
  appendTo: '#appenddiv',
  select: function (event, ui) { 
         $('#newveh').val(ui.item.value);   
         return false;
  },
  change: function (event, ui) {
  if(!ui.item){
	  $(event.target).val("");
	  Swal.fire({
	  icon: 'error',
	  title: 'Error !!!',
	  text: 'Vehicle does not exists !'
	  })
	  $("#newveh").val("");
	  $("#newveh").focus();
  }
  }, 
  focus: function (event, ui){
  return false;
  }
  });
  });
</script>
<form method="post" action="" id="updateSupplies" role="form" autocomplete="off">
   <input type="hidden" value="<?php echo $id; ?>" name="dbid" oninput="this.value=this.value.replace(/[^a-z A-Z 0-9]/,'')">
   <div class="modal-body">
      <p style="color: #444;"> APPROVE STOCK <button type="button" class="close" data-dismiss="modal"> &times; </button> 
      <p style="border-bottom: 1px solid #ccc;"></p>
      </p>
      <div class="row">
         <div class="form-group col-md-4">
         	<label>Date</label>
            <input type="text" oninput="this.value=this.value.replace(/[^a-z A-Z 0-9]/,'')" class="form-control" id="" name="" readonly="" value="<?php echo date('d/m/Y H:i:s', strtotime($row['datetime'])) ?>">
         </div>
         <div class="form-group col-md-4">
         	<label>Tanker</label>
            <input type="text" oninput="this.value=this.value.replace(/[^a-z A-Z 0-9]/,'')" class="form-control" id="" name="" readonly="" value="<?php echo $row["tank_name"] ?>">
         </div>
         <div class="form-group col-md-4">
         	<label>Vehicle</label>
            <input type="text" oninput="this.value=this.value.replace(/[^a-z A-Z 0-9]/,'')" class="form-control" id="" name="" readonly="" value="<?php echo $row["vehicle_name"] ?>">
         </div>
         <div class="form-group col-md-4">
         	<label>Operator</label>
            <input type="text" oninput="this.value=this.value.replace(/[^a-z A-Z 0-9]/,'')" class="form-control" id="" name="" readonly="" value="<?php echo $row["personnel_name"] ?>">
         </div>
         <div class="form-group col-md-4">
         	<label>Quantity</label>
            <input type="text" oninput="this.value=this.value.replace(/[^a-z A-Z 0-9]/,'')" class="form-control" id="" name="" readonly="" value="<?php echo $row["quantity"] ?>">
         </div>
        
        <?php
              //start - check what type of truck it is
              $trucktype = "Unknown Vehicle";
              
              $sql = "select tno from dairy.own_truck where tno='".preg_replace("/[^0-9a-zA-Z]/", "", $row["vehicle_name"])."'"; 
              if ($conn->query($sql) === FALSE) {
                    echo mysqli_error($conn);
                    exit();                  
              }
              if($conn->query($sql)->num_rows>0){
                $trucktype = "RRPL Vehicle";
              }

              $sql = "select vehno from diesel_api.pump_vehicle where vehno='".preg_replace("/[^0-9a-zA-Z]/", "", $row["vehicle_name"])."'"; 
              if ($conn->query($sql) === FALSE) {
                    echo mysqli_error($conn);
                    exit();                  
              }
              if($conn->query($sql)->num_rows>0){
                $trucktype = "RRPL Non-Trip Vehicle";
              }

              $sql = "select tno from diesel_api.cons_truck where tno='".preg_replace("/[^0-9a-zA-Z]/", "", $row["vehicle_name"])."'";
              if ($conn->query($sql) === FALSE) {
                    echo mysqli_error($conn);
                    exit();                  
              }
              if($conn->query($sql)->num_rows>0){
                $trucktype = "SCPL Vehicle";
              }
              //end - check what type of truck it is

              //start - stock balance availability
              $sql = "select * from dairy.diesel_pump_branch where trim(name)='".trim($row['tank_name'])."'";
              if ($conn->query($sql) === FALSE) {
                  echo mysqli_error($conn);
                  exit();                  
              }
              $res = $conn->query($sql)->fetch_assoc();
              $pump_code = $res['code'];

              $sql = "SELECT COALESCE(SUM(balance),0) as balance FROM dairy.diesel_pump_stock WHERE pumpcode='$pump_code' and status='0'";
              if ($conn->query($sql) === FALSE) {
                    echo mysqli_error($conn);
                    exit();                  
              }
              $res = $conn->query($sql)->fetch_assoc();
              $pump_balance = $res['balance'];


              $sql = "select * from dairy.trip where tno='".preg_replace("/[^0-9a-zA-Z]/", "", $row["vehicle_name"])."' and (status='0' or active_trip='1')";
              if ($conn->query($sql) === FALSE) {
                    echo mysqli_error($conn);
                    exit();                  
              }
              $trip_result = $conn->query($sql);

              $sql = "select * from diesel_api.cons_dsl where tno='".preg_replace("/[^0-9a-zA-Z]/", "", $row["vehicle_name"])."' and done='0'";
              if ($conn->query($sql) === FALSE) {
                    echo mysqli_error($conn);
                    exit();                  
              }
              $scpl_result = $conn->query($sql);

        ?>
        <input type="hidden" value="<?php echo $trucktype; ?>" name="trucktype" oninput="this.value=this.value.replace(/[^a-z A-Z 0-9]/,'')">
        <input type="hidden" value="<?php echo $trip_no_new; ?>" name="trip_no_new" />

        <div class="form-group col-md-4">
          <label>Vehicle Type</label>
            <input type="text" oninput="this.value=this.value.replace(/[^a-z A-Z 0-9]/,'')" class="form-control" id="" name="" readonly="" value="<?php echo $trucktype ?>" style="background: #fff;">
         </div>


         <div class="form-group col-md-4">
          <label>Tanker Stock Qty</label>
            <input type="text" oninput="this.value=this.value.replace(/[^a-z A-Z 0-9]/,'')" class="form-control" id="" name="" readonly="" value="<?php echo $pump_balance ?>">
         </div>
        
        <?php
            if($trucktype!='RRPL Vehicle' and $trucktype!='SCPL Vehicle'){
        ?>
        <div class="form-group col-md-4">
          <label>Narration</label>
            <input type="text" oninput="this.value=this.value.replace(/[^a-z A-Z 0-9]/,'')" class="form-control" id="" name="description" required="">
         </div>
         <?php } ?>


        <?php
            if($trucktype=='RRPL Vehicle'){
        ?>
        <div class="form-group col-md-4">
            <label>Trip </label>
            <select class="form-control" required="" name="tripno">
              <option value=""> -- select -- </option>
              <?php
                while ($data = $trip_result->fetch_assoc()) {
                  echo "<option value='$data[id]_$data[trip_no]'> $data[from_station] to $data[to_station] ($data[id]) </option>";
                }
              ?>
              <option value="NA"> Trip pending </option>
            </select>
         </div>
         <?php } ?>

        <?php
            if($trucktype=='SCPL Vehicle'){
        ?>
        <div class="form-group col-md-4">
            <label>Request </label>
            <select class="form-control" required="" name="reqno">
              <option value=""> -- select -- </option>
              <?php
                while ($data = $scpl_result->fetch_assoc()) {
                  $tank = $data['qty'];
                  $tank_date = date('d/m/y', strtotime($data['date']));
                  if($data['full']=='1'){ $tank = "Full Tank"; }
                  echo "<option value='$data[id]'> $tank by $data[user] ($tank_date)</option>";
                }
              ?>
            </select>
         </div>
         <?php } ?>

         <div class="col-md-4" style="margin-top: 18px;">
            <button type="button" id="hidemodal" class="btn btn-danger" data-dismiss="modal">CLOSE</button>
            <?php
            echo '<input type="submit" id="formbtn" class="btn btn-success" name="submit" value="SAVE" />';
            ?>
         </div>

      </div>
   </div>
   <div class="modal-footer" style="justify-content: center;">
    <p style="font-size: 12px; text-align: left !important; margin: 0px;">
RRPL Vehicle - ट्रिप डालना कंपल्सरी होगा अगर ट्रिप नहीं बनी है तोह पेंडिंग में दाल सकते है और तीन दिन में ट्रिप बनाके अपडेट करना होगा | <br>
RRPL Non-Trip Vehicle - बिना ट्रिप के सिस्टम में एंट्री हो जाएगा पर एडमिन अप्रूवल  चाहिए होगा | <br>
SCPL Vehicle - शिवानी की गाड़ी अगर डीजल लेगी तोह उनके सिस्टम में एंट्री होगा |<br>
Unknown Vehicle - सिस्टम में एंट्री नहीं होगी, अप्रूवल करने के लिए नरेशन डालना होगा जैसे एयर निकलने के लिए किया हो इत्यादि | 

    </p>
     
   </div>
</form>