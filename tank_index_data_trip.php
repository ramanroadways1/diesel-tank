<?php
 
require('connect.php');
 
    $connection = new PDO('mysql:host='.$host.';dbname='.$db_name.';', $username, $password );
    $statement = $connection->prepare("SELECT d.*, b.name as pumpname, emp.name as empname from dairy.diesel d
left join dairy.diesel_entry e on e.id=d.id and e.unq_id = d.unq_id
left join dairy.diesel_pump_branch b on b.code = e.card 
left join rrpl_database.emp_attendance emp on emp.code = d.branch_user
where d.trip_id='NA' and b.admin='$branch_name'");
    $statement->execute();
    $result = $statement->fetchAll();
    $count = $statement->rowCount();
    $data = array();

foreach($result as $row)
{
    $sub_array = array(); 
    $sub_array[] = "<center><button onclick='updateTrip(\"".$row['id']."\")' class='btn btn-sm btn-warning' style='margin-left: 10px; color: #fff; letter-spacing: 1px;'> <i class='fa fa-truck'></i> ADD TRIP </button></center>"; 

    $sub_array[] = date('Y-m-d', strtotime($row['timestamp'])); 
    $sub_array[] = date('d/m/Y H:i:s', strtotime($row['timestamp'])); 
    $sub_array[] = $row["pumpname"]; 
    $sub_array[] = $row["tno"].""; 
    $sub_array[] = $row["qty"]; 
    $sub_array[] = ucwords(strtolower($row["empname"])); 
    $sub_array[] = $row["id"]; 

    $data[] = $sub_array;
} 

    $results = array(
      "sEcho" => 1,
    "iTotalRecords" => $count,
    "iTotalDisplayRecords" => $count,
    "aaData"=>$data);

echo json_encode($results); 
exit
?>
 