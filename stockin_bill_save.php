<?php
   	
	require('connect.php'); 
 
    $dbid = $conn->real_escape_string($_POST['dbid']);
    $pump = $conn->real_escape_string($_POST['pump']);
    $calc = $conn->real_escape_string($_POST['calculate']); 

	if(isset($_POST['loadtemp']))
	{
		$loadtemp = $conn->real_escape_string(strtoupper($_POST['loadtemp']));
	} else {
		$loadtemp = 0;
	} 

	if(isset($_POST['unloadtemp']))
	{
		$unloadtemp = $conn->real_escape_string(strtoupper($_POST['unloadtemp']));
	} else {
		$unloadtemp = 0;
	} 

	if(isset($_POST['short']))
	{
		$short = $conn->real_escape_string(strtoupper($_POST['short']));
	} else {
		$short = 0;
	} 
	
	// if(isset($_POST['file']))
	// {
	// 	$file = $conn->real_escape_string(strtoupper($_POST['file']));
	// } else {
	// 	$file = NULL;
	// } 

try {
	$conn->query("START TRANSACTION"); 

			$bothfiles[0] = NULL;
			// Checks if user sent an empty form  
			if($calc=='1') { 

			// Configure upload directory and allowed file types 
			$upload_dir = 'uploads/'; //.DIRECTORY_SEPARATOR
			$allowed_types = array('jpg', 'png', 'jpeg', 'gif', 'pdf'); 
			  
			// Define maxsize for files i.e 2MB 
			$maxsize = 2 * 1024 * 1024;  

				$bothfiles = array(); 
			// Loop through each file in files[] array 
			foreach ($_FILES['file']['tmp_name'] as $key => $value) { 
			      
			    $file_tmpname = $_FILES['file']['tmp_name'][$key]; 
			    $file_name = $_FILES['file']['name'][$key]; 
			    $file_size = $_FILES['file']['size'][$key]; 
			    $file_ext = pathinfo($file_name, PATHINFO_EXTENSION); 
					
					$file_name = preg_replace("/[^a-zA-Z0-9.]+/", "", $file_name);
			    // Set upload file path 
			    $filepath = $upload_dir.time().$file_name; 

			    // Check file type is allowed or not 
			    if(in_array(strtolower($file_ext), $allowed_types)) { 

			        // Verify file size - 2MB max  
					if ($file_size > $maxsize){
					
					throw new Exception("Error: {$file_name} - File size is larger than the allowed limit."); 

			        } else {

			        	 if( move_uploaded_file($file_tmpname, $filepath)) {
			            	$bothfiles[] = $filepath;
			            }  
			            else {     
							throw new Exception("Error uploading {$file_name}"); 
			            } 
			        }    
			    } 
			    else {  
			        // If file extention not valid 
					throw new Exception("Error uploading {$file_name} ({$file_ext} file type is not allowed)"); 
			    }  
			} 
			}

			$sql = "select * from dairy.diesel_pump_stock where id='$dbid'";
			if ($conn->query($sql) === FALSE) {
				throw new Exception(mysqli_error($conn)." Code 002");             
         	}
			$row = $conn->query($sql)->fetch_assoc();
			$prate = $row['purchaserate'];
			$pqty = round(($row['purchaseqty']/1000),2); 
 
			$short = round(((0.84)*($loadtemp-$unloadtemp)*($pqty)),2);
			$tqty = $row['purchaseqty']-($short); 


			$sql = "update dairy.diesel_pump_stock set openuser='$branch_emp' ,balance='$tqty', totalqty='$tqty', masterid=id, pumpcode='$pump', loadtemp='$loadtemp', unloadtemp='$unloadtemp', expshort='$short', tempslip='$bothfiles[0]', status='0' where id='$dbid'";
			if ($conn->query($sql) === FALSE) {
					throw new Exception(mysqli_error($conn)." Code 001");             
			}
 
			$conn->query("COMMIT");
			echo "
			<script>
			Swal.fire({
			position: 'top-end',
			icon: 'success',
			title: 'Updated Successfully',
			showConfirmButton: false,
			timer: 1000
			})
			</script>";

} catch(Exception $e) { 

			$conn->query("ROLLBACK"); 
			$content = $e->getMessage();
			$content = preg_replace("/[^0-9a-zA-Z_\.\- ]/", "", $content);  
			echo "
			<script>
			Swal.fire({
			icon: 'error',
			title: 'Error !!!',
			text: '$content'
			})
			</script>";		
} 