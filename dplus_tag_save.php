<?php 
 
	require('connect.php');

	function sanitizer($data) {
		$data = trim($data);
		$data = stripslashes($data);
		$data = htmlspecialchars($data);
		return $data;
	}   

 	$pin = sanitizer($conn->real_escape_string($_POST['pin'])); 
 	$company = sanitizer($conn->real_escape_string($_POST['company'])); 
 	$truckno = strtoupper(sanitizer($conn->real_escape_string($_POST['truckno']))); 
 	$tagno = sanitizer($conn->real_escape_string($_POST['tagno'])); 
 
	$file_name = $_FILES['truckphoto']['name'];
	$file_tmp = $_FILES['truckphoto']['tmp_name'];
	$file_type = $_FILES['truckphoto']['type'];
	@$file_ext = strtolower(end(explode('.',$file_name)));
	$uploadid = strtoupper(chr(rand(65, 90)) . strtotime('now') . chr(rand(97,122)));
	$getname = substr(preg_replace("/[^0-9a-zA-Z]/", "", $file_name), 0, -4);	

	$file_name2 = $_FILES['tagphoto']['name'];
	$file_tmp2 = $_FILES['tagphoto']['tmp_name'];
	$file_type2 = $_FILES['tagphoto']['type'];
	@$file_ext2 = strtolower(end(explode('.',$file_name2)));
	$uploadid2 = strtoupper(chr(rand(65, 90)) . strtotime('now') . chr(rand(97,122)));
	$getname2 = substr(preg_replace("/[^0-9a-zA-Z]/", "", $file_name2), 0, -4);	

	$allowedExts = array("gif", "jpeg", "jpg", "png", "pdf"); 
 
 	try {
	$conn->query("START TRANSACTION"); 

	define("dplus_url","https://api.dieselplus.net/api/v1/raman-roadways/");
 	$sql = "SELECT title FROM diesel_api.dsl_cpanel where company='DIESELPLUS_LOGIN'";
    if ($conn->query($sql) === FALSE) {
      throw new Exception(mysqli_error($conn)); 
    }
    $resx = $conn->query($sql);
    $rowx = $resx->fetch_assoc();
    define("dplus_login",$rowx["title"]);

    $sql = "SELECT title FROM diesel_api.dsl_cpanel where company='DIESELPLUS_TOKEN'";
    if ($conn->query($sql) === FALSE) {
      throw new Exception(mysqli_error($conn)); 
    }
    $resx = $conn->query($sql);
    $rowx = $resx->fetch_assoc();
    define("dplus_token",$rowx["title"]);
 
	$sql = "select name from rrpl_database.emp_attendance where code = '$branch_emp'";
	if($conn->query($sql) === FALSE) {
	throw new Exception(mysqli_error($conn)." Code 008");             
	} 
	$res = $conn->query($sql);
	$row = $res->fetch_assoc();
	$emp_name = $row['name'];

		$sql = "select title from diesel_api.dsl_cpanel where company='ADDTAGPIN'";
		if($conn->query($sql) === FALSE) {
		    throw new Exception(mysqli_error($conn)." Code 004");             
		}
		$res = $conn->query($sql)->fetch_assoc();
		$getPIN = $res['title'];

		if($pin!=$getPIN){
			throw new Exception("Incorrect Password !");             
		}

		$sql = "SELECT * FROM diesel_api.dplus_vehicles where license='$truckno' and is_active='1'";  
		if ($conn->query($sql)->num_rows > 0) {
					throw new Exception("Truckno already having another tag !"); 
		}

		$sql = "SELECT * FROM diesel_api.dplus_vehicles where terminal='$tagno' and is_active='1'";  
		if ($conn->query($sql)->num_rows > 0) {
					throw new Exception("TagNo already installed on another Truck"); 
		}
 
	if ((($file_type == "image/gif") || ($file_type == "image/jpeg") || ($file_type == "image/jpg") || ($file_type == "image/pjpeg") || ($file_type == "image/x-png") || ($file_type == "image/png") || ($file_type == "application/pdf") && in_array($file_ext, $allowedExts)) && (($file_type2 == "image/gif") || ($file_type2 == "image/jpeg") || ($file_type2 == "image/jpg") || ($file_type2 == "image/pjpeg") || ($file_type2 == "image/x-png") || ($file_type2 == "image/png") || ($file_type2 == "application/pdf") && in_array($file_ext2, $allowedExts))){

	$truckphoto = $getname.$uploadid.".".$file_ext;	
	$tagphoto = $getname2.$uploadid2.".".$file_ext2;	
	
	$targetPath =  'uploads/'.$truckphoto; 
	$targetPath2 =  'uploads/'.$tagphoto; 
 
	if(move_uploaded_file($file_tmp, $targetPath) && move_uploaded_file($file_tmp2, $targetPath2)){

		if($company=='4130933214'){ //RAMAN ROADWAYS
			$sql = "select tno from (SELECT tno FROM dairy.own_truck UNION SELECT vehno from diesel_api.pump_vehicle) temp where tno='".preg_replace("/[^0-9a-zA-Z]/", "", $truckno)."'"; 

		} else if($company=='149233631'){ //SHIVANI CARRIERS
			$sql = "select tno from diesel_api.cons_truck where tno='".preg_replace("/[^0-9a-zA-Z]/", "", $truckno)."'";
			 
		}  

		$res = $conn->query($sql);
		if ($res === FALSE) {
			$errorno = mysqli_error($conn);
			throw new Exception($errorno); 
		}

		if ($res->num_rows == 0) {
				throw new Exception("Truck number not found in rrpl system"); 
		}

		function displayRecursiveResults($arrayObject) {
			$respond = "";
		    foreach($arrayObject as $key=>$data) {
		        if(is_array($data)) {
		            displayRecursiveResults($data);
		        } elseif(is_object($data)) {
		            displayRecursiveResults($data);
		        } else {
		        	$respond = $respond.strtoupper($key).": ".$data." ";
		        }
		    }
			throw new Exception($respond); 
		}

		function add_vehicle($conn,$company,$truckno,$tagno,$truckphoto,$tagphoto,$emp_name){
			$dated = date('Y-m-d');
			$curl = curl_init(); 
			curl_setopt_array($curl, array(
			  CURLOPT_URL => dplus_url."vehicles.json",
			  CURLOPT_RETURNTRANSFER => true,
			  CURLOPT_ENCODING => "",
			  CURLOPT_MAXREDIRS => 10,
			  CURLOPT_TIMEOUT => 30,
			  CURLOPT_FOLLOWLOCATION => true,
			  CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
			  CURLOPT_CUSTOMREQUEST => "POST",
			  CURLOPT_POSTFIELDS =>"{\r\n    \"vehicle\":\r\n    {\r\n    \"registration_country_id\": \"IN\",\r\n    \"vehicle_group_id\": \"$company\", \r\n    \"license\": \"$truckno\",\r\n    \"identification_in_terminal\": \"$tagno\", \r\n    \"control_unit\": \"none\",\r\n    \"is_consumption_calculation\": false,\r\n    \"is_allowed_internal_facilities_supplies\": true,\r\n    \"is_allowed_all_fluids\": true,\r\n    \"is_active\": true,\r\n    \"activation_start_date\": \"$dated\"\r\n    }\r\n}",
			  CURLOPT_HTTPHEADER => array(
			    "X-Login-Name: ".dplus_login,
	    		"X-Auth-Token: ".dplus_token,
			    "Content-Type: application/json"
			  ),
			));

			$response = curl_exec($curl); 
			curl_close($curl);

			$output = json_decode($response , true); 
			@$errorcode = $output['error']['code'];

			if($errorcode!=""){
				displayRecursiveResults($output);
			} else {
				@$getid = $output['vehicle']['id'];

				if($getid!=""){

					$row = $output['vehicle'];
			 
		    		$sql = "insert into diesel_api.dplus_vehicles (id, code, vehicle_group_id, license, terminal, is_active, updated_by, updated_at, unit_source, activation_start_date, organization_id, truckphoto, tagphoto, personnel, admin) values ('$row[id]', '$row[code]', '$row[vehicle_group_id]', '$row[license]', '$row[identification_in_terminal]', '$row[is_active]', '$row[updated_by]', '$row[updated_at]', '$row[unit_source]', '$row[activation_start_date]', '$row[organization_id]', '$truckphoto', '$tagphoto', '".$emp_name."', '1')";		    
		    		if ($conn->query($sql) === FALSE) {
						$errorno = mysqli_error($conn);
						throw new Exception($errorno); 
					}

				} else {
		    		throw new Exception("API is not responding required data"); 
				} 
			}  		
		} 

		add_vehicle($conn,$company,$truckno,$tagno,$truckphoto,$tagphoto,$emp_name);

	} else {
			throw new Exception("Uploading Error - Something went wrong !");  
	}

	} else {
			throw new Exception("Please upload a valid image file !");  
	}
 
			$conn->query("COMMIT");
			echo "
			<script>
			Swal.fire({
			position: 'top-end',
			icon: 'success',
			title: 'Truck Tag Updated Successfully',
			showConfirmButton: false,
			timer: 1000
			})
			</script>";

 	} catch(Exception $e) { 

			$conn->query("ROLLBACK"); 
			$content = $e->getMessage();
			$content = preg_replace("/[^0-9a-zA-Z_ ]/", "", $content);  
			echo "
			<script>
			Swal.fire({
			icon: 'error',
			title: 'Error !!!',
			text: '$content'
			})
			</script>";		
			unlink('uploads/'.$truckphoto);
			unlink('uploads/'.$tagphoto);
	} 
?>  