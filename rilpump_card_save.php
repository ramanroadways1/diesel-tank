<?php
 
 require('connect.php');
  
    $cardno = $conn->real_escape_string($_POST['cardno']);
    $amount = $conn->real_escape_string($_POST['amount']);

try {
    $conn->query("START TRANSACTION"); 
     
     
    $sql = "select * from diesel_api.dsl_ril_stock order by id desc limit 1";
    if ($conn->query($sql) === FALSE) {
            throw new Exception(mysqli_error($conn)." Code 001");             
    }
    $row = $conn->query($sql)->fetch_assoc();
    $stock = $row['stockid']+1;
    $date = date('Y-m-d');
 
      $sql = "select * from diesel_api.dsl_ril_stock WHERE cardno = '$cardno' and status='0'";
      if ($conn->query($sql) === FALSE) {
            throw new Exception(mysqli_error($conn)." Code 002");             
      }
      if($conn->query($sql)->num_rows>0){
            throw new Exception("Please clear pending card stock !");             
      }
      $row = $conn->query($sql)->fetch_assoc();


      $sql = "insert into diesel_api.dsl_ril_stock (reqdate, stockid, amount, cardno, totalamt, balance, stamp) values ('$date', '$stock', '$amount', '$cardno', '$amount', '$amount', now())";
      if ($conn->query($sql) === FALSE) {
            throw new Exception(mysqli_error($conn)." Code 003");             
      }

     
    $conn->query("COMMIT");
    echo "
    <script>
    Swal.fire({
    position: 'top-end',
    icon: 'success',
    title: 'Updated Successfully',
    showConfirmButton: false,
    timer: 1000
    })
    </script>";

} catch(Exception $e) { 

    $conn->query("ROLLBACK"); 
    $content = $e->getMessage();
    $content = preg_replace("/[^0-9a-zA-Z_\.\- ]/", "", $content);  
    echo "
    <script>
    Swal.fire({
    icon: 'error',
    title: 'Error !!!',
    text: '$content'
    })
    </script>";     
} 
  
?>