<?php
   require('connect.php'); 
    $id = $conn->real_escape_string($_REQUEST['id']);
    $sql = "select * from dairy.diesel_pump_stock where id='$id'";
    $row = $conn->query($sql)->fetch_assoc();
?>
<style type="text/css"> 
   .modal-backdrop
   {
   opacity:0.9 !important;
   background: #e9ecef;
   }

	#appenddiv, #appenddiv2 {
		display: block; 
		position:relative
	} 
	.ui-autocomplete {
		position: absolute;
	}
</style>
 
<form method="post" action="" id="updatestock" role="form" autocomplete="off" enctype='multipart/form-data'>
   <input type="hidden" value="<?php echo $id; ?>" name="dbid" oninput="this.value=this.value.replace(/[^a-z A-Z 0-9]/,'')">
   <div class="modal-body">
      <p style="color: #444;"> <?php echo $row["branch"] ?> STOCK-IN  <button type="button" class="close" data-dismiss="modal"> &times; </button> 
      <p style="border-bottom: 1px solid #ccc;"></p>
      </p>
      <div class="row">
         <div class="form-group col-md-3">
         	<label>Date</label>
            <input type="text" oninput="this.value=this.value.replace(/[^a-z A-Z 0-9]/,'')" class="form-control" id="" name="" readonly="" value="<?php echo date('d/m/Y', strtotime($row['purchasedate'])) ?>" style=" color: #000;">
         </div>
         <div class="form-group col-md-3">
         	<label>Qty (ltr)</label>
            <input type="text" oninput="this.value=this.value.replace(/[^a-z A-Z 0-9]/,'')" class="form-control" id="qty" name="" readonly="" value="<?php echo $row["purchaseqty"] ?>" style=" color: #000;">
         </div>
         <div class="form-group col-md-3">
         	<label>Fuel</label>
            <input type="text" oninput="this.value=this.value.replace(/[^a-z A-Z 0-9]/,'')" class="form-control" id="" name="" readonly="" value="<?php echo $row['adi']=='0' ? 'HSD' : 'ADI'; ?>" style=" color: #000;">
         </div>
         <div class="form-group col-md-3">
         	<label>Supplier</label>
            <input type="text" oninput="this.value=this.value.replace(/[^a-z A-Z 0-9]/,'')" class="form-control" id="" name="" readonly="" value="<?php echo $row["vendor"] ?>" style=" color: #000;">
         </div>

         <div class="form-group col-md-5">
          <label>Pump </label>
            <select class="form-control" name="pump" required="">
              <option value=""> --select-- </option>
              <?php

                $sql = "select * from dairy.diesel_pump_branch where admin='$branch_name' and billtype='1'";
                $res = $conn->query($sql);
                while($row = $res->fetch_assoc()) {
                    echo "<option value='$row[code]'> $row[name] </option>";
                }

              ?>
            </select>
         </div>

         <div class="col-md-3">
           <label>Calculate Temp. </label>
           <select class="form-control" onChange="filtr(this);" name="calculate" required="">
              <option value="1"> Yes </option>
              <option value="0"> No (Update Later)  </option>
           </select>
         </div>


             <div class="col-md-4">
      <label>LOADING  TEMP (<sup>o</sup>C)</label>
      <input type="text"   onkeyup="calc()"  id="load" name="loadtemp" class="form-control" required="required"  oninput="this.value=this.value.replace(/[^0-9.]/,'')">
    </div> 


    <div class="col-md-4">
      <label>UNLOADING TEMP (<sup>o</sup>C)</label>
      <input type="text"  onkeyup="calc()" id="unload" name="unloadtemp" class="form-control" required="required"  oninput="this.value=this.value.replace(/[^0-9.]/,'')">
    </div> 

    <div class="col-md-4">
      <label>  TEMPERATURE GAUGE </label>
      <input type="file" name="file[]" id="gauge" accept="image/x-png,image/gif,image/jpeg,application/pdf" class="form-control" style="height: 32px !important; overflow: hidden;" required="required">
    </div> 

 
    <div class="col-md-4">
      <label>TEMPERATURE SHORTAGE</label>
      <input type="text" name="short" id="short" class="form-control" readonly="">
    </div> 

      </div>
   </div>
   <div class="modal-footer">
      <button type="button" id="hidemodal" class="btn btn-danger" data-dismiss="modal">CLOSE</button>
   	  <?php 
   	  	echo '<input type="submit" id="uploadbtn" class="btn btn-success" name="submit" value="SAVE" />';
      ?>
   
   </div>
</form>


<script type="text/javascript">
   function filtr(sel) {
  
      if(sel.value == "0")
      {
        $("#load").prop('required',false);
        $("#load").prop('disabled',true);
        $("#load").val('');
        $("#unload").prop('required',false);
        $("#unload").prop('disabled',true);
        $("#unload").val('');
        $("#gauge").prop('required',false);
        $("#gauge").prop('disabled',true);   
        $("#gauge").val('');
      } else {
        $("#load").prop('required',true);
        $("#load").prop('disabled',false);
        $("#unload").prop('required',true);
        $("#unload").prop('disabled',false);
        $("#gauge").prop('required',true);
        $("#gauge").prop('disabled',false);           
      }
  }



</script>